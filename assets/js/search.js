import '../icons/styles.css';
import './../styles/clear.css';
import '../styles/views/thesis_element.scss';
import './../styles/views/similar.css';
import '../styles/views/logo.scss';
import '../styles/views/header.scss';
import '../styles/search.scss';
import './../styles/views/hightlight.scss';

import './../bootstrap';

document.addEventListener('DOMContentLoaded', function () {
    const searchInput = document.querySelector('.search__input');
    const btnSearch = document.querySelector('.search__btn-search');
    const btnClear = document.querySelector('.search__btn-clear');
    const searchComplete = document.querySelector('.search__complete');

    searchInput.addEventListener('input', (e) => {
        let query = searchInput.value;
        if (query !== '' && !searchInput.classList.contains('lock')) {
            searchComplete.classList.remove('lock');
            btnClear.style.visibility = 'visible';
            btnSearch.style.color = '#5a8dab';
            complete(e).then(result => {
                searchComplete.innerHTML = '';
                let queries = result.complete;
                if (typeof queries !== 'undefined' && queries.length > 0) {
                    queries.forEach(queryItem => {
                        let complete = document.querySelector('.search__complete');
                        let completeItem = createCompleteItem(queryItem.query);
                        complete.appendChild(completeItem);
                    });
                    if (!searchComplete.classList.contains('lock')) {
                        searchComplete.style.display = 'block';
                        searchInput.style.borderRadius = '25px 25px 0 0';
                    }
                } else {
                    searchInput.style.borderRadius = '25px';
                    searchComplete.style.display = 'none';
                }
            });
        } else {
            searchComplete.classList.add('lock');
            clearSearch();
        }
    });

    document.addEventListener('keydown', function (e) {
        if (e.which === 27) {
            clearSearch();
        }
        if (e.which === 40) {
            let completeItemFocus = document.querySelector('.complete-item-focus');
            if (completeItemFocus === null) {
                let completeItem = document.querySelector('.complete-item');
                completeItem.classList.add('complete-item-focus');
            } else {
                completeItemFocus.classList.remove('complete-item-focus');
                let nextCompleteItem = completeItemFocus.nextElementSibling;
                if (nextCompleteItem !== null && nextCompleteItem.classList.contains('complete-item')) {
                    nextCompleteItem.classList.add('complete-item-focus');
                }
            }
        }
        if (e.which === 38) {
            let completeItemFocus = document.querySelector('.complete-item-focus');
            if (completeItemFocus === null) {
                let completeItem = searchComplete.lastElementChild;
                completeItem.classList.add('complete-item-focus');
            } else {
                completeItemFocus.classList.remove('complete-item-focus');
                let previousCompleteItem = completeItemFocus.previousElementSibling;
                if (previousCompleteItem !== null && previousCompleteItem.classList.contains('complete-item')) {
                    previousCompleteItem.classList.add('complete-item-focus');
                }
            }
        }
        if (e.which === 13) {
            let completeItemFocus = document.querySelector('.complete-item-focus');
            if (completeItemFocus !== null) {
                let completeItemText = completeItemFocus.querySelector('.complete-item__text');
                let text = completeItemText.innerHTML;
                let clearValue = text.replace(/<\/?[^>]+(>|$)/g, '');
                searchInput.value = clearValue;
                clearSearch(false);
                window.location.href = location.protocol + '//' + location.host + location.pathname + "?q=" + clearValue;
            } else {
                searchInput.classList.add('lock');
                window.location.href = location.protocol + '//' + location.host + location.pathname + "?q=" + searchInput.value;
            }
        }
    });

    async function complete(e)
    {
        e.preventDefault();
        let searchValue = searchInput.value;
        let response = await fetch('/complete/q=' + searchValue, {
            method: 'POST'
        });
        let result = {};
        if (response.ok) {
            result = await response.json();
        }
        return result;
    }

    function createCompleteItem(query)
    {
        const div = document.createElement('div');
        div.className = 'complete-item';
        div.innerHTML = `<div class="complete-item__icon"><i class="icon-search-regular"></i></div>
                         <div class="complete-item__text">` + query + `</div>`;
        div.addEventListener('click', (e) => {
            let textElement = div.getElementsByClassName('complete-item__text')[0];
            let text = textElement.innerHTML;
            let clearValue = text.replace(/<\/?[^>]+(>|$)/g, '');
            searchInput.value = clearValue;
            clearSearch(false);
            window.location.href = location.protocol + '//' + location.host + location.pathname + "?q=" + clearValue;
        });

        return div;
    }

    function clearSearch(withBtnClear = true)
    {
        btnSearch.style.color = '#8d8d8d';
        if (withBtnClear) {
            btnClear.style.visibility = 'hidden';
        }
        searchInput.style.borderRadius = '25px';
        searchComplete.style.display = 'none';
    }





    let thesisBtnShow = document.getElementsByClassName('btn-show-content');
    for (let i = 0; i < thesisBtnShow.length; i++) {
        thesisBtnShow[i].addEventListener('click', () => {
            let currentThesisElement = thesisBtnShow[i].closest('.thesis-item');
            let content = currentThesisElement.querySelector('.thesis-item__content');
            let iconDown = currentThesisElement.querySelector('.fa-chevron-down');
            let iconUp = currentThesisElement.querySelector('.fa-angle-up');
            if (content.classList.contains('open')) {
                content.style.maxHeight = '300px';
                content.classList.remove('open');
                iconUp.style.display = 'none';
                iconDown.style.display = 'block';
            } else {
                let originalHeight = content.scrollHeight;
                content.style.maxHeight = originalHeight + 'px';
                content.classList.add('open');
                iconUp.style.display = 'block';
                iconDown.style.display = 'none';
            }
        });
    }

    let thesisItems = document.getElementsByClassName('thesis-item');
    for (let i = 0; i < thesisItems.length; i++) {
        let currentContentItem = thesisItems[i].querySelector('.thesis-item__content');
        if (currentContentItem !== null) {
            currentContentItem.style.maxHeight = 'none';
            let realHeight = currentContentItem.clientHeight;
            if (realHeight > 305) {
                let btnShow = thesisItems[i].querySelector('.btn-show-content');
                btnShow.style.display = 'block';
            }
            currentContentItem.style.maxHeight = '300px';
        }
    }

    let bashCopyButtons = document.getElementsByClassName('btn-copy-bash');
    for (let i = 0; i < bashCopyButtons.length; i++) {
        let copyText = bashCopyButtons[i].querySelector('.tooltip__text');
        bashCopyButtons[i].addEventListener('click', () => {
            let bashCommand = bashCopyButtons[i].previousElementSibling;
            let text = bashCommand.innerText;
            let resultText = text.replace(/#/g, "");
            resultText = resultText.replace(/\$/g, "");
            resultText = resultText.replace(/[^\x00-\x7F]/g, "");
            let strParts = resultText.split('\n');
            let resultParts = [];
            strParts.forEach(function (item, i) {
                if (item.trim().length !== 0) {
                    resultParts.push(item.trim());
                }
            });
            navigator.clipboard.writeText(resultParts.join('\n')).then(() => {
                copyText.style.visibility = 'visible';
                copyText.style.opacity = '1';
            });
        });
        bashCopyButtons[i].addEventListener('mouseleave', () => {
            copyText.style.visibility = 'hidden';
            copyText.style.opacity = '0';
        });
    }

    let cssCopyButtons = document.getElementsByClassName('btn-copy-css');
    for (let i = 0; i < cssCopyButtons.length; i++) {
        let copyText = cssCopyButtons[i].querySelector('.tooltip__text');
        cssCopyButtons[i].addEventListener('click', () => {
            let styles = cssCopyButtons[i].previousElementSibling;
            let text = styles.innerText;
            let resultText = text.replace(/[^\x00-\x7F]/g, "");
            navigator.clipboard.writeText(resultText).then(() => {
                copyText.style.visibility = 'visible';
                copyText.style.opacity = '1';
            });
        });
        cssCopyButtons[i].addEventListener('mouseleave', () => {
            copyText.style.visibility = 'hidden';
            copyText.style.opacity = '0';
        });
    }

    let phpCopyButtons = document.getElementsByClassName('btn-copy-php');
    for (let i = 0; i < phpCopyButtons.length; i++) {
        let copyText = phpCopyButtons[i].querySelector('.tooltip__text');
        phpCopyButtons[i].addEventListener('click', () => {
            let styles = phpCopyButtons[i].previousElementSibling;
            let text = styles.innerText;
            let resultText = text.replace(/[^\x00-\x7F]/g, "");
            navigator.clipboard.writeText(resultText).then(() => {
                copyText.style.visibility = 'visible';
                copyText.style.opacity = '1';
            });
        });
        phpCopyButtons[i].addEventListener('mouseleave', () => {
            copyText.style.visibility = 'hidden';
            copyText.style.opacity = '0';
        });
    }
});