/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (index.css in this case)
import './../../styles/clear.css';
import '../../styles/admin/app.scss';
import './../../styles/admin/views/title.css';
import './../../styles/admin/index.css';

// start the Stimulus application
import './../../bootstrap';