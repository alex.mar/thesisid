import './../../../styles/clear.css';
import './../../../styles/preloader.css';
import '../../../styles/admin/app.scss';
import './../../../styles/admin/views/title.css';
import './../../../styles/admin/views/submenu.css';
import '../../../styles/admin/thesis/thesis_list_all.scss';

import './../../../bootstrap';

const LIMIT_PRODUCTS = 10;

document.addEventListener('DOMContentLoaded', function () {
    const searchInput = document.querySelector('.search__input');
    const listBody = document.querySelector('.table-list__body');
    const btnLoadMore = document.querySelector('.load-more__btn-load');
    const tableListBody = document.querySelector('.table-list__body');

    // btnLoadMore.addEventListener('click', (e) => {
    //     btnLoadMore.style.color = 'rgb(28 28 28 / 28%)';
    //     btnLoadMore.style.pointerEvents = 'none';
    //     let preloader = btnLoadMore.nextElementSibling;
    //     preloader.style.visibility = 'visible';
    //     let offset = tableListBody.rows.length;
    //     let query = searchInput.value;
    //     let lastRow = tableListBody.rows[tableListBody.rows.length - 1];
    //
    //     (async() => {
    //         const rawResponse = await fetch('/admin/thesis/list/all/load-more/', {
    //             method: 'POST',
    //             body: JSON.stringify({
    //                 offset: offset,
    //                 query: query,
    //                 lastId: lastRow.dataset.id
    //             })
    //         });
    //         const result = await rawResponse.json();
    //         let docs = result.docs;
    //         updateBtnLoadMore(docs)
    //         appendItems(docs);
    //         preloader.style.visibility = 'hidden';
    //     })();
    //
    //     return false;
    // });

    function createListElement(thesis)
    {
        const row = document.createElement('tr');
        row.className = 'table-list__row';
        row.dataset.id = thesis.id;
        let elementStatus = '<span class="table-list-btn-status_published"><i class="fa-solid fa-check table-list-btn-status__icon"></i> Published</span>';
        if (thesis.show === false) {
            elementStatus = '<span class="table-list-btn-status_progress"><i class="fa-solid fa-circle-pause table-list-btn-status__icon"></i> Progress</span>';
        }
        row.innerHTML = `<td class="table-list__body-col-id">` + thesis.id + `</td>
        <td class="table-list__body-col-title">` + thesis.title + `</td>
        <td class="table-list__body-col-type">` + thesis.type + `</td>
        <td class="table-list__body-col-status">`
        + elementStatus +
        `</td>`;
        row.addEventListener('click', () => {
            window.location='/admin/thesis/' + thesis.id + '/content/';
        });

        return row;
    }

    function appendItems(docs)
    {
        docs.forEach(item => {
            let row = createListElement(item);
            listBody.appendChild(row);
        });
    }

    function updateBtnLoadMore(docs)
    {
        if (docs.length < LIMIT_PRODUCTS) {
            btnLoadMore.style.pointerEvents = 'none';
            btnLoadMore.style.color = 'rgb(28 28 28 / 28%)';
        } else {
            btnLoadMore.style.pointerEvents = 'auto';
            btnLoadMore.style.color = '#1c1c1ccf';
        }
    }
});