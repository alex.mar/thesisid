import './../../../styles/clear.css';
import './../../../styles/select.css';
import '../../../styles/admin/app.scss';
import './../../../styles/admin/views/title.css';
import './../../../styles/admin/views/back_btn.css';
import './../../../styles/admin/views/submenu.css';
import '../../../styles/admin/views/form.scss';
import './../../../styles/views/hightlight.scss';
import '../../../styles/admin/thesis/thesis_content.scss';

import './../../../bootstrap';
import './../../select';

document.addEventListener('DOMContentLoaded', function () {
    // const btnCreateThesis = document.querySelector('.btn-create-thesis');
    // btnCreateThesis.addEventListener('click', formSend);

    const btnAddContent = document.querySelector('.btn-add-content');
    if (btnAddContent !== null) {
        btnAddContent.addEventListener('click', addFormContent);
    }

    const thesisTypeSelector = document.querySelector('.select-thesis-type');
    thesisTypeSelector.addEventListener('click', (e) => {
        if (!thesisTypeSelector.classList.contains('_active')) {
            addContentBlock(e);
        }
    });

    const btnDeleteThesis = document.querySelector('.btn-delete-thesis');
    if (btnDeleteThesis !== null) {
        btnDeleteThesis.addEventListener('click', deleteThesis);
    }

    const formWrapperItems = document.querySelectorAll('.form-wrapper');
    formWrapperItems.forEach(element => {
        element.addEventListener('click', function (e) {
            if (e.target && (e.target.matches('.form-item-header__btn') || e.target.matches('.form-item-header__icon'))) {
                if (e.target.matches('.btn-delete-item')) {
                    e.target.closest('.form__item').remove();
                } else {
                    let command;
                    if (e.target.matches('.form-item-header__icon')) {
                        let btn = e.target.closest('.form-item-header__btn');
                        command = btn.dataset.element;
                    } else {
                        command = e.target.dataset.element;
                    }
                    document.execCommand(command, false, null);
                }
            }
        });
    });

    function addContentBlock(e)
    {
        e.preventDefault();

        const thesisTypeSelector = document.getElementById('thesis-type-selector');
        let selectedType = thesisTypeSelector.value;
        let contentListWrapper = document.querySelector('.content-list-wrapper');
        let activeBlock = contentListWrapper.querySelector('.sub-block_active');
        if (selectedType !== activeBlock.dataset.thesisType) {
            activeBlock.style.display = 'none';
            activeBlock.classList.remove('sub-block_active');
            let selectedContentBlock = contentListWrapper.querySelector('[data-thesis-type="' + selectedType + '"]');
            selectedContentBlock.style.display = 'block';
            selectedContentBlock.classList.add('sub-block_active');
        }
    }

    function addFormContent(e)
    {
        e.preventDefault();
        const contentSelector = document.getElementById('content-selector');
        let selectedType = contentSelector.value;
        switch (selectedType) {
            case 'text':
                addThesisTextElement();
                break;
            case 'bash':
                addThesisBashElement();
                break;
            case 'css':
                addThesisCssElement();
                break;
            case 'php':
                addThesisPhpElement();
                break;
            case 'html':
                addThesisHtmlElement();
                break;
            default:
                break;
        }
    }

    async function deleteThesis(e)
    {
        e.preventDefault();
        if (confirm('Are you sure you want delete thesis?')) {
            let thesisId = btnDeleteThesis.dataset.thesisId
            let toDelete = []
            toDelete.push(thesisId)
            let response = await fetch('/admin/thesis/content/delete/', {
                method: 'POST',
                body: JSON.stringify({
                    thesisIds: toDelete
                })
            });
            if (response.ok) {
                let result = await response.json();
                alert(result.message);
                location.href = result.url;
            } else {
                alert('Error');
            }
        }
        return false;
    }

    function addThesisTextElement()
    {
        const div = document.createElement('div');
        div.className = 'form__item';
        div.innerHTML = `
            <label class="form__label">Text</label>
                <div class="form-item-header">
                    <div>
                        <button type="button" class="form-item-header__btn" data-element="bold"><i class="fa-solid fa-bold form-item-header__icon"></i></button>
                        <button type="button" class="form-item-header__btn" data-element="italic"><i class="fa-solid fa-italic form-item-header__icon"></i></button>
                        <button type="button" class="form-item-header__btn" data-element="insertUnorderedList"><i class="fa-solid fa-list-ul form-item-header__icon"></i></button>
                    </div>
                    <div>
                        <button type="button" class="form-item-header__btn btn-delete-item"><i class="fa-solid fa-trash form-item-header__icon btn-delete-item"></i></button>
                        <button type="button" class="form-item-header__btn btn-move-item"><i class="fa-solid fa-sort form-item-header__icon btn-move-item"></i></button>
                    </div>
                </div>
            <div class="form__textarea thesis-content-element text" contenteditable="true" data-type="text"></div>
        `;
        document.querySelector('.form-item-container').appendChild(div);
    }

    function addThesisBashElement()
    {
        const div = document.createElement('div');
        div.className = 'form__item';
        div.innerHTML = `
            <label class="form__label">Bash</label>
            <div class="form-item-header">
                <div>
                    <button type="button" class="form-item-header__btn" data-element="bold"><i class="fa-solid fa-bold form-item-header__icon"></i></button>
                </div>
                <div>
                    <button type="button" class="form-item-header__btn btn-delete-item"><i class="fa-solid fa-trash form-item-header__icon btn-delete-item"></i></button>
                    <button type="button" class="form-item-header__btn btn-move-item"><i class="fa-solid fa-sort form-item-header__icon btn-move-item"></i></button>
                </div>
            </div>
            <div class="form__textarea textarea-bash-bg thesis-content-element" contenteditable="true" data-type="bash"></div>
        `;
        document.querySelector('.form-item-container').appendChild(div);
    }

    function addThesisCssElement()
    {
        const div = document.createElement('div');
        div.className = 'form__item';
        div.innerHTML = `
            <label class="form__label">Css</label>
                <div class="form-item-header">
                    <div>
                    </div>
                    <div>
                        <button type="button" class="form-item-header__btn btn-delete-item"><i class="fa-solid fa-trash form-item-header__icon btn-delete-item"></i></button>
                        <button type="button" class="form-item-header__btn btn-move-item"><i class="fa-solid fa-sort form-item-header__icon btn-move-item"></i></button>
                    </div>
                </div>
            <div class="form__textarea textarea-css-bg thesis-content-element" contenteditable="true" data-type="css"></div>
        `;
        document.querySelector('.form-item-container').appendChild(div);
    }

    function addThesisPhpElement()
    {
        const div = document.createElement('div');
        div.className = 'form__item';
        div.innerHTML = `
            <label class="form__label">Php</label>
                <div class="form-item-header">
                    <div>
                    </div>
                    <div>
                        <button type="button" class="form-item-header__btn btn-delete-item"><i class="fa-solid fa-trash form-item-header__icon btn-delete-item"></i></button>
                        <button type="button" class="form-item-header__btn btn-move-item"><i class="fa-solid fa-sort form-item-header__icon btn-move-item"></i></button>
                    </div>
                </div>
            <div class="form__textarea textarea-php-bg thesis-content-element" contenteditable="true" data-type="php"></div>
        `;
        document.querySelector('.form-item-container').appendChild(div);
    }

    function addThesisHtmlElement()
    {
        const div = document.createElement('div');
        div.className = 'form__item';
        div.innerHTML = `
            <label class="form__label">Html</label>
                <div class="form-item-header">
                    <div>
                    </div>
                    <div>
                        <button type="button" class="form-item-header__btn btn-delete-item"><i class="fa-solid fa-trash form-item-header__icon btn-delete-item"></i></button>
                        <button type="button" class="form-item-header__btn btn-move-item"><i class="fa-solid fa-sort form-item-header__icon btn-move-item"></i></button>
                    </div>
                </div>
            <div class="form__textarea textarea-html-bg thesis-content-element" contenteditable="true" data-type="html"></div>
        `;
        document.querySelector('.form-item-container').appendChild(div);
    }
});
