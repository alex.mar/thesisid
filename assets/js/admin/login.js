import './../../styles/clear.css';
import './../../styles/preloader.css';
import '../../styles/admin/views/form.scss';
import '../../styles/admin/login.scss';

import './../../bootstrap';

import {disableLoadingBtnBlue, enableLoadingBtnBlue} from "../form";

document.addEventListener('DOMContentLoaded', () => {
    let btnLogin = document.querySelector('.login__btn');
    btnLogin.addEventListener('click', formSend);

    let formInputs = document.querySelectorAll('.form__input');
    for (let i = 0; i < formInputs.length; i++) {
        formInputs[i].addEventListener('keydown', clearError);
    }

    async function formSend(e)
    {
        e.preventDefault();
        let btnLogin = document.querySelector('.login__btn');
        disableLoadingBtnBlue(btnLogin);
        let formLogin = document.getElementById('form-login');
        let formData = new FormData(formLogin);
        formData.append('submit', JSON.stringify(true));

        let response = await fetch('/admin/login/', {
            method: 'POST',
            body: formData
        });
        enableLoadingBtnBlue(btnLogin);
        if (response.ok) {
            let result = await response.json();
            let status = result.status;
            if (status === 'success') {
                window.location.href = result.url;
            } else if (status === 'invalid') {
                let error = document.querySelector('.form__error');
                error.innerHTML = result.message;
                error.style.visibility = 'visible';
            }
        } else {
            alert('Error');
        }

        return false;
    }

    function clearError()
    {
        let error = document.querySelector('.form__error');
        error.style.visibility = 'hidden';
    }
});