import './../../../styles/clear.css';
import '../../../styles/admin/app.scss';
import './../../../styles/select.css';
import './../../../styles/admin/views/title.css';
import './../../../styles/admin/views/submenu.css';
import '../../../styles/admin/views/form.scss';
import './../../../styles/admin/seo/seo_main.css';

import './../../../bootstrap';
import './../../select';

document.addEventListener('DOMContentLoaded', function () {
    const btnSaveSeo = document.querySelector('.btn-save-seo');

    btnSaveSeo.addEventListener('click', formSend);

    async function formSend(e)
    {
        e.preventDefault();
        const formQuery = document.getElementById('form-seo-main');
        let formData = new FormData(formQuery);
        let response = await fetch('/admin/thesis/seo/main/save/', {
            method: 'POST',
            body: formData
        });
        if (response.ok) {
            let result = await response.json();
            alert(result.message);
            location.href = result.data.url;
        } else {
            alert('Error');
        }
        return false;
    }
});