/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (index.css in this case)
import '../icons/styles.css';
import './../styles/clear.css';
import './../styles/views/logo.scss';
import './../styles/app.css';

// start the Stimulus application
import './../bootstrap';

document.addEventListener('DOMContentLoaded', function () {
    const searchInput = document.querySelector('.search__input');
    searchInput.focus();
    const searchInputWrapper = document.querySelector('.search-input-wrapper');
    const btnSearch = document.querySelector('.search__btn-search');
    const btnClear = document.querySelector('.search__btn-clear');
    const searchComplete = document.querySelector('.search__complete');
    const wrapper = document.querySelector('.wrapper')

    btnSearch.addEventListener('click', (e) => {
        let query = searchInput.value.trim();
        if (query === '') {
            e.preventDefault();
            return false;
        }
    });

    searchInput.addEventListener('input', (e) => {
        let query = searchInput.value;
        if (query !== '') {
            searchComplete.classList.remove('lock');
            activeSearch();
            complete(e).then(result => {
                searchComplete.innerHTML = '';
                let complete = result.complete;
                if (typeof complete !== 'undefined' && complete.length > 0) {
                    complete.forEach(completeItem => {
                        let completeContainer = document.querySelector('.search__complete');
                        let completeElement = createCompleteItem(completeItem);
                        completeContainer.appendChild(completeElement);
                    });
                    if (!searchComplete.classList.contains('lock')) {
                        searchComplete.style.display = 'block';
                        searchInput.style.borderRadius = '20px 20px 10px 10px';
                        searchInputWrapper.style.borderRadius = '20px 20px 10px 10px';
                    }
                } else {
                    searchInput.style.borderRadius = '30px';
                    searchInputWrapper.style.borderRadius = '30px';
                    searchComplete.style.display = 'none';
                }
            });
        } else {
            searchComplete.classList.add('lock');
            clearSearch();
        }
    });
    searchInput.addEventListener('click', (e) => {
        searchComplete.classList.remove('lock');
        if (searchInput.value !== '') {
            activeSearch(true);
        }
    });
    wrapper.addEventListener('click', (e) => {
        if (!e.target.closest('.search-input-wrapper')) {
            clearSearch();
            searchComplete.classList.add('lock');
        }
    });
    btnClear.addEventListener('click', (e) => {
         searchInput.value = '';
         clearSearch();
         searchInput.focus();
         return false;
    });

    document.addEventListener('keydown', function (e) {
        if (e.which === 27) {
            clearSearch();
        }
        if (e.which === 40) {
            let completeItemFocus = document.querySelector('.complete-item-focus');
            if (completeItemFocus === null) {
                let completeItem = document.querySelector('.complete-item');
                completeItem.classList.add('complete-item-focus');
            } else {
                completeItemFocus.classList.remove('complete-item-focus');
                let nextCompleteItem = completeItemFocus.nextElementSibling;
                nextCompleteItem.classList.add('complete-item-focus');
            }
        }
        if (e.which === 38) {
            let completeItemFocus = document.querySelector('.complete-item-focus');
            if (completeItemFocus === null) {
                let completeItem = searchComplete.lastElementChild;
                completeItem.classList.add('complete-item-focus');
            } else {
                completeItemFocus.classList.remove('complete-item-focus');
                let nextCompleteItem = completeItemFocus.previousElementSibling;
                nextCompleteItem.classList.add('complete-item-focus');
            }
        }
        if (e.which === 13) {
            let completeItemFocus = document.querySelector('.complete-item-focus');
            if (completeItemFocus !== null) {
                let completeItemText = completeItemFocus.querySelector('.complete-item__text');
                let text = completeItemText.innerHTML;
                let clearValue = text.replace(/<\/?[^>]+(>|$)/g, '');
                searchInput.value = clearValue;
                clearSearch(false);
                window.location.href = window.location.href + "?q=" + clearValue;
            }
        }
    });

    async function complete (e) {
        e.preventDefault();
        let searchValue = searchInput.value;
        let response = await fetch('/complete/q=' + searchValue, {
            method: 'POST'
        });
        let result = {};
        if (response.ok) {
            result = await response.json();
        }
        return result;
    }

    function completeUpdate (completeId) {
        fetch('/complete/update/', {
            method: 'POST',
            body: JSON.stringify({
                completeId: completeId
            })
        })
    }

    function createCompleteItem(completeItem)
    {
        const div = document.createElement('div');
        div.className = 'complete-item';
        div.innerHTML = `<div class="complete-item__icon"><i class="icon-search-regular"></i></div>
                         <div class="complete-item__text">` + completeItem.query + `</div>`;
        div.dataset.completeId = completeItem.completeId;
        div.addEventListener('click', (e) => {
            let textElement = div.getElementsByClassName('complete-item__text')[0];
            let text = textElement.innerHTML;
            let clearValue = text.replace(/<\/?[^>]+(>|$)/g, '');
            searchInput.value = clearValue;
            clearSearch(false);
            completeUpdate(completeItem.completeId);
            window.location.href = window.location.href + '?q=' + clearValue + '&complete=' + completeItem.completeId;
        });

        return div;
    }

    function activeSearch(withComplete = false)
    {
        btnClear.style.visibility = 'visible';
        btnSearch.style.color = '#5a8dab';
        if (withComplete) {
            searchComplete.style.display = 'block';
        }
    }

    function clearSearch(withBtnClear = true)
    {
        btnSearch.style.color = '#8d8d8d';
        if (withBtnClear) {
            btnClear.style.visibility = 'hidden';
        }
        searchInput.style.borderRadius = '30px';
        searchInputWrapper.style.borderRadius = '30px';
        searchComplete.style.display = 'none';
    }
});

