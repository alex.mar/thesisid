export function disableLoadingBtnBlue(btn) {
    btn.style.backgroundColor = 'rgba(90,141,171,0.44)';
    btn.style.cursor = 'auto';
    btn.disabled = true;
    let preloader = btn.nextElementSibling;
    preloader.style.visibility = 'visible';
}

export function enableLoadingBtnBlue(btn) {
    btn.style.backgroundColor = '#5a8dab';
    btn.style.cursor = 'pointer';
    btn.disabled = false;
    let preloader = btn.nextElementSibling;
    preloader.style.visibility = 'hidden';
}