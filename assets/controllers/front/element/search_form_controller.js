import {Controller} from "@hotwired/stimulus";

export default class extends Controller {

    static targets = ['inputSearch', 'inputSearch', 'inputSearchWrapper', 'btnSearch', 'btnClear', 'completeBlock']

    connect() {
        let body = document.body
        body.addEventListener('click', (e) => {
            if (e.target !== this.inputSearchTarget) {
                this.clearSearch();
                this.completeBlockTarget.classList.add('lock');
            }
        })
    }

    handleComplete(e)
    {
        let completeItem = e.currentTarget
        let textComplete = completeItem.lastElementChild
        this.inputSearchTarget.value = textComplete.textContent;
        this.clearSearch(false);
        let completeId = completeItem.dataset.completeId
        fetch(e.params.urlUpdateRank, {
            method: 'POST',
            body: JSON.stringify({completeId: completeId})
        })
        window.location.href = location.protocol + '//' + location.host + location.pathname + '?q=' + textComplete.textContent + '&complete=' + completeId
    }

    clearSearch(withBtnClear = true)
    {
        this.btnSearchTarget.style.color = '#8d8d8d';
        if (withBtnClear) {
            this.btnClearTarget.style.visibility = 'hidden';
        }
        this.inputSearchTarget.style.borderRadius = '30px';
        this.completeBlockTarget.style.display = 'none';
    }

    showComplete(e)
    {
        let searchInput = e.currentTarget
        this.btnSearchTarget.style.color = '#5a8dab';
        if (searchInput.value !== '') {
            this.btnClearTarget.style.visibility = 'visible';
        }
        if (this.completeBlockTarget.children.length > 0) {
            searchInput.style.borderRadius = '25px 25px 0 0';
            this.completeBlockTarget.classList.remove('lock');
            this.completeBlockTarget.style.display = 'block';
        }
    }

    clearInputSearch()
    {
        this.inputSearchTarget.value = '';
        this.clearSearch();
        this.inputSearchTarget.focus();
    }

    checkEmptyInputSearch(e)
    {
        let query = this.inputSearchTarget.value.trim();
        if (query === '') {
            e.preventDefault();
            return false;
        }
    }
}