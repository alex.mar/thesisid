import { Controller } from "@hotwired/stimulus";
import {selectsInit} from "../../../../js/select";

export default class extends Controller {

    static targets = ['listElements']

    addListItemContent(e)
    {
        e.preventDefault()
        let parentElement = e.currentTarget.parentNode
        let formItem = parentElement.parentNode
        let selector = parentElement.firstChild.firstChild
        let elementEvent = this.dispatch('addListItemContent', {detail: {type: selector.value, withItemMenu: false}})
        let element = elementEvent.detail.element
        parentElement.replaceChild(element, parentElement.firstChild)
        e.currentTarget.remove()
        let elementLabel = element.getElementsByTagName('label')[0]
        let listItemContentLabel = formItem.firstElementChild
        listItemContentLabel.textContent = listItemContentLabel.textContent + ' ' + elementLabel.textContent
        elementLabel.remove()
    }

    addListItem(e)
    {
        e.preventDefault()
        let listItem = this.getListItem()
        this.listElementsTarget.appendChild(listItem)
        selectsInit()
    }

    getListItem()
    {
        let formItem = document.createElement('div')
            formItem.classList.add('form__item', 'form-wrapper');
        formItem.innerHTML = `
        <label class="form__label">List Item</label>
            <div class="form-item-header">
                <div></div>
                <div>
                    <button type="button" class="form-item-header__btn" data-action="admin--thesis--content--base#btnDeleteContentElement">
                        <i class="fa-solid fa-trash form-item-header__icon" data-action="admin--thesis--content--base#btnDeleteContentElement"></i>
                    </button>
                    <button type="button" class="form-item-header__btn btn-move-item">
                        <i class="fa-solid fa-sort form-item-header__icon btn-move-item"></i>
                    </button>
                </div>
            </div>
            <div class="form__textarea thesis-content-element-wrapper" contentEditable="false">
                <div class="form__item">
                    <label class="form__label">Title</label>
                    <input type="text" class="form__input thesis-title-element"/>
                </div>
                <div class="form__item">
                    <p class="form__label">Content</p>
                    <div>
                        <select id="content-selector" class="form__select select-content">
                            <option value="text">Text</option>
                            <option value="bash">Bash</option>
                            <option value="php">PHP</option>
                            <option value="html">HTML</option>
                            <option value="css">CSS</option>
                        </select>
                        <button class="form__btn btn-green btn-add-content"
                                data-action="click->admin--thesis--content--list-type#addListItemContent">Add Content
                        </button>
                    </div>
                </div>
            </div>`

        return formItem
    }
}