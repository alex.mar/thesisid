import { Controller } from "@hotwired/stimulus";
import Sortable from "sortablejs";

export default class extends Controller {

    static targets = ['btnDeleteElement', 'form']
    static classes = ['listElements', 'btnMoveItem', 'formItem', 'contentListWrapper', 'subBlock', 'subBlockActive']

    connect()
    {
        let listElements = document.getElementsByClassName(this.listElementsClass)
        for (let i = 0; i < listElements.length; i++) {
            Sortable.create(listElements[i], {
                handle: '.' + this.btnMoveItemClass,
                animation: 250,
            });
        }
    }

    btnDeleteContentElement(e)
    {
        e.currentTarget.closest('.' + this.formItemClass).remove();
    }

    save(e)
    {
        e.preventDefault()
        let params = e.params
        let form = this.formTarget
        let contentListWrapper = form.getElementsByClassName(this.contentListWrapperClass)[0]
        let contentBlocks = contentListWrapper.querySelectorAll('.' + this.subBlockClass)
        let thesisType;
        contentBlocks.forEach(element => {
            if (!element.classList.contains(this.subBlockActiveClass)) {
                element.remove();
            } else {
                thesisType = element.dataset.thesisType;
            }
        });
        let formData = new FormData(form);
        formData.append('thesis-type', thesisType);
        let thesisContentElements
        let contentElementOrder = 0
        switch (thesisType) {
            case 'list':
                thesisContentElements = document.querySelectorAll('.thesis-content-element-wrapper')
                contentElementOrder = 0;
                for (let i = 0; i < thesisContentElements.length; i++) {
                    let thesisContentItem = {};
                    let element = thesisContentElements[i]
                    if (element.hasAttribute('data-list-id')) {
                        thesisContentItem.listId = element.dataset.listId;
                    }
                    thesisContentItem.title = element.querySelector('.thesis-title-element').value
                    let currentContentElement = element.querySelector('.thesis-content-element')
                    if (currentContentElement !== null) {
                        thesisContentItem.content = currentContentElement.innerHTML
                        if (currentContentElement.hasAttribute('data-content-id')) {
                            thesisContentItem.contentId = currentContentElement.dataset.contentId;
                        }
                        thesisContentItem.type = currentContentElement.dataset.type;
                    } else {
                        thesisContentItem.content = '';
                    }
                    thesisContentItem.order = contentElementOrder;
                    formData.append('content[]', JSON.stringify(thesisContentItem))
                    contentElementOrder++;
                }
                break
            default:
                thesisContentElements = document.querySelectorAll('.thesis-content-element');
                contentElementOrder = 0;
                thesisContentElements.forEach(element => {
                    let thesisContentItem = {};
                    thesisContentItem.content = element.innerHTML;
                    thesisContentItem.order = contentElementOrder;
                    if (element.hasAttribute('data-content-id')) {
                        thesisContentItem.contentId = element.dataset.contentId;
                    }
                    thesisContentItem.type = element.dataset.type;
                    formData.append('content[]', JSON.stringify(thesisContentItem));
                    contentElementOrder++;
                });
                break
        }

        let url = new URL(document.URL)
        let generationId = url.searchParams.get('generationId')
        if (generationId !== null) {
            formData.append('generationId', generationId)
        }

        fetch(params.url, {
            method: 'POST',
            body: formData
        })
            .then(response => response.json())
            .then(response => {
                alert(response.message)
                if (response.status === 'success') {
                    location.href = response.data.url;
                }
            })
    }
}