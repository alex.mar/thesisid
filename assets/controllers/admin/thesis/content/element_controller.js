import { Controller } from "@hotwired/stimulus"

export default class extends Controller {

    initialize()
    {
        this.withItemMenu = true
    }

    buildContentElement(e)
    {
        let element
        let type = e.detail.type
        this.withItemMenu = e.detail.withItemMenu ?? true
        switch (type) {
            case 'text':
                element = this.addThesisTextElement()
                break
            case 'bash':
                element = this.addThesisBashElement()
                break
            case 'css':
                element = this.addThesisCssElement()
                break
            case 'php':
                element = this.addThesisPhpElement()
                break
            case 'html':
                element = this.addThesisHtmlElement()
                break
            default:
                break
        }

        e.detail.element = element
    }

    addThesisTextElement()
    {
        const div = document.createElement('div')
        div.className = 'form__item'
        let element = `
            <label class="form__label">Text</label>
                <div class="form-item-header">
                    <div>
                        <button type="button" class="form-item-header__btn" data-element="bold"><i class="fa-solid fa-bold form-item-header__icon"></i></button>
                        <button type="button" class="form-item-header__btn" data-element="italic"><i class="fa-solid fa-italic form-item-header__icon"></i></button>
                        <button type="button" class="form-item-header__btn" data-element="insertUnorderedList"><i class="fa-solid fa-list-ul form-item-header__icon"></i></button>
                    </div>
                    <div>`
        if (this.withItemMenu) {
            element += `<button type="button" class="form-item-header__btn btn-delete-item"><i class="fa-solid fa-trash form-item-header__icon btn-delete-item"></i></button>
                <button type="button" class="form-item-header__btn btn-move-item"><i class="fa-solid fa-sort form-item-header__icon btn-move-item"></i></button>`
        }
        element += `</div>
                </div>
            <div class="form__textarea thesis-content-element text" contenteditable="true" data-type="text"></div>
        `
        div.innerHTML = element
        return div
    }

    addThesisBashElement()
    {
        const div = document.createElement('div')
        div.className = 'form__item'
        let element = `
            <label class="form__label">Bash</label>
            <div class="form-item-header">
                <div>
                    <button type="button" class="form-item-header__btn" data-element="bold"><i class="fa-solid fa-bold form-item-header__icon"></i></button>
                </div>
                <div>`
        if (this.withItemMenu) {
            element += `<button type="button" class="form-item-header__btn btn-delete-item"><i class="fa-solid fa-trash form-item-header__icon btn-delete-item"></i></button>
                <button type="button" class="form-item-header__btn btn-move-item"><i class="fa-solid fa-sort form-item-header__icon btn-move-item"></i></button>`
        }
        element += `</div>
            </div>
            <div class="form__textarea textarea-bash-bg thesis-content-element" contenteditable="true" data-type="bash"></div>`
        div.innerHTML = element

        return div
    }

    addThesisCssElement()
    {
        const div = document.createElement('div')
        div.className = 'form__item'
        let element = `
            <label class="form__label">Css</label>
                <div class="form-item-header">
                    <div>
                    </div>
                    <div>`
        if (this.withItemMenu) {
            element += `<button type="button" class="form-item-header__btn btn-delete-item"><i class="fa-solid fa-trash form-item-header__icon btn-delete-item"></i></button>
                <button type="button" class="form-item-header__btn btn-move-item"><i class="fa-solid fa-sort form-item-header__icon btn-move-item"></i></button>`
        }
        element += `</div>
                </div>
            <div class="form__textarea textarea-css-bg thesis-content-element" contenteditable="true" data-type="css"></div>
        `
        div.innerHTML = element

        return div
    }

    addThesisPhpElement()
    {
        const div = document.createElement('div')
        div.className = 'form__item'
        let element = `
            <label class="form__label">Php</label>
                <div class="form-item-header">
                    <div>
                    </div>
                    <div>`
        if (this.withItemMenu) {
            element += `<button type="button" class="form-item-header__btn btn-delete-item"><i class="fa-solid fa-trash form-item-header__icon btn-delete-item"></i></button>
                <button type="button" class="form-item-header__btn btn-move-item"><i class="fa-solid fa-sort form-item-header__icon btn-move-item"></i></button>`
        }
        element += `</div>
                </div>
            <div class="form__textarea textarea-php-bg thesis-content-element" contenteditable="true" data-type="php"></div>
        `
        div.innerHTML = element

        return div
    }

    addThesisHtmlElement()
    {
        const div = document.createElement('div')
        div.className = 'form__item'
        let element = `
            <label class="form__label">Html</label>
                <div class="form-item-header">
                    <div>
                    </div>
                    <div>`
        if (this.withItemMenu) {
            element += `<button type="button" class="form-item-header__btn btn-delete-item"><i class="fa-solid fa-trash form-item-header__icon btn-delete-item"></i></button>
                <button type="button" class="form-item-header__btn btn-move-item"><i class="fa-solid fa-sort form-item-header__icon btn-move-item"></i></button>`
        }
                element += `</div>
            </div>
        <div class="form__textarea textarea-html-bg thesis-content-element" contenteditable="true" data-type="html"></div>
        `
        div.innerHTML = element

        return div
    }
    
}