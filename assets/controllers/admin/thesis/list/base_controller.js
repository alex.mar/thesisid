import { Controller } from "@hotwired/stimulus";

export default class extends Controller {

    static targets = ['listBody'];

    updateStatus(e)
    {
        let btnStatus = e.currentTarget
        let show = btnStatus.classList.contains('published') ? 0 : 1;
        let params = e.params
        let row = btnStatus.closest('tr')
        let thesisId = row.dataset.id
        fetch(params.urlUpdateStatus, {
            method: 'POST',
            body: JSON.stringify({
                thesisId: thesisId,
                show: show
            })
        })
            .then(response => response.json())
            .then(response => {
                let icon = document.createElement('i')
                icon.classList.add('fa-solid', 'btn-status__icon')
                let btnText
                if (show) {
                    btnStatus.classList.add('published')
                    btnStatus.classList.remove('progress')
                    icon.classList.add('fa-check')
                    btnText = 'Published'
                } else {
                    btnStatus.classList.add('progress')
                    btnStatus.classList.remove('published')
                    icon.classList.add('fa-circle-pause')
                    btnText = 'Progress'
                }
                btnStatus.innerHTML = icon.outerHTML + ' ' + btnText
            })
    }

    markAll(e)
    {
        let allCheckbox = this.listBodyTarget.querySelectorAll('input[type=checkbox]')
        let isChecked = !!e.currentTarget.checked;
        allCheckbox.forEach(checkbox => {
            checkbox.checked = isChecked
        })
    }

    deleteThesis(e)
    {
        e.preventDefault
        let allCheckbox = this.listBodyTarget.querySelectorAll('input[type=checkbox]:checked')
        let toDelete = []
        allCheckbox.forEach(checkbox => {
            toDelete.push(checkbox.closest('tr').dataset.id)
        })
        if (toDelete.length === 0) {
            alert('Thesis not selected')
        } else {
            fetch(e.params.urlDelete, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                },
                body: JSON.stringify({thesisIds: toDelete})
            })
                .then(response => response.json())
                .then(response => {
                    if (response.status === 'success') {
                        allCheckbox.forEach(checkbox => {
                            toDelete.push(checkbox.dataset.id)
                            checkbox.closest('tr').remove()
                        })
                    } else {
                        alert('Error. Thesis not deleted')
                    }
                })
        }
    }
}