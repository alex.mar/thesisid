import { Controller } from "@hotwired/stimulus";

export default class extends Controller {

    static targets = ['inputSearch', 'inputUrl', 'inputQuery', 'listBody', 'listCount', 'listMessage', 'listBtnLoadMore', 'listTable'];

    searchByContext(e)
    {
        e.preventDefault()
        let url = this.inputUrlTarget.value
        let query = this.inputQueryTarget.value
        if (url.length === 0 && query.length === 0) {
            this.inputUrlTarget.classList.add('error')
            this.inputQueryTarget.classList.add('error')
            return false
        } else {
            this.inputUrlTarget.classList.remove('error')
            this.inputQueryTarget.classList.remove('error')
        }
        let params = e.params
        fetch(params.url, {
            method: 'POST',
            body: JSON.stringify({
                source: url,
                query: query
            })
        })
            .then(response => response.text())
            .then(html => {
                this.listTableTarget.innerHTML = html;
                let countThesis = this.listBodyTarget.rows.length
                this.listCountTarget.textContent = countThesis;
                if (countThesis === 0) {
                    this.listMessageTarget.style.display = 'block'
                    this.listBtnLoadMoreTarget.style.display = 'none'
                } else {
                    this.listMessageTarget.style.display = 'none'
                    if (countThesis < 10) {
                        this.listBtnLoadMoreTarget.style.display = 'none'
                    } else {
                        this.listBtnLoadMoreTarget.style.display = 'block'
                    }
                }
            })
    }

    showBtnClose(e)
    {
        let inputSearch = e.currentTarget
        let searchBtnClose = inputSearch.nextElementSibling
        let searchValue = inputSearch.value
        if (searchValue === '') {
            searchBtnClose.style.visibility = 'hidden'
        } else {
            searchBtnClose.style.visibility = 'visible'
        }
    }

    clearSearchInput(e)
    {
        let btnClear = e.currentTarget
        let searchInput = btnClear.previousElementSibling
        searchInput.value = ''
        searchInput.focus()
        btnClear.style.visibility = 'hidden'
    }
}