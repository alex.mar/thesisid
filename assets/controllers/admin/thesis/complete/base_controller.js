import {Controller} from "@hotwired/stimulus";

export default class extends Controller {

    static targets = ['list']
    thesisId = this.element.dataset.thesisId

    connect() {
        this.loadCompletes()
    }

    loadCompletes()
    {
        fetch(this.element.dataset.urlLoad, {
            method: 'POST',
            body: JSON.stringify({thesisId: this.thesisId})
        })
            .then(response => response.text())
            .then(html => this.listTarget.innerHTML = html)
    }

    createQuery(e)
    {
        let btn = e.currentTarget
        let queryInput = btn.previousElementSibling
        let newQuery = queryInput.value.trim()
        if (newQuery === '') {
            alert('Add complete query')
            return false;
        }
        fetch(e.params.url, {
            method: 'POST',
            body: JSON.stringify({
                thesisId: this.thesisId,
                complete: newQuery
            })
        })
            .then(response => response.json())
            .then(response => {
                if (response.status === 'error') {
                    alert(response.message)
                } else {
                    queryInput.value = ''
                    this.loadCompletes()
                }
            })
    }

    deleteQuery(e)
    {
        let btn = e.currentTarget
        fetch(e.params.url, {
            method: 'POST',
            body: JSON.stringify({
                relationId: e.params.relationId
            })
        })
            .then(response => response.json())
            .then(response => {
                if (response.status === 'success') {
                    let row = btn.closest('li')
                    row.remove()
                } else {
                    alert(response.message)
                }
            })
    }
}