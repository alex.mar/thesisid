<?php

namespace App\Entity;

use App\Repository\ThesisStateRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'thesis_state')]
#[ORM\Entity(repositoryClass: ThesisStateRepository::class)]
class ThesisStateEntity
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'integer')]
    private int $thesisId;

    #[ORM\Column(type: 'integer')]
    private int $typeId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getThesisId(): ?int
    {
        return $this->thesisId;
    }

    public function setThesisId(int $thesisId): self
    {
        $this->thesisId = $thesisId;

        return $this;
    }

    public function getTypeId(): ?int
    {
        return $this->typeId;
    }

    public function setTypeId(int $typeId): self
    {
        $this->typeId = $typeId;

        return $this;
    }
}
