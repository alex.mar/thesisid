<?php

namespace App\Entity;

use App\Repository\ThesisCssRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Table;

#[Table(name: 'thesis_css')]
#[ORM\Entity(repositoryClass: ThesisCssRepository::class)]
class ThesisCssEntity
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'integer')]
    private int $thesisId;

    #[ORM\Column(type: 'string', length: 10000)]
    private string $style;

    #[ORM\Column(name: '`order`', type: 'integer')]
    private int $order;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getThesisId(): ?int
    {
        return $this->thesisId;
    }

    public function setThesisId(int $thesisId): self
    {
        $this->thesisId = $thesisId;

        return $this;
    }

    public function getStyle(): ?string
    {
        return $this->style;
    }

    public function setStyle(string $style): self
    {
        $this->style = $style;

        return $this;
    }

    public function getOrder(): ?int
    {
        return $this->order;
    }

    public function setOrder(int $order): self
    {
        $this->order = $order;

        return $this;
    }
}
