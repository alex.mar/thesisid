<?php

namespace App\Entity;

use App\Repository\GenerationRepository;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Table;

#[Table(name: 'generation')]
#[ORM\Entity(repositoryClass: GenerationRepository::class)]
class GenerationEntity
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'string', length: 255)]
    private string $link;

    #[ORM\Column(type: 'integer')]
    private int $completeId;

    #[ORM\Column(type: 'datetime')]
    private DateTimeInterface $generated;

    public function __construct()
    {
        $this->generated = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getCompleteId(): ?int
    {
        return $this->completeId;
    }

    public function setCompleteId(int $completeId): self
    {
        $this->completeId = $completeId;

        return $this;
    }

    public function getGenerated(): DateTimeInterface
    {
        return $this->generated;
    }

    public function setGenerated(DateTimeInterface $generated): self
    {
        $this->generated = $generated;

        return $this;
    }
}
