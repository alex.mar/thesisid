<?php

namespace App\Entity;

use App\Repository\ThesisBashRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Table;

#[Table(name: 'thesis_bash')]
#[ORM\Entity(repositoryClass: ThesisBashRepository::class)]
class ThesisBashEntity
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'integer')]
    private int $thesisId;

    #[ORM\Column(type: 'string', length: 100000)]
    private string $command;

    #[ORM\Column(name: '`order`', type: 'integer')]
    private int $order;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getThesisId(): ?int
    {
        return $this->thesisId;
    }

    public function setThesisId(int $thesisId): self
    {
        $this->thesisId = $thesisId;

        return $this;
    }

    public function getCommand(): ?string
    {
        return $this->command;
    }

    public function setCommand(string $command): self
    {
        $this->command = $command;

        return $this;
    }

    public function getOrder(): ?int
    {
        return $this->order;
    }

    public function setOrder(int $order): self
    {
        $this->order = $order;

        return $this;
    }
}
