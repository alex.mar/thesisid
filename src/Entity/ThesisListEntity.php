<?php

namespace App\Entity;

use App\Repository\ThesisListRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Table;

#[Table(name: 'thesis_list')]
#[ORM\Entity(repositoryClass: ThesisListRepository::class)]
class ThesisListEntity
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'integer')]
    private int $thesisId;

    #[ORM\Column(type: 'string', length: 255)]
    private string $title;

    #[ORM\Column(type: 'integer')]
    private ?int $contentTypeId = null;

    #[ORM\Column(type: 'integer')]
    private ?int $contentId = null;

    #[ORM\Column(name: '`order`', type: 'integer')]
    private int $order;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getThesisId(): int
    {
        return $this->thesisId;
    }

    public function setThesisId(int $thesisId): self
    {
        $this->thesisId = $thesisId;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContentTypeId(): ?int
    {
        return $this->contentTypeId;
    }

    public function setContentTypeId(?int $contentTypeId): self
    {
        $this->contentTypeId = $contentTypeId;

        return $this;
    }

    public function getContentId(): ?int
    {
        return $this->contentId;
    }

    public function setContentId(?int $contentId): self
    {
        $this->contentId = $contentId;

        return $this;
    }

    public function getOrder(): int
    {
        return $this->order;
    }

    public function setOrder(int $order): void
    {
        $this->order = $order;
    }
}
