<?php

namespace App\Entity;

use App\Repository\ThesisTextRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Table;

#[Table(name: 'thesis_text')]
#[ORM\Entity(repositoryClass: ThesisTextRepository::class)]
class ThesisTextEntity
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'integer')]
    private int $thesisId;

    #[ORM\Column(type: 'string', length: 100000)]
    private string $text;

    #[ORM\Column(name: '`order`', type: 'integer')]
    private int $order;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getThesisId(): ?int
    {
        return $this->thesisId;
    }

    public function setThesisId(int $thesisId): self
    {
        $this->thesisId = $thesisId;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getOrder(): int
    {
        return $this->order;
    }

    public function setOrder(int $order): self
    {
        $this->order = $order;

        return $this;
    }
}
