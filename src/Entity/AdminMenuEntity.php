<?php

namespace App\Entity;

use App\Repository\AdminMenuRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Table;

#[Table(name: 'admin_menu')]
#[ORM\Entity(repositoryClass: AdminMenuRepository::class)]
class AdminMenuEntity
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'string', length: 255)]
    private string $title;

    #[ORM\Column(type: 'string', length: 255)]
    private string $link;

    #[ORM\Column(type: 'string', length: 255)]
    private string $icon;

    #[ORM\Column(name: '`order`', type: 'integer')]
    private int $order;

    #[ORM\Column(type: 'string', length: 255, columnDefinition:"ENUM('yes', 'no')")]
    private string $show;

    #[ORM\Column(type: 'string', length: 255)]
    private string $block;

    #[ORM\Column(type: 'integer')]
    private int $parentId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function setIcon(string $icon): self
    {
        $this->icon = $icon;

        return $this;
    }

    public function getOrder(): int
    {
        return $this->order;
    }

    public function setOrder(int $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function getShow(): string
    {
        return $this->show;
    }

    public function setShow(string $show): void
    {
        $this->show = $show;
    }

    public function getBlock(): string
    {
        return $this->block;
    }

    public function setBlock(string $block): self
    {
        $this->block = $block;

        return $this;
    }

    public function getParentId(): int
    {
        return $this->parentId;
    }

    public function setParentId(int $parentId): self
    {
        $this->parentId = $parentId;

        return $this;
    }

    public function isShow(): bool
    {
        return $this->show === AdminMenuRepository::VALUE_ACTIVE_YES;
    }
}
