<?php

namespace App\Entity;

use App\Repository\ThesisCompleteRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Table;

#[Table(name: 'thesis_complete')]
#[ORM\Entity(repositoryClass: ThesisCompleteRepository::class)]
class ThesisCompleteEntity
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'integer')]
    private int $thesisId;

    #[ORM\Column(type: 'integer')]
    private int $completeId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getThesisId(): ?int
    {
        return $this->thesisId;
    }

    public function setThesisId(int $thesisId): self
    {
        $this->thesisId = $thesisId;

        return $this;
    }

    public function getCompleteId(): ?int
    {
        return $this->completeId;
    }

    public function setCompleteId(int $completeId): self
    {
        $this->completeId = $completeId;

        return $this;
    }
}
