<?php
declare(strict_types=1);

namespace App\Service\Menu;

use App\Entity\SearchMenuEntity;
use App\Repository\SearchMenuRepository;
use App\Repository\ThesisTypeRepository;
use App\Service\Search\Context\VO\SearchContextVO;
use App\Service\Solr\Response\SolrResponseCollection;
use App\Service\ThesisType\ThesisTypeService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class SearchMenuService
{
    private Request $request;

    public function __construct(
        private RequestStack $requestStack,
        private ThesisTypeService $thesisTypeService,
        private SearchMenuRepository $searchMenuRepository
    ) {
        $this->request = $this->requestStack->getCurrentRequest();
    }

    public function loadMenu(SearchContextVO $searchContextVO, SolrResponseCollection $solrResponseCollection): array
    {
        $result = [];

        $searchMenuEntities = $this->searchMenuRepository->findAll();
        $indexedSearchMenuEntities = [];
        foreach ($searchMenuEntities as $entity) {
            $indexedSearchMenuEntities[$entity->getThesisTypeId()] = $entity;
        }

        $allTypes = $this->thesisTypeService->getAll();
        $indexedAllTypes = [];
        foreach ($allTypes as $entity) {
            $indexedAllTypes[$entity->getId()] = $entity->getName();
        }
        $indexedAllTypes = array_flip($indexedAllTypes);

        $thesisId = $this->request->get('id') ?? null;
        $selectedType = $searchContextVO->getThesisType();
        $isSelected = false;
        foreach ($solrResponseCollection->getArrayCopy() as $solrResponse) {
            $active = false;
            if ($selectedType->getName() === ThesisTypeRepository::TYPE_DEFAULT && $solrResponse->getNumFound() === 0) {
                continue;
            }
            $navigation = $searchContextVO->getSearchNavigationVOCollection()->getNavigation($solrResponse->getType());
            if ($solrResponse->getType() !== ThesisTypeRepository::TYPE_DEFAULT
                && $solrResponse->getNumFound() <= $navigation->getLimit()
                && $selectedType->getName() !== $solrResponse->getType()) {
                continue;
            }
            $currentType = $solrResponse->getType();
            $link = $this->buildUrl($currentType);
            if (isset($indexedSearchMenuEntities[$indexedAllTypes[$currentType]])) {
                $currentSearchMenuEntity = $indexedSearchMenuEntities[$indexedAllTypes[$currentType]];
                if ($selectedType->getName() === $currentType && $thesisId === null && !$isSelected) {
                    $active = true;
                    $isSelected = true;
                }
                $result[$currentType] = [
                    'title' => $currentSearchMenuEntity->getDisplayName(),
                    'link' => $link,
                    'icon' => $currentSearchMenuEntity->getIcon(),
                    'active' => $active,
                    'total' => $solrResponse->getNumFound()
                ];
            }
        }
        if (!$isSelected && isset($result[ThesisTypeRepository::TYPE_DEFAULT]) && $thesisId === null) {
            $result[ThesisTypeRepository::TYPE_DEFAULT]['active'] = true;
        }

        if (!isset($result[ThesisTypeRepository::TYPE_DEFAULT])) {
            $searchMenuEntity = $indexedSearchMenuEntities[$indexedAllTypes[ThesisTypeRepository::TYPE_DEFAULT]];
            $link = $this->buildUrl(ThesisTypeRepository::TYPE_DEFAULT);
            $newResult[ThesisTypeRepository::TYPE_DEFAULT] = [
                'title' => $searchMenuEntity->getDisplayName(),
                'link' => $link,
                'icon' => $searchMenuEntity->getIcon(),
                'active' => true,
                'total' => 0
            ];
            if (count($result) === 1) {
                return $newResult;
            }
            if (!empty($result)) {
                $newResult += $result;
            }

            return $newResult;
        }

        $sortedMenu = [];
        $indexedAllTypes = array_flip($indexedAllTypes);
        foreach ($indexedSearchMenuEntities as $thesisTypeId => $entity) {
            $currentThesisType = $indexedAllTypes[$thesisTypeId];
            if (isset($result[$currentThesisType])) {
                $sortedMenu[$currentThesisType] = $result[$currentThesisType];
            }
        }

        return $sortedMenu;
    }

    private function buildUrl(string $type): string
    {
        $params = $_GET;
        if ($type === ThesisTypeRepository::TYPE_DEFAULT) {
            unset($params['type']);
        } else {
            $params['type'] = $type;
        }
        if (!isset($params['q'])) {
            $query = $this->request->cookies->get('query') ?? '';
            if (!empty($query)) {
                $params['q'] = $query;
            }
        }
        $domainUrlPart = '/?' . http_build_query($params);
        return rtrim($domainUrlPart, '?');
    }

    /**
     * @param array $menu
     * @param SearchMenuEntity[] $searchMenuEntities
     * @return array
     */
    private function sort(array $menu, array $searchMenuEntities): array
    {
        $result = [];
        foreach ($searchMenuEntities as $entity) {
            if (isset($menu[$entity->getName()])) {

            }
        }
    }
}
