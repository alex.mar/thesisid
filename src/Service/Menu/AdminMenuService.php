<?php
declare(strict_types=1);

namespace App\Service\Menu;

use App\Repository\AdminMenuRepository;
use Symfony\Component\HttpFoundation\Request;

class AdminMenuService
{
    public function __construct(
        private AdminMenuRepository $adminMenuRepository,
    ) {
    }

    public function loadMenu(Request $request): array
    {
        $result = [];
        $currentUrl = trim($request->getRequestUri(), '/');
        $currentUrl = strtok($currentUrl, '?');
        $currentUrlParts = array_filter(explode('/', $currentUrl));
        $currentItemMenuId = (int) filter_var($currentUrl, FILTER_SANITIZE_NUMBER_INT);

        $maxCountIntersect = 0;
        $menuItems = $this->adminMenuRepository->getAllMainMenuItems();
        $isSelectedMain = false;
        $activeMenuItem = null;
        $withMaxCountCheck = true;
        $hiddenMaxCountIntersect = 0;
        foreach ($menuItems as $menuItem) {
            $currentLink = $menuItem->getLink();
            if (!empty($currentItemMenuId)) {
                $currentLink = $this->formatSubLink($menuItem->getLink(), $currentItemMenuId);
            }
            $menuItemUrlParts = array_filter(explode('/', $currentLink));
            $currentIntersect = array_intersect($currentUrlParts, $menuItemUrlParts);
            $countIntersect = count($currentIntersect);
            if ($countIntersect > $maxCountIntersect) {
                $maxCountIntersect = $countIntersect;
            }
            $link = $this->formatLink($menuItem->getLink());
            $isActive = false;
            if ($currentUrl === trim($link, '/')
                || ($countIntersect > 1 && $withMaxCountCheck && $maxCountIntersect === $countIntersect)
                || (!$withMaxCountCheck && $countIntersect > 1)) {
                $isSelectedMain = true;
                if ($withMaxCountCheck || $maxCountIntersect > $hiddenMaxCountIntersect) {
                    $activeMenuItem = $menuItem;
                }
                if ($menuItem->isShow()) {
                    $isActive = true;
                } else {
                    $withMaxCountCheck = false;
                    $hiddenMaxCountIntersect = $maxCountIntersect;
                }
            }
            $result[$menuItem->getBlock()][] = [
                'title' => $menuItem->getTitle(),
                'link' => $link,
                'icon' => $menuItem->getIcon(),
                'active' => $isActive,
                'show' => $menuItem->getShow()
            ];
        }
        $submenuItems = $this->adminMenuRepository->getAllSubMenuItems($activeMenuItem->getId());
        if ($isSelectedMain && !empty($submenuItems)) {
            $isSelectedSubmenu = false;
            foreach ($submenuItems as $entity) {
                $link = $entity->getLink();
                if (!empty($currentItemMenuId)) {
                    $link = $this->formatSubLink($entity->getLink(), $currentItemMenuId);
                }
                $isSubmenuActive = false;
                if ($currentUrl === ltrim($link, '/')) {
                    $isSelectedSubmenu = true;
                    $isSubmenuActive = true;
                }
                $result['submenu'][] = [
                    'title' => $entity->getTitle(),
                    'link' => $this->formatLink($link),
                    'icon' => $entity->getIcon(),
                    'active' => $isSubmenuActive
                ];
            }
            if (!$isSelectedSubmenu) {
                $result['submenu'][0]['active'] = true;
            }
        } else {
            $result['submenu'] = [];
        }

        return $result;
    }

    private function formatLink(string $link): string
    {
        return '/' . trim($link, '/') . '/';
    }

    private function formatSubLink(string $link, int $currentItemMenuId): string
    {
        return str_replace('{id}', (string) $currentItemMenuId, $link);
    }
}
