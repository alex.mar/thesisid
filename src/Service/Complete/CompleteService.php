<?php
declare(strict_types=1);

namespace App\Service\Complete;

use App\Repository\CompleteRepository;
use App\Service\Search\SearchService;
use App\Service\Search\VO\CompleteVO;
use SolrClientException;
use SolrServerException;

class CompleteService
{
    public function __construct(
        private CompleteRepository $completeRepository,
        private SearchService $searchService
    ) {
    }

    public function compare(string $requestQuery, string $completeQuery): bool
    {
        $isCurrent = false;
        $requestQueryArray = explode(' ', $requestQuery);
        $completeQueryArray = explode(' ', $completeQuery);
        $diff = array_diff($requestQueryArray, $completeQueryArray);
        if (empty($diff) && count($requestQueryArray) === count($completeQueryArray)) {
            $isCurrent = true;
        }

        return $isCurrent;
    }

    public function compareByCompleteId(string $requestQuery, int $completeId): bool
    {
        $isCurrent = false;
        $completeEntity = $this->completeRepository->find($completeId);
        if ($completeEntity !== null) {
            $isCurrent = $this->compare($requestQuery, $completeEntity->getQuery());
        }

        return $isCurrent;
    }

    /**
     * @throws SolrClientException
     * @throws SolrServerException
     */
    public function isExistComplete(string $query): ?CompleteVO
    {
        $result = null;
        $completeVOs = $this->searchService->searchStrictComplete($query);
        foreach ($completeVOs as $completeVO) {
            if ($this->compare($query, $completeVO->getQuery())) {
                $result = $completeVO;
            }
        }

        return $result;
    }

    /**
     * @param string $query
     * @return CompleteVO[]
     * @throws SolrClientException
     * @throws SolrServerException
     */
    public function getSearchCompletes(string $query): array
    {
        $completeVOs = $this->searchService->searchComplete($query);
        $queryParts = explode(' ', $query);
        $formattedCompleteVOs = [];
        foreach ($completeVOs as $completeVO) {
            $complete = $completeVO->getQuery();
            if ($complete === $query) {
                continue;
            }
            if (!str_starts_with($complete, $query)) {
                $completeParts = explode(' ', $complete);
                $convertedComplete = [];
                foreach ($queryParts as $queryPart) {
                    if (empty($queryPart)) {
                        continue;
                    }
                    foreach ($completeParts as $key => $completePart) {
                        if (str_starts_with($completePart, $queryPart)) {
                            $convertedComplete[] = $completePart;
                            unset($completeParts[$key]);
                            continue 2;
                        }
                    }
                }
                foreach ($completeParts as $completePart) {
                    $convertedComplete[] = $completePart;
                }
                $complete = implode(' ', $convertedComplete);
            }
            $formattedComplete = mb_strtolower($complete);
            $formattedComplete = $this->highlightComplete($query, $formattedComplete);
            $formattedCompleteVOs[] = new CompleteVO($completeVO->getCompleteId(), $formattedComplete);
        }

        return $formattedCompleteVOs;
    }

    public function highlightComplete(string $query, string $complete): string
    {
        $posStart = mb_strlen($query);
        $firstPart = mb_substr($complete, 0, $posStart);
        $secondPart = mb_substr($complete, $posStart);
        if (!empty($secondPart)) {
            $secondPart = '<b>' . $secondPart . '</b>';
        }
        return $firstPart . $secondPart;
    }
}
