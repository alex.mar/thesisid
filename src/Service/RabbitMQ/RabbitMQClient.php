<?php
declare(strict_types=1);

namespace App\Service\RabbitMQ;

use App\Service\RabbitMQ\Task\TaskInterface;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class RabbitMQClient
{
    public function __construct(
        private AMQPStreamConnection $connection
    ){
    }

    public function doTask(TaskInterface $task): void
    {
        $chanel = $this->connection->channel();
        $chanel->queue_declare($task->getQueueName(), false, true, false, false);
        $msq = new AMQPMessage(json_encode($task->getWorkData()), '');
        $chanel->basic_publish($msq, '', $task->getQueueName());
    }
}
