<?php
declare(strict_types=1);

namespace App\Service\RabbitMQ\Task;

class Task implements TaskInterface
{
    public function __construct(
        private string $queueName,
        private array $workData
    ){
    }

    public function getQueueName(): string
    {
        return $this->queueName;
    }

    public function getWorkData(): array
    {
        return $this->workData;
    }
}
