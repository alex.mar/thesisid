<?php
declare(strict_types=1);

namespace App\Service\RabbitMQ\Task;

interface TaskInterface
{
    public function getQueueName(): string;
    public function getWorkData(): array;
}