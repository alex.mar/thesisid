<?php
declare(strict_types=1);

namespace App\Service\Thesis\ContentEntity\VO;

use App\Entity\ThesisBashEntity;
use App\Entity\ThesisCssEntity;
use App\Entity\ThesisHtmlEntity;
use App\Entity\ThesisListEntity;
use App\Entity\ThesisPhpEntity;
use App\Entity\ThesisTextEntity;

class ThesisContentEntityVO
{
    public function __construct(
        private string $type,
        private ThesisBashEntity|ThesisCssEntity|ThesisHtmlEntity|ThesisListEntity|ThesisPhpEntity
        |ThesisTextEntity $entity
    ) {
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    public function getEntity(): ThesisBashEntity|ThesisCssEntity|ThesisHtmlEntity|ThesisListEntity|ThesisPhpEntity
    |ThesisTextEntity
    {
        return $this->entity;
    }
}
