<?php
declare(strict_types=1);

namespace App\Service\Thesis\ContentEntity;

use App\Entity\ThesisBashEntity;
use App\Entity\ThesisCssEntity;
use App\Entity\ThesisEntity;
use App\Entity\ThesisHtmlEntity;
use App\Entity\ThesisListEntity;
use App\Entity\ThesisPhpEntity;
use App\Entity\ThesisTextEntity;
use App\Repository\ThesisBashRepository;
use App\Repository\ThesisCssRepository;
use App\Repository\ThesisHtmlRepository;
use App\Repository\ThesisListRepository;
use App\Repository\ThesisPhpRepository;
use App\Repository\ThesisTextRepository;
use App\Repository\ThesisTypeRepository;
use App\Service\Thesis\ContentEntity\VO\ThesisContentEntityVO;
use App\Service\ThesisType\ThesisTypeService;

class ContentEntityLoader
{
    public function __construct(
        private ThesisTypeService $thesisTypeService,
        private ThesisTextRepository     $thesisTextRepository,
        private ThesisBashRepository     $thesisBashRepository,
        private ThesisCssRepository     $thesisCssRepository,
        private ThesisPhpRepository     $thesisPhpRepository,
        private ThesisHtmlRepository     $thesisHtmlRepository,
        private ThesisListRepository $thesisListRepository,
    ) {
    }

    public function loadById(string $contentType, int $contentId): ThesisContentEntityVO
    {
        $entity = match ($contentType) {
            'text' => $this->thesisTextRepository->find($contentId),
            'bash' => $this->thesisBashRepository->find($contentId),
            'css' => $this->thesisCssRepository->find($contentId),
            'php' => $this->thesisPhpRepository->find($contentId),
            'html' => $this->thesisHtmlRepository->find($contentId),
            'list' => $this->thesisListRepository->find($contentId),
        };

        return new ThesisContentEntityVO($contentType, $entity);
    }

    /**
     * @param ThesisEntity[] $thesisEntities
     * @return ThesisContentEntityVO[]
     */
    public function loadByThesisIds(array $thesisEntities): array
    {
        $groupedThesisEntities = [];
        foreach ($thesisEntities as $entity) {
            $groupedThesisEntities[$entity->getTypeId()][] = $entity;
        }
        $thesisContentEntityVOs = [];
        foreach ($groupedThesisEntities as $typeId => $entities) {
            $thesisIds = [];
            /** @var ThesisEntity $entity */
            foreach ($entities as $entity) {
                $thesisIds[] = $entity->getId();
            }
            $thesisTextItems = [];
            $thesisBashItems = [];
            $thesisCssItems = [];
            $thesisPhpItems = [];
            $thesisHtmlItems = [];
            $thesisListItems = [];
            $type = $this->thesisTypeService->getThesisTypeNameById($typeId);
            switch ($type) {
                case ThesisTypeRepository::TYPE_LIST:
                    $thesisListItems = $this->thesisListRepository->findBy(['thesisId' => $thesisIds]);
                    break;
                case ThesisTypeRepository::TYPE_DEFAULT:
                case ThesisTypeRepository::TYPE_DEFINITION:
                    $thesisTextItems = $this->thesisTextRepository->findBy(['thesisId' => $thesisIds]);
                    $thesisBashItems = $this->thesisBashRepository->findBy(['thesisId' => $thesisIds]);
                    $thesisCssItems = $this->thesisCssRepository->findBy(['thesisId' => $thesisIds]);
                    $thesisPhpItems = $this->thesisPhpRepository->findBy(['thesisId' => $thesisIds]);
                    $thesisHtmlItems = $this->thesisHtmlRepository->findBy(['thesisId' => $thesisIds]);
                    break;
                default:
                    break;
            }
            $thesisContentEntityVOs = $this->appendEntities('text', $thesisTextItems, $thesisContentEntityVOs);
            $thesisContentEntityVOs = $this->appendEntities('bash', $thesisBashItems, $thesisContentEntityVOs);
            $thesisContentEntityVOs = $this->appendEntities('css', $thesisCssItems, $thesisContentEntityVOs);
            $thesisContentEntityVOs = $this->appendEntities('php', $thesisPhpItems, $thesisContentEntityVOs);
            $thesisContentEntityVOs = $this->appendEntities('html', $thesisHtmlItems, $thesisContentEntityVOs);
            $thesisContentEntityVOs = $this->appendEntities('list', $thesisListItems, $thesisContentEntityVOs);
        }

        return $thesisContentEntityVOs;
    }

    /**
     * @param string $type
     * @param ThesisBashEntity|ThesisCssEntity|ThesisHtmlEntity|ThesisListEntity|ThesisPhpEntity|ThesisTextEntity $entities
     * @param ThesisContentEntityVO[] $thesisContentEntityVOs
     * @return ThesisContentEntityVO[]
     */
    private function appendEntities(string $type, array $entities, array $thesisContentEntityVOs): array
    {
        foreach ($entities as $entity) {
            $thesisContentEntityVOs[] = new ThesisContentEntityVO($type, $entity);
        }

        return $thesisContentEntityVOs;
    }
}
