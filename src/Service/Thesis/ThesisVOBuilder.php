<?php
declare(strict_types=1);

namespace App\Service\Thesis;

use App\Entity\ThesisEntity;
use App\Repository\ThesisCompleteRepository;
use App\Repository\ThesisRepository;
use App\Repository\ThesisMetaRepository;
use App\Service\Thesis\ContentEntity\ContentEntityLoader;
use App\Service\Thesis\VO\ThesisContentVO;
use App\Service\Thesis\VO\ThesisVO;
use App\Service\ThesisType\ThesisTypeService;
use Exception;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ThesisVOBuilder
{
    private bool $withHighlight = true;

    public function __construct(
        private ThesisRepository         $thesisRepository,
        private ThesisCompleteRepository $thesisCompleteRepository,
        private ThesisMetaRepository     $thesisMetaRepository,
        private UrlGeneratorInterface    $router,
        private ThesisTypeService        $thesisTypeService,
        private ThesisContentVOBuilder   $thesisContentVOBuilder,
        private ContentEntityLoader      $contentEntityLoader
    ) {
    }

    public function buildById(int $thesisId, bool $isPublished = true): ?ThesisVO
    {
        $params = ['id' => $thesisId];
        if ($isPublished) {
            $params['status'] = ThesisRepository::STATUS_PUBLISHED;
        }
        $thesisEntity = $this->thesisRepository->findOneBy($params);
        if ($thesisEntity === null) {
            return null;
        }
        $thesisQueryVOs = $this->thesisCompleteRepository->getByThesisId($thesisId);
        $thesisContent = $this->getContentVOs([$thesisEntity]);
        $thesisContent = $this->sortContent($thesisContent);
        $thesisMetaEntity = $this->thesisMetaRepository->findOneBy(['thesisId' => $thesisId]);

        return new ThesisVO(
            $thesisEntity->getId(),
            $this->thesisTypeService->getThesisTypeNameById($thesisEntity->getTypeId()),
            $this->router->generate(
                'app_thesis',
                ['id' => $thesisEntity->getId()],
                UrlGeneratorInterface::ABSOLUTE_URL
            ),
            $thesisEntity->getTitle(),
            $thesisMetaEntity,
            $thesisContent,
            $thesisEntity->getSource(),
            $thesisEntity->isPublished(),
            $thesisQueryVOs
        );
    }

    public function buildByIds(array $ids, bool $isPublished = true): ThesisVOCollection
    {
        $thesisVOCollection = new ThesisVOCollection();
        if (empty($ids)) {
            return $thesisVOCollection;
        }
        $params = ['id' => $ids];
        if ($isPublished) {
            $params['status'] = ThesisRepository::STATUS_PUBLISHED;
        }
        $thesisEntities = $this->thesisRepository->findBy($params);
        if (empty($thesisEntities)) {
            return new ThesisVOCollection();
        }
        $thesisQueryVOs = $this->thesisCompleteRepository->getCompletesByThesisIds($ids);
        $allThesisContent = $this->getContentVOs($thesisEntities);
        $groupedThesisContent = [];
        foreach ($allThesisContent as $thesisContentVO) {
            $groupedThesisContent[$thesisContentVO->getThesisId()][] = $thesisContentVO;
        }

        $thesisMetaEntities = $this->thesisMetaRepository->findBy(['thesisId' => $ids]);
        $indexedThesisMetaEntities = [];
        foreach ($thesisMetaEntities as $entity) {
            $indexedThesisMetaEntities[$entity->getThesisId()] = $entity;
        }

        $indexedThesisEntities = [];
        foreach ($thesisEntities as $entity) {
            $indexedThesisEntities[$entity->getId()] = $entity;
        }
        foreach ($ids as $id) {
            if (isset($indexedThesisEntities[$id])) {
                $thesisEntity = $indexedThesisEntities[$id];
                $url = $this->router->generate(
                    'app_thesis',
                    ['id' => $thesisEntity->getId()],
                    UrlGeneratorInterface::ABSOLUTE_URL
                );
                if (!isset($indexedThesisMetaEntities[$id]) || !isset($groupedThesisContent[$id])) {
                    continue;
                }
                $thesisContent = $groupedThesisContent[$id];
                $thesisContent = $this->sortContent($thesisContent);
                $currentThesisQueryVOs = $thesisQueryVOs[$id] ?? [];

                $thesisVO = new ThesisVO(
                    $id,
                    $this->thesisTypeService->getThesisTypeNameById($thesisEntity->getTypeId()),
                    $url,
                    $thesisEntity->getTitle(),
                    $indexedThesisMetaEntities[$id],
                    $thesisContent,
                    $thesisEntity->getSource(),
                    $thesisEntity->isPublished(),
                    $currentThesisQueryVOs
                );
                $thesisVOCollection->append($thesisVO);
            }
        }

        return $thesisVOCollection;
    }

    /**
     * @param ThesisContentVO[] $thesisContentVOs
     * @return ThesisContentVO[]
     */
    private function sortContent(array $thesisContentVOs): array
    {
        usort($thesisContentVOs, function (ThesisContentVO $a, ThesisContentVO $b) {
            if ($a->getOrder() === $b->getOrder()) {
                return 0;
            }
            return $a->getOrder() < $b->getOrder() ? -1 : 1;
        });

        return $thesisContentVOs;
    }

    /**
     * @param ThesisEntity[] $thesisEntities
     * @return ThesisContentVO[]
     * @throws Exception
     */
    public function getContentVOs(array $thesisEntities): array
    {
        $contentEntityVOs = $this->contentEntityLoader->loadByThesisIds($thesisEntities);
        return $this->thesisContentVOBuilder->buildFromEntities($contentEntityVOs, $this->withHighlight);
    }

    public function isWithHighlight(): bool
    {
        return $this->withHighlight;
    }

    public function setWithHighlight(bool $withHighlight): void
    {
        $this->withHighlight = $withHighlight;
    }
}
