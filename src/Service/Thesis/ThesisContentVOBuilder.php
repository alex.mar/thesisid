<?php
declare(strict_types=1);

namespace App\Service\Thesis;

use App\Service\Thesis\ContentEntity\ContentEntityLoader;
use App\Service\Thesis\ContentEntity\VO\ThesisContentEntityVO;
use App\Service\Thesis\Formatter\ContentType\BashContentTypeFormatter;
use App\Service\Thesis\Formatter\ContentType\CssContentTypeFormatter;
use App\Service\Thesis\Formatter\ContentType\HtmlContentTypeFormatter;
use App\Service\Thesis\Formatter\ContentType\ListContentTypeFormatter;
use App\Service\Thesis\Formatter\ContentType\PhpContentTypeFormatter;
use App\Service\Thesis\Formatter\ContentType\TextContentTypeFormatter;
use App\Service\Thesis\VO\ThesisContentVO;
use App\Service\ThesisContentType\ThesisContentTypeService;
use Exception;

class ThesisContentVOBuilder
{
    public function __construct(
        private ThesisContentTypeService $thesisContentTypeService,
        private ContentEntityLoader $contentEntityLoader
    ) {
    }

    /**
     * @param string[] $contentItems
     * @param int $thesisId
     * @return ThesisContentVO[]
     */
    public function buildFromJson(array $contentItems, int $thesisId): array
    {
        $thesisContentVOs = [];
        foreach ($contentItems as $index => $contentItemJson) {
            $contentItem = json_decode($contentItemJson, true);
            if (!isset($contentItem['type']) || empty($contentItem['content'])) {
                continue;
            }
            $id = isset($contentItem['contentId']) ? (int) $contentItem['contentId'] : null;
            $formatter = match ($contentItem['type']) {
                'text' => new TextContentTypeFormatter($contentItem['content']),
                'bash' => new BashContentTypeFormatter($contentItem['content'], false),
                'css' => new CssContentTypeFormatter($contentItem['content'], false),
                'html' => new HtmlContentTypeFormatter($contentItem['content'], false),
                'php' => new PhpContentTypeFormatter($contentItem['content'], false),
            };
            $thesisContentVOs[$index] = new ThesisContentVO(
                $id,
                $thesisId,
                $contentItem['type'],
                (int) ($contentItem['order'] ?? 0),
                $formatter->format()
            );
        }

        return $thesisContentVOs;
    }

    /**
     * @param ThesisContentEntityVO[] $thesisContentEntityVOs
     * @param bool $withHighlight
     * @return ThesisContentVO[]
     * @throws Exception
     */
    public function buildFromEntities(array $thesisContentEntityVOs, bool $withHighlight): array
    {
        $thesisContentVOs = [];
        foreach ($thesisContentEntityVOs as $contentEntityVO) {
            $formatter = null;
            $entity = $contentEntityVO->getEntity();
            switch ($contentEntityVO->getType()) {
                case 'text':
                    $formatter = new TextContentTypeFormatter($entity->getText());
                    break;
                case 'bash':
                    $formatter = new BashContentTypeFormatter($entity->getCommand(), $withHighlight);
                    break;
                case 'css':
                    $formatter = new CssContentTypeFormatter($entity->getStyle(), $withHighlight);
                    break;
                case 'html':
                    $formatter = new HtmlContentTypeFormatter($entity->getCode(), $withHighlight);
                    break;
                case 'php':
                    $formatter = new PhpContentTypeFormatter($entity->getCode(), $withHighlight);
                    break;
                case 'list':
                    $thesisContentVO = null;
                    if ($entity->getContentTypeId() !== null) {
                        $contentType = $this->thesisContentTypeService->getThesisTypeNameById($entity->getContentTypeId());
                        $contentId = $entity->getContentId();
                        $listContentEntityVO = $this->contentEntityLoader->loadById($contentType, $contentId);
                        $thesisContentVO = $this->buildFromEntities([$listContentEntityVO], $withHighlight)[0];
                    }
                    $formatter = new ListContentTypeFormatter($entity->getTitle(), $thesisContentVO);
                    break;
            }
            $thesisContentVOs[] = new ThesisContentVO(
                $entity->getId(),
                $entity->getThesisId(),
                $contentEntityVO->getType(),
                $entity->getOrder(),
                $formatter->format()
            );
        }

        return $thesisContentVOs;
    }
}
