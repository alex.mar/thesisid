<?php
declare(strict_types=1);

namespace App\Service\Thesis\Highlight;

class HighlightContent
{
    public static function highlightCss(string $css): string
    {
        $styleParts = [];
        preg_match_all("|<div>(.*)</div>|U", $css, $styleParts, PREG_PATTERN_ORDER);
        $valueParts = $styleParts[1];
        foreach ($valueParts as $valuePart) {
            if (str_contains($valuePart, '{')) {
                // selector
                $tag = strip_tags($valuePart);
                $tag = preg_replace("/{/", '', $tag);
                $formattedTag = '<span class="c-dbl">' . $tag . '</span>';
                $css = str_replace($tag, $formattedTag, $css);
            } elseif (str_contains($valuePart, ':') && str_contains($valuePart, '$')) {
                // sass variable
                $valuePartKeyValue = explode(':', $valuePart);
                $valuePartKey = $valuePartKeyValue[0];
                if (str_contains($valuePartKey, '$')) {
                    $formattedKey = '<span class="c-bl w-bo">' . $valuePartKey . '</span>';
                    if (str_contains($css, $formattedKey)) {
                        $css = str_replace($formattedKey, $valuePartKey, $css);
                    }
                    $css = str_replace($valuePartKey, $formattedKey, $css);
                } else {
                    // key css
                    $formattedKey = '<span class="c-re w-bo">' . $valuePartKey . '</span>';
                    if (str_contains($css, $formattedKey)) {
                        $css = str_replace($formattedKey, $valuePartKey, $css);
                    }
                    $css = str_replace($valuePartKey, $formattedKey, $css);
                }
            } elseif (str_contains($valuePart, ':')) {
                // key css
                $valuePartKeyValue = explode(':', $valuePart);
                $valuePartKey = $valuePartKeyValue[0];
                $formattedKey = '<span class="c-re w-bo">' . $valuePartKey . '</span>';
                if (str_contains($css, $formattedKey)) {
                    $css = str_replace($formattedKey, $valuePartKey, $css);
                }
                $css = str_replace($valuePartKey, $formattedKey, $css);
            } elseif (str_contains($valuePart, '//')) {
                // comment
                $comment = strip_tags($valuePart);
                $formattedComment = '<span class="c-gr">' . $comment . '</span>';
                $css = str_replace($comment, $formattedComment, $css);
            }
        }

        return $css;
    }

    public static function highlightPhp(string $code): string
    {
        $code = str_replace(
            [
                '[',
                ']',
                ',',
                '-&gt;',
                '{',
                '}',
                '(',
                ')',
                '|',
                ':',
                '?',
            ],
            [
                '<span class="c-gre">[</span>',
                '<span class="c-gre">]</span>',
                '<span class="c-gre">,</span>',
                '<span class="c-gre">-&gt;</span>',
                '<span class="c-gre">{</span>',
                '<span class="c-gre">}</span>',
                '<span class="c-gre">(</span>',
                '<span class="c-gre">)</span>',
                '<span class="c-gre">|</span>',
                '<span class="c-gre">:</span>',
                '<span class="c-gre">?</span>',
            ],
            $code
        );
        $internalKeywords = ['abstract ', 'and', 'as ', 'break;', 'callable', 'case ', 'catch ',
            'class ', 'clone', 'const ', 'continue', 'declare', 'default:', 'die;', 'do ', 'echo ', 'else ', 'elseif ',
            'empty', 'enddeclare', 'endfor', 'endforeach', 'endif', 'endswitch', 'endwhile', 'eval', 'exit;',
            'extends', 'final', 'for', 'foreach', 'function', 'global', 'if ', 'implements ',
            'include ', 'include_once ', 'instanceof ', 'insteadof ', 'interface', 'isset', 'list', 'namespace ',
            'new ', 'or ', 'print', 'private ', 'protected ', 'public ', 'require', 'require_once', 'return',
            'static ', 'switch ', 'throw ', 'trait', ' try ', 'unset', 'use ', 'var', 'while '];
        foreach ($internalKeywords as $keyword) {
            $code = str_replace($keyword, '<span class="c-gre w-bo">' . $keyword . '</span>', $code);
        }
        $code = str_replace('&nbsp<span class="c-gre">;</span>', '&nbsp;', $code);
        $code = str_replace('<span class="c-gre">(</span><span class="c-gre">)</span>', '()', $code);
        return preg_replace("/'([^']*)'/", "<span class='c-re'>'$1'</span>", $code);
    }

    public static function highlightHtml(string $code): string
    {
        $dom = new DOMDocument();
        $dom->loadHTML($code);
        $nodes = $dom->getElementsByTagName('div');
        $replacedTagNames = [];
        foreach ($nodes as $node) {
            if (!isset($replacedTagNames[$node->tagName])) {
                $code = str_replace('&lt;' . $node->tagName, '&lt;<span class="c-bl">'
                    . $node->tagName . '</span>', $code);
                $code = str_replace('&lt;/' . $node->tagName, '&lt;/<span class="c-bl">'
                    . $node->tagName . '</span>', $code);
                $replacedTagNames[$node->tagName] = $node->tagName;
            }
        }
        preg_match_all("|<div>(.*)</div>|U", $code, $codeParts, PREG_PATTERN_ORDER);
        $htmlCodeParts = $codeParts[0];
        foreach ($htmlCodeParts as $key => $codePart) {
            $htmlCodeParts[$key] = preg_replace(
                ["/(&lt;!--.*--&gt;)/", '/class=("\w+-*\w+")&gt;/'],
                ["<span class='c-lgr'>$1</span>", "class=<span class='c-lbl'>$1</span>&gt;"],
                $codePart
            );
        }
        return implode('', $htmlCodeParts);
    }

    public static function highlightBash(string $command): string
    {
        preg_match_all("|<div>(.*)</div>|U", $command, $parts, PREG_PATTERN_ORDER);
        $commandParts = $parts[1];
        foreach ($commandParts as $valuePart) {
            if ($valuePart[0] === '#') {
                $comment = strip_tags($valuePart);
                $formattedComment = '<span class="c-gr">' . $comment . '</span>';
                $command = str_replace($comment, $formattedComment, $command);
            }
        }
        if ($command[0] === '#') {
            $pos = stripos($command, '<div>');
            $clearSubString = substr($command, 0, $pos);
            $subString = '<span class="c-gr">' . $clearSubString . '</span>';
            $command = substr_replace($command, $subString, 0, $pos);
        }
        $command = str_replace('$', '<span class="c-dw w-bo">$</span>', $command);
        return $command;
    }
}
