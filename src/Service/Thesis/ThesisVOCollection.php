<?php
declare(strict_types=1);

namespace App\Service\Thesis;

use App\Service\Thesis\VO\ThesisVO;
use ArrayObject;
use Exception;
use JetBrains\PhpStorm\Pure;

class ThesisVOCollection extends ArrayObject
{
    public function append(mixed $value): void
    {
        if (!($value instanceof ThesisVO)) {
            throw new Exception('Value must be ThesisVO');
        }
        parent::append($value);
    }

    public function offsetSet(mixed $key, mixed $value): void
    {
        if (!($value instanceof VO\ThesisVO)) {
            throw new Exception('Value must be ThesisVO');
        }
        parent::offsetSet($key, $value);
    }

    #[Pure] public function toArray(): array
    {
        $result = [];
        /** @var ThesisVO $thesisVO */
        foreach ($this as $thesisVO) {
            $result[] = $thesisVO->toArray();
        }

        return $result;
    }

    /**
     * @return ThesisVO[]
     */
    public function getArrayCopy(): array
    {
        return parent::getArrayCopy();
    }
}
