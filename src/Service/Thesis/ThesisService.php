<?php
declare(strict_types=1);

namespace App\Service\Thesis;

use App\Repository\ThesisRepository;

class ThesisService
{
    public const DEFAULT_LIMIT = 10;

    public function __construct(
        private ThesisRepository $thesisRepository,
        private ThesisVOBuilder $thesisVOBuilder
    ) {
    }

    public function checkExistThesis(string $source, string $content): int
    {
        $similarThesisId = 0;
        $content = preg_replace('/\b\w{1,3}\b(\s|.\s)?/u', '', $content);
        $content = preg_replace('/[^ a-zа-яё\d]/ui', '', $content);
        $content = trim($content);

        $thesisIds = $this->thesisRepository->getThesisIdsBySource($source);
        $this->thesisVOBuilder->setWithHighlight(false);
        $thesisVOCollection = $this->thesisVOBuilder->buildByIds($thesisIds, false);
        foreach ($thesisVOCollection->getArrayCopy() as $thesisVO) {
            $contentVOs = $thesisVO->getContent();
            $fullContent = '';
            foreach ($contentVOs as $contentVO) {
                $contentString = implode('|', $contentVO->getContent());
                $currentContent = strip_tags($contentString);
                $currentContent = preg_replace('/\b\w{1,3}\b(\s|.\s)?/u', '', $currentContent);
                $currentContent = preg_replace('/[^ a-zа-яё\d]/ui', '', $currentContent);
                $currentContent = trim($currentContent);
                $fullContent .= ' ' . $currentContent;
                similar_text($content, $currentContent, $percent);
                if ($percent > 50) {
                    $similarThesisId = $thesisVO->getId();
                    break 2;
                }
            }
            similar_text($content, $fullContent, $percent);
            if ($percent > 50) {
                $similarThesisId = $thesisVO->getId();
                break;
            }
        }

        return $similarThesisId;
    }
}
