<?php
declare(strict_types=1);

namespace App\Service\Thesis\Formatter\ContentType;

use App\Repository\ThesisPhpRepository;
use App\Service\Thesis\Highlight\HighlightContent;
use JetBrains\PhpStorm\ArrayShape;

class PhpContentTypeFormatter implements ContentTypeFormatterInterface
{
    public function __construct(
        private string $html,
        private bool $withHighlight
    ) {
    }


    #[ArrayShape([ThesisPhpRepository::FIELD_CODE => "string"])]
    public function format(): array
    {
        $content = $this->withHighlight ? HighlightContent::highlightBash($this->html) : $this->html;
        return [
            ThesisPhpRepository::FIELD_CODE => $content
        ];
    }
}
