<?php
declare(strict_types=1);

namespace App\Service\Thesis\Formatter\ContentType;

use App\Repository\ThesisHtmlRepository;
use App\Service\Thesis\Highlight\HighlightContent;
use JetBrains\PhpStorm\ArrayShape;

class HtmlContentTypeFormatter implements ContentTypeFormatterInterface
{
    public function __construct(
        private string $html,
        private bool $withHighlight
    ) {
    }

    #[ArrayShape([ThesisHtmlRepository::FIELD_CODE => "string"])]
    public function format(): array
    {
        $content = $this->withHighlight ? HighlightContent::highlightBash($this->html) : $this->html;
        return [
            ThesisHtmlRepository::FIELD_CODE => $content
        ];
    }
}
