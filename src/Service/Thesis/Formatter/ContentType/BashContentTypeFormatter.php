<?php
declare(strict_types=1);

namespace App\Service\Thesis\Formatter\ContentType;

use App\Repository\ThesisBashRepository;
use App\Service\Thesis\Highlight\HighlightContent;
use JetBrains\PhpStorm\ArrayShape;

class BashContentTypeFormatter implements ContentTypeFormatterInterface
{
    public function __construct(
        private string $bash,
        private bool $withHighlight
    ) {
    }

    #[ArrayShape([ThesisBashRepository::FIELD_COMMAND => "string"])]
    public function format(): array
    {
        $content = $this->withHighlight ? HighlightContent::highlightBash($this->bash) : $this->bash;
        return [
            ThesisBashRepository::FIELD_COMMAND => $content
        ];
    }
}
