<?php
declare(strict_types=1);

namespace App\Service\Thesis\Formatter\ContentType;

use App\Repository\ThesisTextRepository;
use JetBrains\PhpStorm\ArrayShape;

class TextContentTypeFormatter implements ContentTypeFormatterInterface
{
    public function __construct(
        private string $text
    ) {
    }

    #[ArrayShape([ThesisTextRepository::FIELD_TEXT => "string"])]
    public function format(): array
    {
        return [
            ThesisTextRepository::FIELD_TEXT => $this->text
        ];
    }
}
