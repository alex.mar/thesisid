<?php
declare(strict_types=1);

namespace App\Service\Thesis\Formatter\ContentType;

interface ContentTypeFormatterInterface
{
    public function format(): array;
}