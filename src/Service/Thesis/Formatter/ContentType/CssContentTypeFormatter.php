<?php
declare(strict_types=1);

namespace App\Service\Thesis\Formatter\ContentType;

use App\Repository\ThesisCssRepository;
use App\Service\Thesis\Highlight\HighlightContent;
use JetBrains\PhpStorm\ArrayShape;

class CssContentTypeFormatter implements ContentTypeFormatterInterface
{
    public function __construct(
        private string $style,
        private bool $withHighlight
    ) {
    }

    #[ArrayShape([ThesisCssRepository::FIELD_STYLE => "string"])]
    public function format(): array
    {
        $content = $this->withHighlight ? HighlightContent::highlightBash($this->style) : $this->style;
        return [
            ThesisCssRepository::FIELD_STYLE => $content
        ];
    }
}
