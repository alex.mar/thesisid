<?php
declare(strict_types=1);

namespace App\Service\Thesis\Formatter\ContentType;

use App\Service\Thesis\VO\ThesisContentVO;
use JetBrains\PhpStorm\ArrayShape;

class ListContentTypeFormatter implements ContentTypeFormatterInterface
{
    public function __construct(
        private string $title,
        private ?ThesisContentVO $thesisContentVO
    ) {
    }

    #[ArrayShape(['id' => "int", 'title' => "string", 'type' => "string", 'content' => "array"])]
    public function format(): array
    {
        return [
            'id' => $this->thesisContentVO?->getId(),
            'title' => $this->title,
            'type' => $this->thesisContentVO?->getType(),
            'content' => $this->thesisContentVO?->getContent()
        ];
    }
}
