<?php
declare(strict_types=1);

namespace App\Service\Thesis\VO;

class ThesisQueryVO
{
    public function __construct(
        private int $id,
        private $query
    ) {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getQuery()
    {
        return $this->query;
    }
}
