<?php
declare(strict_types=1);

namespace App\Service\Thesis\VO;

use App\Entity\ThesisMetaEntity;

class ThesisVO
{
    /**
     * @param int $id
     * @param string $type
     * @param string $url
     * @param string $title
     * @param ThesisMetaEntity $seo
     * @param ThesisContentVO[] $content
     * @param string $source
     * @param bool $show
     * @param ThesisQueryVO[] $queries
     */
    public function __construct(
        private int              $id,
        private string           $type,
        private string           $url,
        private string           $title,
        private ThesisMetaEntity $seo,
        private array            $content,
        private string           $source,
        private bool             $show,
        private array            $queries
    ) {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return ThesisContentVO[]
     */
    public function getContent(): array
    {
        return $this->content;
    }

    public function getSource(): string
    {
        return $this->source;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function isShow(): bool
    {
        return $this->show;
    }

    public function getSeo(): ThesisMetaEntity
    {
        return $this->seo;
    }

    /**
     * @return ThesisQueryVO[]
     */
    public function getQueries(): array
    {
        return $this->queries;
    }

    public function toArray(): array
    {
        return get_object_vars($this);
    }
}
