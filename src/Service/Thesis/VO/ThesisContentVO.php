<?php
declare(strict_types=1);

namespace App\Service\Thesis\VO;

class ThesisContentVO
{
    public function __construct(
        private ?int $id,
        private int $thesisId,
        private string $type,
        private int $order,
        private array $content
    ) {
    }

    public function getId(): int|null
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getThesisId(): int
    {
        return $this->thesisId;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getOrder(): int
    {
        return $this->order;
    }

    public function getContent(): array
    {
        return $this->content;
    }
}
