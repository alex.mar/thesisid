<?php
declare(strict_types=1);

namespace App\Service\GoogleTranslate;

use Google\ApiCore\ApiException;
use Google\Cloud\Translate\V3\TranslationServiceClient;
use Symfony\Component\HttpKernel\KernelInterface;

class GoogleTranslateService
{

    public function __construct(
        private KernelInterface $kernel
    ) {
    }

    public function translate(array $textParts, $targetLanguage): array
    {
        $result = [];
        $container = $this->kernel->getContainer();
        $projectRoot = $this->kernel->getProjectDir() . DIRECTORY_SEPARATOR
            . $container->getParameter('google_application_credentials_file');
        putenv('GOOGLE_APPLICATION_CREDENTIALS=' . $projectRoot);
        $translationServiceClient = new TranslationServiceClient();
        try {
            $formattedParent = $translationServiceClient
                ->locationName($container->getParameter('google_cloud_project_id'), 'global');
            try {
                $response = $translationServiceClient->translateText($textParts, $targetLanguage, $formattedParent);
                foreach ($response->getTranslations()->getIterator() as $item) {
                    $result[] = $item->getTranslatedText();
                }
            } catch (ApiException $e) {
            }
        } finally {
            $translationServiceClient->close();
        }

        return $result;
    }
}
