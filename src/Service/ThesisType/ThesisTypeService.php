<?php
declare(strict_types=1);

namespace App\Service\ThesisType;

use App\Entity\ThesisTypeEntity;
use App\Repository\ThesisTypeRepository;

class ThesisTypeService
{
    /** @var ThesisTypeEntity[] */
    private array $allThesisTypes = [];

    public function __construct(
        private ThesisTypeRepository $thesisTypeRepository
    ) {
        $this->init();
    }


    private function init(): void
    {
        $allThesisTypes = $this->thesisTypeRepository->findBy([], ['order' => 'ASC']);
        $indexedAllThesisTypes = [];
        foreach ($allThesisTypes as $entity) {
            $indexedAllThesisTypes[$entity->getId()] = $entity;
        }
        $this->allThesisTypes = $indexedAllThesisTypes;
    }

    /**
     * @return ThesisTypeEntity[]
     */
    public function getAll(): array
    {
        return $this->allThesisTypes;
    }

    public function getThesisTypeNameById(int $thesisTypeId): string
    {
        return $this->allThesisTypes[$thesisTypeId] ? $this->allThesisTypes[$thesisTypeId]->getName() : '';
    }

    public function getThesisTypeByName(string $type): ThesisTypeEntity
    {
        $indexedThesisTypes = [];
        foreach ($this->allThesisTypes as $entity) {
            $indexedThesisTypes[$entity->getName()] = $entity;
        }
        $thesisType = $indexedThesisTypes[$type] ?? null;
        if ($thesisType === null) {
            $thesisType = $indexedThesisTypes[ThesisTypeRepository::TYPE_DEFAULT];
        }

        return $thesisType;
    }

    public function getAllTypeNames(): array
    {
        $result = [];
        foreach ($this->allThesisTypes as $entity) {
            $result[] = $entity->getName();
        }

        return $result;
    }
}
