<?php
declare(strict_types=1);

namespace App\Service\Search;

use App\Service\Search\ConditionBlock\Complete\QueryConditionBlock;
use App\Service\Search\VO\CompleteVO;
use App\Service\Search\VO\CompleteVOBuilder;
use App\Service\Solr\Client\SolrClientBuilder;
use App\Service\Solr\CompleteFields;
use App\Service\Solr\Query\Condition\Condition;
use App\Service\Solr\Query\Condition\ConditionBlock;
use SolrClientException;
use SolrQuery;
use SolrServerException;

class SearchService
{
    /**
     * @param SolrClientBuilder $solrClientBuilder
     */
    public function __construct(
        private SolrClientBuilder $solrClientBuilder,
        private CompleteVOBuilder $completeVOBuilder
    ) {
    }

    /**
     * @param string $query
     * @return CompleteVO[]
     * @throws SolrClientException
     * @throws SolrServerException
     */
    public function searchComplete(string $query): array
    {
        $solrClient = $this->solrClientBuilder->build(SolrClientBuilder::CORE_COMPLETE);
        $conditionBlock = new QueryConditionBlock();
        $solrQuery = new SolrQuery();
        $solrQuery->setStart('0');
        $solrQuery->setRows('10');
        $solrQuery->addSortField(CompleteFields::FIELD_RANK);
        $solrQuery->addField(CompleteFields::FIELD_QUERY);
        $solrQuery->addField(CompleteFields::FIELD_ID);
        $solrQuery->setQuery($conditionBlock->getConditionBlock($query)->buildQuery());

        $response = $solrClient->query($solrQuery);

        return $this->completeVOBuilder->buildFromSolr($response);
    }

    /**
     * @param string $query
     * @return CompleteVO[] - completes indexed by completeId
     * @throws SolrClientException
     * @throws SolrServerException
     */
    public function searchStrictComplete(string $query): array
    {
        $queryArray = explode(' ', $query);
        $solrClient = $this->solrClientBuilder->build(SolrClientBuilder::CORE_COMPLETE);
        $conditionBlock = new ConditionBlock();
        foreach ($queryArray as $queryItem) {
            $conditionBlock->appendCondition(new Condition(CompleteFields::FIELD_QUERY, $queryItem, true));
        }
        $solrQuery = new SolrQuery();
        $solrQuery->setStart('0');
        $solrQuery->setRows('100');
        $solrQuery->addField(CompleteFields::FIELD_ID);
        $solrQuery->addField(CompleteFields::FIELD_QUERY);
        $solrQuery->setQuery($conditionBlock->buildQuery());

        $solrQueryResponse = $solrClient->query($solrQuery);

        return $this->completeVOBuilder->buildFromSolr($solrQueryResponse);
    }

//    public function search(SearchContextVO $searchContextVO): SolrResponseCollection
//    {
//        $responseCollection = new SolrResponseCollection();
//        $selectedType = $searchContextVO->getThesisType();
//        foreach ($this->searchTypes as $searchType) {
//            if (in_array($selectedType->getName(), $searchType->getAllowedTypes())) {
//                $responseCollection->append($searchType->search($searchContextVO));
//            } else {
//                $responseCollection->append(new SolrResponse($searchType->getType(), 0, new ThesisVOCollection()));
//            }
//        }
//
//        return $responseCollection;
//    }
//
//    public function searchSimilar(string $query): SolrResponse
//    {
//        $query = strtolower($query);
//
//        $solrClient = $this->solrClientBuilder->build(SolrClientBuilder::CORE_THESIS);
//
//        $queryCollection = new QueryCollection();
//        $queryCollection->append((new ShowConditionBlock())->getConditionBlock($query));
//        $queryCollection->append(
//            (new ThesisConditionBlock(Condition::OPERAND_OR))
//            ->setGroupSingleCharacter(true)
//            ->getConditionBlock($query)
//        );
//
//        $solrQuery = new SolrQuery($queryCollection->getQuery());
//        $solrQuery->setStart('0');
//        $solrQuery->setRows('10');
////        $solrQuery->setTermsSort(0);
//        $solrQuery->addField(ThesisFields::FIELD_ID);
//
//        $queryResponse = $solrClient->query($solrQuery);
//        return $this->solrResponseBuilder->build($queryResponse);
//    }
}
