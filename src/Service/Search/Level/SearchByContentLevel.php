<?php
declare(strict_types=1);

namespace App\Service\Search\Level;

use App\Service\Search\Context\VO\SearchContextVO;
use App\Service\Search\Type\SearchTypeContainer;
use App\Service\Solr\Response\SolrResponse;
use App\Service\Solr\Response\SolrResponseCollection;
use App\Service\Thesis\ThesisVOCollection;

class SearchByContentLevel implements SearchLevelInterface
{
    private const LEVEL = 1000;
    private const KEY = 'SearchByContentLevel';

    public function __construct(
        private SearchTypeContainer $searchTypeContainer
    ) {
    }

    public function search(SearchContextVO $searchContext): SolrResponseCollection
    {
        $responseCollection = new SolrResponseCollection();
        $selectedType = $searchContext->getThesisType();
        foreach ($this->searchTypeContainer->all() as $searchType) {
            if (in_array($selectedType->getName(), $searchType->getAllowedTypes())) {
                $responseCollection->append($searchType->search($searchContext));
            } else {
                $responseCollection->append(new SolrResponse($searchType->getType(), 0, new ThesisVOCollection()));
            }
        }

        return $responseCollection;
    }

    public function getLevel(): int
    {
        return self::LEVEL;
    }

    public static function getKey(): string
    {
        return self::KEY;
    }
}
