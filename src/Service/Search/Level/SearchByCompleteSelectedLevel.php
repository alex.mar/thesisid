<?php
declare(strict_types=1);

namespace App\Service\Search\Level;

use App\Repository\ThesisCompleteRepository;
use App\Service\Complete\CompleteService;
use App\Service\Search\Context\VO\SearchContextVO;
use App\Service\Solr\Response\SolrResponseBuilder;
use App\Service\Solr\Response\SolrResponseCollection;
use App\Service\Thesis\ThesisVOBuilder;

class SearchByCompleteSelectedLevel implements SearchLevelInterface
{
    private const LEVEL = 100;
    private const KEY = 'SearchByCompleteSelectedLevel';

    public function __construct(
        private CompleteService $completeService,
        private ThesisCompleteRepository $thesisCompleteRepository,
        private ThesisVOBuilder $thesisVOBuilder,
        private SolrResponseBuilder $solrResponseBuilder
    ) {
    }

    public function search(SearchContextVO $searchContext): SolrResponseCollection
    {
        $solrResponseCollection = new SolrResponseCollection();
        $fromCompleteId = (int) $searchContext->getRequest()->query->get('complete');
        if (!$fromCompleteId
            || !$this->completeService->compareByCompleteId($searchContext->getQuery(), $fromCompleteId)) {
            return $solrResponseCollection;
        }
        $thesisIds = $this->thesisCompleteRepository->getThesisIdsByCompleteId($fromCompleteId);
        $thesisVOCollection = $this->thesisVOBuilder->buildByIds($thesisIds);

        return $this->solrResponseBuilder->buildFromThesis(
            $thesisVOCollection,
            $searchContext
        );
    }

    public function getLevel(): int
    {
        return self::LEVEL;
    }

    public static function getKey(): string
    {
        return self::KEY;
    }
}
