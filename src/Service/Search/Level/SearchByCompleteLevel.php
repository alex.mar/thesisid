<?php
declare(strict_types=1);

namespace App\Service\Search\Level;

use App\Repository\ThesisCompleteRepository;
use App\Service\Complete\CompleteService;
use App\Service\Search\Context\VO\SearchContextVO;
use App\Service\Search\SearchService;
use App\Service\Solr\Response\SolrResponseBuilder;
use App\Service\Solr\Response\SolrResponseCollection;
use App\Service\Thesis\ThesisVOBuilder;

class SearchByCompleteLevel implements SearchLevelInterface
{
    private const LEVEL = 200;
    private const KEY = 'SearchByCompleteLevel';

    public function __construct(
        private CompleteService $completeService,
        private ThesisCompleteRepository $thesisCompleteRepository,
        private ThesisVOBuilder $thesisVOBuilder,
        private SolrResponseBuilder $solrResponseBuilder,
        private SearchService $searchService
    ) {
    }

    public function search(SearchContextVO $searchContext): SolrResponseCollection
    {
        $solrResponseCollection = new SolrResponseCollection();
        $query = $searchContext->getQuery();
        $completeVOs = $this->searchService->searchStrictComplete($query);
        foreach ($completeVOs as $completeVO) {
            if ($this->completeService->compare($query, $completeVO->getQuery())) {
                $thesisIds = $this->thesisCompleteRepository->getThesisIdsByCompleteId($completeVO->getCompleteId());
                $thesisVOCollection = $this->thesisVOBuilder->buildByIds($thesisIds);
                $solrResponseCollection = $this->solrResponseBuilder->buildFromThesis(
                    $thesisVOCollection,
                    $searchContext
                );
                break;
            }
        }

        return $solrResponseCollection;
    }

    public function getLevel(): int
    {
        return self::LEVEL;
    }

    public static function getKey(): string
    {
        return self::KEY;
    }
}
