<?php
declare(strict_types=1);

namespace App\Service\Search\Level;

use Exception;

class SearchLevelBuilder
{
    /**
     * @param SearchLevelContainer $searchLevelContainer
     */
    public function __construct(
        private SearchLevelContainer $searchLevelContainer
    ) {
    }

    /**
     * @throws Exception
     */
    public function build(string $levelKey): SearchLevelInterface
    {
        $searchLevel = null;
        foreach ($this->searchLevelContainer->all() as $level) {
            if ($level::getKey() === $levelKey) {
                $searchLevel = $level;
                break;
            }
        }
        if ($searchLevel === null) {
            throw new Exception('Search level not defined');
        }

        return $searchLevel;
    }
}
