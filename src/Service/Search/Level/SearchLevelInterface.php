<?php
declare(strict_types=1);

namespace App\Service\Search\Level;

use App\Service\Search\Context\VO\SearchContextVO;
use App\Service\Solr\Response\SolrResponseCollection;

interface SearchLevelInterface
{
    public function search(SearchContextVO $searchContext): SolrResponseCollection;
    public function getLevel(): int;
    public static function getKey(): string;
}