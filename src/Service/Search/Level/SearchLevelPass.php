<?php
declare(strict_types=1);

namespace App\Service\Search\Level;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class SearchLevelPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->has(SearchLevelContainer::class)) {
            return;
        }

        $definition = $container->getDefinition(SearchLevelContainer::class);
        $taggedServices = $container->findTaggedServiceIds('search_level');

        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall('addSearchLevel', [new Reference($id)]);
        }
    }
}
