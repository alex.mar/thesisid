<?php
declare(strict_types=1);

namespace App\Service\Search\Level;

class SearchLevelContainer
{
    private array $searchLevels;

    public function __construct()
    {
        $this->searchLevels = [];
    }

    public function addSearchLevel(SearchLevelInterface $searchLevel): void
    {
        $this->searchLevels[] = $searchLevel;
    }

    /**
     * @return SearchLevelInterface[]
     */
    public function all(): array
    {
        return $this->searchLevels;
    }
}
