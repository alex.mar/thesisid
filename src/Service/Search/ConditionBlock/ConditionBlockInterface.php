<?php
declare(strict_types=1);

namespace App\Service\Search\ConditionBlock;

use App\Service\Solr\Query\Condition\ConditionInterface;

interface ConditionBlockInterface
{
    public function getConditionBlock(string $query): ConditionInterface;
}
