<?php
declare(strict_types=1);

namespace App\Service\Search\ConditionBlock\Complete;

use App\Service\Search\ConditionBlock\ConditionBlockInterface;
use App\Service\Solr\CompleteFields;
use App\Service\Solr\Query\Condition\Condition;
use App\Service\Solr\Query\Condition\ConditionBlock;
use App\Service\Solr\Query\Condition\ConditionInterface;
use App\Service\Solr\SolrFunctionsHelper;
use JetBrains\PhpStorm\Pure;

class QueryConditionBlock implements ConditionBlockInterface
{
    public function getConditionBlock(string $query): ConditionInterface
    {
        $conditionBlock = new ConditionBlock();
        $queryParts = explode(' ', $query);
        foreach ($queryParts as $queryPart) {
            if (empty($queryPart)) {
                continue;
            }
            $conditionBlock->appendCondition(new Condition(
                CompleteFields::FIELD_QUERY,
                $queryPart . '*',
                false
            ));
        }

        return $conditionBlock;
    }
}
