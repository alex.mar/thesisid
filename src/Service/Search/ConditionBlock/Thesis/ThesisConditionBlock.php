<?php
declare(strict_types=1);

namespace App\Service\Search\ConditionBlock\Thesis;

use App\Service\Search\ConditionBlock\ConditionBlockInterface;
use App\Service\Search\VO\QueryPartVOBuilder;
use App\Service\Solr\Query\Condition\Condition;
use App\Service\Solr\Query\Condition\ConditionBlock;
use App\Service\Solr\Query\Condition\ConditionInterface;
use App\Service\Solr\ThesisFields;

class ThesisConditionBlock implements ConditionBlockInterface
{
    private bool $groupSingleCharacter = false;

    public function __construct(
        private string $operand = Condition::OPERAND_AND
    ) {
    }

    public function getConditionBlock(string $query): ConditionInterface
    {
        $conditionBlock = new ConditionBlock($this->operand);

        $queryPartVOs = (new QueryPartVOBuilder())->build($query, $this->isGroupSingleCharacter());
        foreach ($queryPartVOs as $queryPartVO) {
            $conditionBlock->appendCondition(
                new Condition(
                    ThesisFields::FIELD_THESIS,
                    $queryPartVO->getQueryPart(),
                    $queryPartVO->isPhrase(),
                    Condition::TILDA_AUTO_CALCULATE
                )
            );
        }

        return $conditionBlock;
    }

    public function isGroupSingleCharacter(): bool
    {
        return $this->groupSingleCharacter;
    }

    public function setGroupSingleCharacter(bool $groupSingleCharacter): self
    {
        $this->groupSingleCharacter = $groupSingleCharacter;

        return $this;
    }
}
