<?php
declare(strict_types=1);

namespace App\Service\Search\ConditionBlock\Thesis;

use App\Service\Search\ConditionBlock\ConditionBlockInterface;
use App\Service\Solr\ThesisFields;
use App\Service\Solr\Query\Condition\Condition;
use App\Service\Solr\Query\Condition\ConditionInterface;
use App\Service\Solr\ThesisValues;

class ShowConditionBlock implements ConditionBlockInterface
{
    public function getConditionBlock(string $query): ConditionInterface
    {
        return new Condition(ThesisFields::FIELD_SHOW, ThesisValues::VALUE_SHOW_YES);
    }
}
