<?php
declare(strict_types=1);

namespace App\Service\Search\ConditionBlock\Thesis;

use App\Service\Search\ConditionBlock\ConditionBlockInterface;
use App\Service\Solr\ThesisFields;
use App\Service\Solr\Query\Condition\Condition;
use App\Service\Solr\Query\Condition\ConditionBlock;
use App\Service\Solr\Query\Condition\ConditionInterface;

class TitleConditionBlock implements ConditionBlockInterface
{
    public function __construct(
        private string $operand = Condition::OPERAND_AND
    ) {
    }

    public function getConditionBlock(string $query): ConditionInterface
    {
        $conditionBlock = new ConditionBlock($this->operand);
        $queryParts = explode(' ', $query);
        foreach ($queryParts as $word) {
            $conditionBlock->appendCondition(
                new Condition(
                    ThesisFields::FIELD_TITLE,
                    $word,
                    false,
                    1
                )
            );
        }

        return $conditionBlock;
    }
}
