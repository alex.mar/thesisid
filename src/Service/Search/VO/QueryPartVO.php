<?php
declare(strict_types=1);

namespace App\Service\Search\VO;

class QueryPartVO
{
    public function __construct(
        private string $queryPart,
        private bool $isPhrase
    ) {
    }

    public function getQueryPart(): string
    {
        return $this->queryPart;
    }

    public function isPhrase(): bool
    {
        return $this->isPhrase;
    }
}
