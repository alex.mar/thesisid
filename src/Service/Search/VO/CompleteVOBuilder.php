<?php
declare(strict_types=1);

namespace App\Service\Search\VO;

use App\Entity\CompleteEntity;
use App\Service\Solr\CompleteFields;
use SolrObject;
use SolrQueryResponse;

class CompleteVOBuilder
{
    public function buildFromSolr(SolrQueryResponse $solrQueryResponse): array
    {
        /** @var SolrObject $response */
        $response = $solrQueryResponse->getResponse()->offsetGet('response');
        /** @var SolrObject[] $solrDocs */
        $solrDocs = $response->offsetGet('docs');
        $completeVOs = [];
        if (!empty($solrDocs)) {
            foreach ($solrDocs as $doc) {
                $completeId = (int) $doc->offsetGet(CompleteFields::FIELD_ID);
                $query = (string) $doc->offsetGet(CompleteFields::FIELD_QUERY);
                $completeVOs[] = new CompleteVO($completeId, $query);
            }
        }

        return $completeVOs;
    }

    public function buildFromEntity(CompleteEntity $entity): CompleteVO
    {
        return new CompleteVO($entity->getId(), $entity->getQuery());
    }
}
