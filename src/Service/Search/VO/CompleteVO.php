<?php
declare(strict_types=1);

namespace App\Service\Search\VO;

class CompleteVO
{
    public function __construct(
        private int $completeId,
        private string $query
    ) {
    }

    public function getCompleteId(): int
    {
        return $this->completeId;
    }

    public function getQuery(): string
    {
        return $this->query;
    }

    public function toArray(): array
    {
        return get_object_vars($this);
    }
}
