<?php
declare(strict_types=1);

namespace App\Service\Search\VO;

class QueryPartVOBuilder
{
    /**
     * @param string $query
     * @param bool $groupSingleCharacter
     * @return QueryPartVO[]
     */
    public function build(string $query, bool $groupSingleCharacter): array
    {
        $result = [];
        $words = explode(' ', $query);
        if ($groupSingleCharacter) {
            $skipNext = false;
            foreach ($words as $index => $word) {
                if ($skipNext) {
                    $skipNext = false;
                    continue;
                }
                if (iconv_strlen($word) === 1) {
                    $withPrevious = $words[$index - 1] . ' ' . $word;
                    $withNext = $word . ' ' . $words[$index + 1];
                    $result[] = new QueryPartVO($withPrevious, true);
                    $result[] = new QueryPartVO($withNext, true);
                    $skipNext = true;
                } else {
                    $result[] = new QueryPartVO($word, false);
                    $skipNext = false;
                }
            }
        } else {
            foreach ($words as $word) {
                $result[] = new QueryPartVO($word, false);
            }
        }

        return $result;
    }
}
