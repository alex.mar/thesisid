<?php
declare(strict_types=1);

namespace App\Service\Search;

use App\Service\Search\Context\VO\SearchContextVO;
use App\Service\Search\Level\SearchLevelContainer;
use App\Service\Solr\Response\SolrResponseCollection;

class ThesisSearchEngine
{
    public function __construct(
        private SearchLevelContainer $searchLevelContainer
    ) {
    }

    public function search(SearchContextVO $searchContext): SolrResponseCollection
    {
        $solrResponseCollection = new SolrResponseCollection();
        $sortedSearchLevels = [];
        foreach ($this->searchLevelContainer->all() as $searchLevel) {
            $sortedSearchLevels[$searchLevel->getLevel()] = $searchLevel;
        }
        ksort($sortedSearchLevels);
        foreach ($sortedSearchLevels as $searchLevel) {
            $solrResponseCollection = $searchLevel->search($searchContext);
            if ($solrResponseCollection->isHasResult()) {
                break;
            }
        }

        return $solrResponseCollection;
    }
}
