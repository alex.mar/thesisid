<?php
declare(strict_types=1);

namespace App\Service\Search\Context;

use App\Repository\ThesisTypeRepository;
use App\Service\Search\Context\VO\SearchContextVO;
use App\Service\Search\Context\VO\SearchNavigationVOCollection;
use App\Service\ThesisType\ThesisTypeService;
use Symfony\Component\HttpFoundation\Request;

class SearchContextBuilder
{

    public function __construct(
        private ThesisTypeService $thesisTypeService,
        private SearchNavigationBuilder $searchNavigationBuilder
    ) {
    }

    public function build(Request $request): SearchContextVO
    {
        $url = strtok($request->getRequestUri(), '?');
        $originalQuery = $request->query->get('q') ?? '';
        $thesisId = $request->get('id') ?? null;
        if (empty($originalQuery) && $thesisId !== null) {
            $originalQuery = $request->cookies->get('query') ?? '';
        }
        $query = $this->validateQuery($originalQuery);
        $type = $request->query->get('type') ?? ThesisTypeRepository::TYPE_DEFAULT;
        $thesisType = $this->thesisTypeService->getThesisTypeByName($type);

        $searchNavigationCollection = $this->searchNavigationBuilder->build($request);

        return new SearchContextVO($request, $url, $originalQuery, $query, $thesisType, $searchNavigationCollection);
    }

    public function buildEmpty(string $query): SearchContextVO
    {
        $validatedQuery = $this->validateQuery($query);
        return new SearchContextVO(new Request(), '', $query, $validatedQuery, null, new SearchNavigationVOCollection());
    }

    public function validateQuery(string $query): string
    {
        return trim(htmlspecialchars($query));
    }
}
