<?php
declare(strict_types=1);

namespace App\Service\Search\Context\VO;

use App\Entity\ThesisTypeEntity;
use Symfony\Component\HttpFoundation\Request;

class SearchContextVO
{
    public function __construct(
        private Request $request,
        private string $url,
        private string $originalQuery,
        private string $query,
        private ?ThesisTypeEntity $thesisType,
        private SearchNavigationVOCollection $searchNavigationVOCollection
    ) {
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getOriginalQuery(): string
    {
        return $this->originalQuery;
    }

    public function getQuery(): string
    {
        return $this->query;
    }

    public function getThesisType(): ?ThesisTypeEntity
    {
        return $this->thesisType;
    }

    public function getSearchNavigationVOCollection(): SearchNavigationVOCollection
    {
        return $this->searchNavigationVOCollection;
    }

    public function getRequest(): Request
    {
        return $this->request;
    }
}
