<?php
declare(strict_types=1);

namespace App\Service\Search\Context\VO;

class SearchNavigationVO
{
    public function __construct(
        private string $type,
        private int $offset,
        private int $limit
    ) {
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getOffset(): int
    {
        return $this->offset;
    }
    public function getLimit(): int
    {
        return $this->limit;
    }
}
