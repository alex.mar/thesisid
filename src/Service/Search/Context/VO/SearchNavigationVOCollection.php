<?php
declare(strict_types=1);

namespace App\Service\Search\Context\VO;

use Exception;

class SearchNavigationVOCollection extends \ArrayObject
{
    /**
     * @param int $key
     * @param SearchNavigationVO $value
     * @return void
     * @throws Exception
     */
    public function offsetSet(mixed $key, mixed $value)
    {
        if (!($value instanceof SearchNavigationVO)) {
            throw new Exception('Value must be SearchNavigationVO');
        }
        parent::offsetSet($key, $value);
    }

    /**
     * @param SearchNavigationVO $value
     * @return void
     * @throws Exception
     */
    public function append(mixed $value)
    {
        if (!($value instanceof SearchNavigationVO)) {
            throw new Exception('Value must be SearchNavigationVO');
        }
        parent::append($value);
    }

    public function getNavigation(string $type): SearchNavigationVO
    {
        $result = null;
        /** @var SearchNavigationVO $searchNavigation */
        foreach ($this as $searchNavigation) {
            if ($searchNavigation->getType() === $type) {
                $result = $searchNavigation;
                break;
            }
        }
        if ($result === null) {
            throw new Exception('Search navigation not found');
        }

        return $result;
    }

    /**
     * @return SearchNavigationVO[]
     */
    public function getArrayCopy(): array
    {
        return parent::getArrayCopy();
    }
}
