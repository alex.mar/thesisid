<?php
declare(strict_types=1);

namespace App\Service\Search\Context;

use App\Repository\ThesisTypeRepository;
use App\Service\Search\Context\VO\SearchNavigationVO;
use App\Service\Search\Context\VO\SearchNavigationVOCollection;
use Symfony\Component\HttpFoundation\Request;

class SearchNavigationBuilder
{
    private const LIMIT_DOCS = 10;

    public function build(Request $request): SearchNavigationVOCollection
    {
        $selectedType = $request->get('type') ?? null;
        $offset = $request->get('offset') ?? 0;
        $thesisId = $request->get('id') ?? 0;

        $searchNavigationCollection = new SearchNavigationVOCollection();

        if ($thesisId) {
            $searchDefinitionTypeNavigation = new SearchNavigationVO(ThesisTypeRepository::TYPE_DEFAULT, 0, 1);
            $searchDefaultTypeNavigation = new SearchNavigationVO(ThesisTypeRepository::TYPE_DEFINITION, 0, 1);
            $searchListTypeNavigation = new SearchNavigationVO(ThesisTypeRepository::TYPE_LIST, 0, 1);
            $searchNavigationCollection->append($searchDefaultTypeNavigation);
            $searchNavigationCollection->append($searchDefinitionTypeNavigation);
            $searchNavigationCollection->append($searchListTypeNavigation);

            return $searchNavigationCollection;
        }

        switch ($selectedType) {
            case ThesisTypeRepository::TYPE_DEFINITION:
                $searchDefaultTypeNavigation = new SearchNavigationVO(ThesisTypeRepository::TYPE_DEFAULT, 0, 0);
                $searchListTypeNavigation = new SearchNavigationVO(ThesisTypeRepository::TYPE_LIST, 0, 0);
                $searchDefinitionTypeNavigation = new SearchNavigationVO(
                    ThesisTypeRepository::TYPE_DEFINITION,
                    $offset,
                    self::LIMIT_DOCS
                );
                $searchNavigationCollection->append($searchDefaultTypeNavigation);
                $searchNavigationCollection->append($searchListTypeNavigation);
                $searchNavigationCollection->append($searchDefinitionTypeNavigation);
                break;
            case ThesisTypeRepository::TYPE_LIST:
                $searchDefaultTypeNavigation = new SearchNavigationVO(ThesisTypeRepository::TYPE_DEFAULT, 0, 0);
                $searchDefinitionTypeNavigation = new SearchNavigationVO(ThesisTypeRepository::TYPE_DEFINITION, 0, 0);
                $searchListTypeNavigation = new SearchNavigationVO(
                    ThesisTypeRepository::TYPE_LIST,
                    $offset,
                    self::LIMIT_DOCS
                );
                $searchNavigationCollection->append($searchDefaultTypeNavigation);
                $searchNavigationCollection->append($searchListTypeNavigation);
                $searchNavigationCollection->append($searchDefinitionTypeNavigation);
                break;
            case ThesisTypeRepository::TYPE_DEFAULT:
            default:
                $searchDefaultTypeNavigation = new SearchNavigationVO(
                    ThesisTypeRepository::TYPE_DEFAULT,
                    $offset,
                    self::LIMIT_DOCS
                );
                $searchDefinitionTypeNavigation = new SearchNavigationVO(ThesisTypeRepository::TYPE_DEFINITION, 0, 1);
                $searchListTypeNavigation = new SearchNavigationVO(ThesisTypeRepository::TYPE_LIST, 0, 1);
                $searchNavigationCollection->append($searchDefaultTypeNavigation);
                $searchNavigationCollection->append($searchDefinitionTypeNavigation);
                $searchNavigationCollection->append($searchListTypeNavigation);
                break;
        }

        return $searchNavigationCollection;
    }
}
