<?php
declare(strict_types=1);

namespace App\Service\Search\Type;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class SearchTypePass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->has(SearchTypeContainer::class)) {
            return;
        }

        $definition = $container->getDefinition(SearchTypeContainer::class);
        $taggedServices = $container->findTaggedServiceIds('search_type');

        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall('addSearchLevel', [new Reference($id)]);
        }
    }
}
