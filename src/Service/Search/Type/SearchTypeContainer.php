<?php
declare(strict_types=1);

namespace App\Service\Search\Type;

class SearchTypeContainer
{
    private array $searchTypes;

    public function __construct()
    {
        $this->searchTypes = [];
    }

    public function addSearchLevel(SearchTypeInterface $searchType): void
    {
        $this->searchTypes[] = $searchType;
    }

    /**
     * @return SearchTypeInterface[]
     */
    public function all(): array
    {
        return $this->searchTypes;
    }
}
