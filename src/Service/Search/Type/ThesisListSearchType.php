<?php
declare(strict_types=1);

namespace App\Service\Search\Type;

use App\Repository\ThesisTypeRepository;
use App\Service\Search\ConditionBlock\Thesis\ShowConditionBlock;
use App\Service\Search\ConditionBlock\Thesis\ThesisConditionBlock;
use App\Service\Search\Context\VO\SearchContextVO;
use App\Service\Solr\Client\SolrClientBuilder;
use App\Service\Solr\Query\QueryCollection;
use App\Service\Solr\Response\SolrResponse;
use App\Service\Solr\Response\SolrResponseBuilder;
use App\Service\Solr\ThesisFields;
use SolrQuery;

class ThesisListSearchType implements SearchTypeInterface
{
    public function __construct(
        private SolrClientBuilder $solrClientBuilder,
        private SolrResponseBuilder $solrResponseBuilder
    ) {
    }

    public function getType(): string
    {
        return ThesisTypeRepository::TYPE_LIST;
    }

    public function search(SearchContextVO $searchContextVO): SolrResponse
    {
        $query = $searchContextVO->getQuery();
        $solrClient = $this->solrClientBuilder->build(SolrClientBuilder::CORE_THESIS_LIST);
        $queryCollection = new QueryCollection();
        $queryCollection->append((new ShowConditionBlock())->getConditionBlock($query));
        $queryCollection->append((new ThesisConditionBlock())->getConditionBlock($query));
        $solrQuery = new SolrQuery($queryCollection->getQuery());
        $searchNavigation = $searchContextVO->getSearchNavigationVOCollection()->getNavigation($this->getType());
        $solrQuery->setStart((string) $searchNavigation->getOffset());
        $solrQuery->setRows((string) $searchNavigation->getLimit());
        $solrQuery->addField(ThesisFields::FIELD_ID);
        $queryResponse = $solrClient->query($solrQuery);

        return $this->solrResponseBuilder->buildFromSolr($queryResponse, $this->getType());
    }

    public function getAllowedTypes(): array
    {
        return [
            ThesisTypeRepository::TYPE_LIST,
            ThesisTypeRepository::TYPE_DEFAULT,
            ThesisTypeRepository::TYPE_DEFINITION
        ];
    }
}
