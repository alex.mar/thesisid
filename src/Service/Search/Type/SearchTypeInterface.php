<?php
declare(strict_types=1);

namespace App\Service\Search\Type;

use App\Service\Search\Context\VO\SearchContextVO;
use App\Service\Solr\Response\SolrResponse;

interface SearchTypeInterface
{
    public function getType(): string;
    public function search(SearchContextVO $searchContextVO): SolrResponse;
    public function getAllowedTypes(): array;
}
