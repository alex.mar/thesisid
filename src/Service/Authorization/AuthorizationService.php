<?php
declare(strict_types=1);

namespace App\Service\Authorization;

use App\Repository\AdminUserRepository;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;

class AuthorizationService
{

    public function __construct(
        private AdminUserRepository $adminUserRepository,
        private RouterInterface $router
    ) {
    }

    public function checkAuth(Request $request): void
    {
        $xid = $request->cookies->get('xid');
        $user = $this->adminUserRepository->findOneBy(['xid' => $xid]);
        if ($user === null) {
            $response = new RedirectResponse($this->router->generate('app_admin_login'));
            $response->send();
            die();
        }
    }

    public function generateXid(): string
    {
        return md5(uniqid((string)mt_rand(), true));
    }
}
