<?php
declare(strict_types=1);

namespace App\Service\Solr\Query;

use App\Service\Solr\Query\Condition\Condition;
use App\Service\Solr\Query\Condition\ConditionInterface;
use Exception;

class QueryCollection extends \ArrayObject
{
    public function __construct(
        private string $operand = Condition::OPERAND_AND
    ) {
        parent::__construct();
    }

    /**
     * @param int $key
     * @param ConditionInterface $value
     * @return void
     * @throws Exception
     */
    public function offsetSet(mixed $key, mixed $value)
    {
        if (!($value instanceof ConditionInterface)) {
            throw new Exception('Value must be ConditionInterface');
        }
        parent::offsetSet($key, $value);
    }

    /**
     * @param ConditionInterface $value
     * @return void
     * @throws Exception
     */
    public function append(mixed $value)
    {
        if (!($value instanceof ConditionInterface)) {
            throw new Exception('Value must be ConditionInterface');
        }
        parent::append($value);
    }

    public function getQuery(): string
    {
        $query = '';
        /** @var ConditionInterface $item */
        foreach ($this as $item) {
            $query .= ' ' . $item->buildQuery() . ' ' . $this->operand;
        }
        return substr_replace($query, '', -4);
    }
}
