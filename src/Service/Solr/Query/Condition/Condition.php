<?php
declare(strict_types=1);

namespace App\Service\Solr\Query\Condition;

class Condition implements ConditionInterface
{
    public const OPERAND_AND = 'AND';
    public const OPERAND_OR = 'OR';
    public const TILDA_AUTO_CALCULATE = 999;

    public function __construct(
        private string $field,
        private string $value,
        private bool $isPhrase = false,
        private int $tilda = 0,
    ) {
        if ($this->tilda === self::TILDA_AUTO_CALCULATE) {
            $this->calculateTilda();
        }
    }

    public function getField(): string
    {
        return $this->field;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function buildQuery(): string
    {
        $value = $this->isPhrase ? "\"$this->value\"" : $this->value;
        if ($this->tilda) {
            $value .= '~' . $this->tilda;
        }
        return $this->field . ':' . $value;
    }

    private function calculateTilda(): void
    {
        $characters = iconv_strlen($this->value);
        if ($characters > 8) {
            $this->tilda = 2;
        } elseif ($characters > 4) {
            $this->tilda = 1;
        } else {
            $this->tilda = 0;
        }
    }
}
