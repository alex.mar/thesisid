<?php
declare(strict_types=1);

namespace App\Service\Solr\Query\Condition;

class ConditionBlock implements ConditionInterface
{
    /** @var ConditionInterface[] */
    private array $conditions = [];

    public function __construct(
        private string $operand = Condition::OPERAND_AND
    ) {
    }

    public function appendCondition(ConditionInterface $condition): void
    {
        $this->conditions[] = $condition;
    }

    public function buildQuery(): string
    {
        $query = '(';
        foreach ($this->conditions as $condition) {
            $query .= $condition->buildQuery() . ' ' . $this->operand . ' ';
        }
        return substr_replace($query, '', -4) . ')';
    }
}
