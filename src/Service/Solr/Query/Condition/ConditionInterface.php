<?php
declare(strict_types=1);

namespace App\Service\Solr\Query\Condition;

interface ConditionInterface
{
    public function buildQuery(): string;
}
