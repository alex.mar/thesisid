<?php
declare(strict_types=1);

namespace App\Service\Solr\Client;

use App\Repository\ThesisTypeRepository;
use SolrClient;

class SolrClientBuilder
{
    public const CORE_THESIS = 'thesis';
    public const CORE_THESIS_DEFINITION = 'thesis_definition';
    public const CORE_THESIS_LIST = 'thesis_list';
    public const CORE_COMPLETE = 'complete';

    public function __construct(
        private string $host,
        private int $port,
        private string $user,
        private string $password
    ) {
    }

    public function build(string $coreName): ?\SolrClient
    {
        $client = null;
        try {
            $client = new SolrClient([
                'hostname' => $this->host,
                'login' => $this->user,
                'password' => $this->password,
                'port' => $this->port,
                'path' => '/solr/' . $coreName,
            ]);
        } catch (\SolrIllegalArgumentException $e) {
        }

        return $client;
    }

    public function getThesisCoreByThesisTypeName(string $thesisTypeName): string
    {
        return match ($thesisTypeName) {
            ThesisTypeRepository::TYPE_DEFAULT => self::CORE_THESIS,
            ThesisTypeRepository::TYPE_DEFINITION => self::CORE_THESIS_DEFINITION,
        };
    }
}
