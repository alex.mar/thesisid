<?php
declare(strict_types=1);

namespace App\Service\Solr\Response;

use App\Service\Thesis\ThesisVOCollection;

class SolrResponse
{
    /**
     * @param string $type
     * @param int $numFound
     * @param ThesisVOCollection $docs
     */
    public function __construct(
        private string $type,
        private int $numFound,
        private ThesisVOCollection $docs
    ) {
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    public function getNumFound(): int
    {
        return $this->numFound;
    }

    public function getDocs(): ThesisVOCollection
    {
        return $this->docs;
    }
}
