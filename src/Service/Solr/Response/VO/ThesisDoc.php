<?php
declare(strict_types=1);

namespace App\Service\Solr\Response\VO;

class ThesisDoc
{
    public function __construct(
        private int $thesisId
    ) {
    }

    public function getThesisId(): int
    {
        return $this->thesisId;
    }
}
