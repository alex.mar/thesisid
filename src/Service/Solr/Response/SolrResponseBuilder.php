<?php
declare(strict_types=1);

namespace App\Service\Solr\Response;

use App\Service\Search\Context\VO\SearchContextVO;
use App\Service\Solr\ThesisFields;
use App\Service\Thesis\ThesisVOBuilder;
use App\Service\Thesis\ThesisVOCollection;
use SolrObject;
use SolrQueryResponse;

class SolrResponseBuilder
{
    public function __construct(
        private ThesisVOBuilder $thesisVOBuilder
    ) {
    }

    public function buildFromSolr(SolrQueryResponse $solrQueryResponse, string $type, $withPublished = true): SolrResponse
    {
        /** @var SolrObject $response */
        $response = $solrQueryResponse->getResponse()->offsetGet('response');
        $numFound = (int) $response->offsetGet('numFound');
        /** @var SolrObject[] $docs */
        $docs = $response->offsetGet('docs');
        $thesisIds = [];
        if (!empty($docs)) {
            foreach ($docs as $doc) {
                $thesisIds[] = (int) $doc->offsetGet(ThesisFields::FIELD_ID);
            }
        }
        $thesisVOCollection = $this->thesisVOBuilder->buildByIds($thesisIds, $withPublished);

        return new SolrResponse($type, $numFound, $thesisVOCollection);
    }

    public function buildFromThesis(
        ThesisVOCollection $thesisVOCollection,
        SearchContextVO $searchContextVO
    ): SolrResponseCollection {
        $groupedThesis = [];
        foreach ($thesisVOCollection->getArrayCopy() as $thesisVO) {
            $groupedThesis[$thesisVO->getType()][] = $thesisVO;
        }

        $searchNavigationCollection = $searchContextVO->getSearchNavigationVOCollection();
        $solrResponseCollection = new SolrResponseCollection();
        foreach ($groupedThesis as $type => $thesisVOs) {
            $allCountThesis = count($thesisVOs);
            $currentThesisVOCollection = new ThesisVOCollection();
            if (count($groupedThesis) > 1) {
                $navigation = $searchNavigationCollection->getNavigation($type);
                $limit = $navigation->getLimit();
                $thesisVOs = array_slice($thesisVOs, 0, $limit);
            }
            foreach ($thesisVOs as $thesisVO) {
                $currentThesisVOCollection->append($thesisVO);
            }
            $solrResponse = new SolrResponse($type,$allCountThesis, $currentThesisVOCollection);
            $solrResponseCollection->append($solrResponse);
        }

        return $solrResponseCollection;
    }
}
