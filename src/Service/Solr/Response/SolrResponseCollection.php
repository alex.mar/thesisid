<?php
declare(strict_types=1);

namespace App\Service\Solr\Response;

use Exception;

class SolrResponseCollection extends \ArrayObject
{
    /**
     * @param int $key
     * @param SolrResponse $value
     * @return void
     * @throws Exception
     */
    public function offsetSet(mixed $key, mixed $value)
    {
        if (!($value instanceof SolrResponse)) {
            throw new Exception('Value must be SolrResponse');
        }
        parent::offsetSet($key, $value);
    }

    /**
     * @param SolrResponse $value
     * @return void
     * @throws Exception
     */
    public function append(mixed $value)
    {
        if (!($value instanceof SolrResponse)) {
            throw new Exception('Value must be SolrResponse');
        }
        parent::append($value);
    }

    /**
     * @return SolrResponse[]
     */
    public function getArrayCopy(): array
    {
        return parent::getArrayCopy();
    }

    public function isHasResult(): bool
    {
        $hasResult = false;
        foreach ($this->getArrayCopy() as $solrResponse) {
            $thesisVOCollection = $solrResponse->getDocs();
            if ($thesisVOCollection->getArrayCopy()) {
                $hasResult = true;
                break;
            }
        }

        return $hasResult;
    }
}
