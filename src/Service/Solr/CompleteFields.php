<?php
declare(strict_types=1);

namespace App\Service\Solr;

class CompleteFields
{
    public const FIELD_ID = 'id';
    public const FIELD_QUERY = 'query';
    public const FIELD_RANK = 'rank';
}
