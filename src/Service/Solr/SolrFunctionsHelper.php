<?php
declare(strict_types=1);

namespace App\Service\Solr;

class SolrFunctionsHelper
{
    public static function complexPhrase(string $field, bool $inOrder = true): string
    {
        if ($inOrder) {
            $field = '{!complexphrase inOrder=true}' . $field;
        } else {
            $field = '{!complexphrase inOrder=false}' . $field;
        }

        return $field;
    }
}
