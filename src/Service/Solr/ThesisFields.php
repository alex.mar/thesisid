<?php
declare(strict_types=1);

namespace App\Service\Solr;

class ThesisFields
{
    public const FIELD_ID = 'id';
    public const FIELD_TITLE = 'title';
    public const FIELD_SEO_KEYWORDS = 'seo_keywords';
    public const FIELD_SEO_DESCRIPTION = 'seo_description';
    public const FIELD_THESIS_TEXT = 'thesis_text';
    public const FIELD_THESIS_BASH = 'thesis_bash';
    public const FIELD_SHOW = 'show';
    public const FIELD_SOURCE = 'source';
    public const FIELD_THESIS = 'thesis';
}
