<?php
declare(strict_types=1);

namespace App\Service\Formatter;

use App\Repository\ThesisTypeRepository;
use App\Service\Solr\Response\SolrResponse;
use App\Service\Solr\Response\SolrResponseCollection;
use App\Service\ThesisType\ThesisTypeService;

class ThesisDocsFormatter
{

    public function __construct(
        private ThesisTypeService $thesisTypeService
    ) {
    }

    public function format(SolrResponseCollection $solrResponseCollection): array
    {
        $solrResponseCollection = $this->sort($solrResponseCollection);
        $result = [];
        foreach ($solrResponseCollection->getArrayCopy() as $solrResponse) {
            $docs = [];
            $thesisVOCollection = $solrResponse->getDocs();
            foreach ($thesisVOCollection->getArrayCopy() as $thesisVO) {
                $docs[] = $thesisVO->toArray();
            }
            if (!empty($docs)) {
                $result[$solrResponse->getType()] = [
                    'docs' => $docs
                ];
            }
        }

        return $result;
    }

    private function sort(SolrResponseCollection $solrResponseCollection): SolrResponseCollection
    {
        $thesisTypes = $this->thesisTypeService->getAllTypeNames();
        $sortedSolrResponseCollection = new SolrResponseCollection();
        $indexedSolrResponse = [];
        foreach ($solrResponseCollection->getArrayCopy() as $solrResponse) {
            $indexedSolrResponse[$solrResponse->getType()] = $solrResponse;
        }
        foreach ($thesisTypes as $thesisType) {
            if (isset($indexedSolrResponse[$thesisType])) {
                $sortedSolrResponseCollection->append($indexedSolrResponse[$thesisType]);
            }
        }

        return $sortedSolrResponseCollection;
    }
}
