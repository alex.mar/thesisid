<?php
declare(strict_types=1);

namespace App\Service\Seo\VO;

class SeoVO
{
    public function __construct(
        private string $title,
        private string $keywords,
        private string $description
    ) {
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getKeywords(): string
    {
        return $this->keywords;
    }

    public function getDescription(): string
    {
        return $this->description;
    }
}
