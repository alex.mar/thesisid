<?php
declare(strict_types=1);

namespace App\Service\Seo;

use App\Repository\SeoRepository;
use App\Service\Seo\VO\SeoVO;
use App\Service\Thesis\VO\ThesisVO;

class SeoBuilder
{

    public function __construct(
        private SeoRepository $seoRepository
    ) {
    }

    public function buildByUrl(string $url): SeoVO
    {
        $seoEntity = $this->seoRepository->getSeoByUrl($url);
        if ($seoEntity === null) {
            $seoVO = $this->buildDefault();
        } else {
            $seoVO = new SeoVO(
                $seoEntity->getTitle(),
                $seoEntity->getKeywords(),
                $seoEntity->getDescription(),
            );
        }

        return $seoVO;
    }

    public function buildBySearch(string $query): SeoVO
    {
        return new SeoVO(
            $query . ' - поиск в ThesisID',
            '',
            '',
        );
    }

    public function buildDefault(): SeoVO
    {
        return new SeoVO(
            'ThesisID',
            'thesis, thesisid, поиск, тезисы',
            'Поиск ответов'
        );
    }
}
