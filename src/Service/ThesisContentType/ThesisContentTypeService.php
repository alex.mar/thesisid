<?php
declare(strict_types=1);

namespace App\Service\ThesisContentType;

use App\Entity\ThesisContentTypeEntity;
use App\Repository\ThesisContentTypeRepository;
use Exception;

class ThesisContentTypeService
{
    /** @var ThesisContentTypeEntity[] */
    private array $allThesisContentTypes = [];

    public function __construct(
        private ThesisContentTypeRepository $thesisContentTypeRepository
    ) {
        $this->init();
    }


    private function init(): void
    {
        $allThesisContentTypes = $this->thesisContentTypeRepository->findAll();
        $indexedAllThesisContentTypes = [];
        foreach ($allThesisContentTypes as $entity) {
            $indexedAllThesisContentTypes[$entity->getId()] = $entity;
        }
        $this->allThesisContentTypes = $indexedAllThesisContentTypes;
    }

    /**
     * @return ThesisContentTypeEntity[]
     */
    public function getAll(): array
    {
        return $this->allThesisContentTypes;
    }

    /**
     * @throws Exception
     */
    public function getThesisTypeNameById(int $id): string
    {
        $name = '';
        foreach ($this->allThesisContentTypes as $entity) {
            if ($entity->getId() === $id) {
                $name = $entity->getName();
            }
        }

        if (empty($name)) {
            throw new Exception('Thesis content type id not defined');
        }

        return $name;
    }
}
