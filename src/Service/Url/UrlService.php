<?php
declare(strict_types=1);

namespace App\Service\Url;

class UrlService
{
    public function getDomainFromUrl(string $url): string
    {
        $parse = parse_url($url);
        return preg_replace('/^www\.(.+\.)/i', '$1', $parse['host']);
    }
}
