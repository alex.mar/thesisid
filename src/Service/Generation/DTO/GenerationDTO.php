<?php
declare(strict_types=1);

namespace App\Service\Generation\DTO;

class GenerationDTO
{
    public function __construct(
        private string $link,
        private string $query
    ) {
    }

    public function getLink(): string
    {
        return $this->link;
    }

    public function getQuery(): string
    {
        return $this->query;
    }
}
