<?php
declare(strict_types=1);

namespace App\Service\Generation\Meta;

use App\Entity\ThesisMetaEntity;
use App\Service\Generation\DTO\GenerationDTO;
use App\Service\GoogleTranslate\GoogleTranslateService;
use cijic\phpMorphy\Morphy;
use DOMDocument;
use LanguageDetector\LanguageDetector;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class MetaGenerator
{
    public function __construct(
        private HttpClientInterface $httpClient,
        private TranslatorInterface $translator,
        private GoogleTranslateService $googleTranslateService
    ) {
    }

    public function generate(GenerationDTO $generationDTO): ThesisMetaEntity
    {
        $thesisMetaEntity = new ThesisMetaEntity();

        $response = $this->httpClient->request('GET', $generationDTO->getLink());
        if ($response->getStatusCode() === 200) {
            $detector = new LanguageDetector();

            $pageContent = $response->getContent();
            libxml_use_internal_errors(true);
            $dom = new DomDocument();
            $dom->loadHTML($pageContent);
            $htmlAttributes = [];
            foreach ($dom->getElementsByTagName('html') as $tag) {
                foreach ($tag->attributes as $attribName => $attribNodeVal)
                {
                    $htmlAttributes[$attribName]=$tag->getAttribute($attribName);
                }
            }
            $contentLang = $htmlAttributes['lang'] ?? null;
            if ($contentLang === null) {
                $contentLang = $detector->evaluate($dom->textContent)->getLanguage()->getCode();
            }

            if ($contentLang !== null) {
                $title = $this->buildTitle($generationDTO->getQuery(), $contentLang);
                $description = $this->buildDescription($generationDTO, $contentLang);
                $thesisMetaEntity->setTitle($title);
                $thesisMetaEntity->setDescription($description);

                $queryLang = $detector->evaluate($generationDTO->getQuery())->getLanguage()->getCode();
                if ($queryLang !== 'en') {
                    $translatedQuery = $this->googleTranslateService->translate([$generationDTO->getQuery()], 'en');
                    $translatedQuery = current($translatedQuery);

                    $keywordsPartSpeech = ['NOUN', 'VERB'];
                    $morphy = new Morphy('en');
                    $queryParts = explode(' ', $translatedQuery);
                    $keywords = [];
                    foreach ($queryParts as $queryPart) {
                        $originalQueryPart = $queryPart;
                        $queryPart = strtoupper($queryPart);
                        $baseForm = $morphy->getBaseForm($queryPart);
                        if ($baseForm !== false) {
                            $baseFormWord = current($baseForm);
                            $grammarInfo = $morphy->getGramInfoMergeForms($baseFormWord);
                            if ($grammarInfo !== false) {
                                foreach ($grammarInfo as $item) {
                                    if (isset($item['pos']) && in_array($item['pos'], $keywordsPartSpeech, true)) {
                                        $keywords[] = $originalQueryPart;
                                    }
                                    continue 2;
                                }
                            }
                        }
                    }
                    foreach ($queryParts as $key => $queryPart) {
                        if (is_numeric($queryPart)) {
                            $previousWord = $queryParts[$key - 1];
                            $keyKeyword = array_search($previousWord, $keywords, true);
                            if ($keyKeyword !== false) {
                                $keywords[$keyKeyword] = $keywords[$keyKeyword] . ' ' . $queryPart;
                                continue;
                            }
                            $nextWord = $queryParts[$key + 1];
                            $keyKeyword = array_search($nextWord, $keywords, true);
                            if ($keyKeyword !== false) {
                                $keywords[$keyKeyword] = $keywords[$keyKeyword] . ' ' . $queryPart;
                            }
                        }
                    }

                    $translatedKeywords = $this->googleTranslateService->translate($keywords, $contentLang);
                    $thesisMetaEntity->setKeywords(implode(', ', $translatedKeywords));
                }
            }
        }

        return $thesisMetaEntity;
    }

    private function buildTitle(string $query, string $lang): string
    {
        return $this->translator->trans('Thesis on request', [], 'messages', $this->buildTranslationCode($lang)) . ': '
            . $query
            . ' | ThesisID';
    }

    private function buildDescription(GenerationDTO $generationDTO, string $lang): string
    {
        $serviceDomain = parse_url($generationDTO->getLink(), PHP_URL_HOST);
        $serviceName = preg_replace('#^www\.(.+\.)#i', '$1', $serviceDomain);
        $serviceName = ucfirst($serviceName);
        $firstPart = $this->translator->trans('Main information on the topic', [], 'messages', $this->buildTranslationCode($lang));
        $secondPart = $this->translator->trans('The thesis is generated from the service', [], 'messages', $this->buildTranslationCode($lang));
        return $firstPart. ': ' . $generationDTO->getQuery() . '. ' .  $secondPart . ' ' . $serviceName;
    }

    private function buildTranslationCode(string $lang): string
    {
        return $lang . '_' . strtoupper($lang);
    }
}
