<?php
declare(strict_types=1);

namespace App\Service\ContentUpdate\ThesisType;

use App\Entity\ThesisEntity;
use App\Service\ContentUpdate\ContentType\ContentTypeUpdateItemInterface;
use App\Service\Thesis\ThesisContentVOBuilder;

class DefaultTypeUpdateItem implements ThesisTypeUpdateInterface
{
    private const KEY = 'DefaultTypeUpdateItem';

    /**
     * @param ContentTypeUpdateItemInterface[] $contentUpdateItems
     */
    public function __construct(
        private ThesisContentVOBuilder $thesisContentVOBuilder,
        private array $contentUpdateItems
    ) {
    }

    public function actualizeContent(ThesisEntity $thesisEntity, array $contentItems): void
    {
        $thesisContentVOs = $this->thesisContentVOBuilder->buildFromJson($contentItems, $thesisEntity->getId());
        foreach ($this->contentUpdateItems as $contentUpdateItem) {
            $contentUpdateItem->update($thesisContentVOs, $thesisEntity->getId());
        }
    }

    public static function getKey(): string
    {
        return self::KEY;
    }
}
