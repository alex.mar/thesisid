<?php
declare(strict_types=1);

namespace App\Service\ContentUpdate\ThesisType;

use App\Entity\ThesisEntity;

interface ThesisTypeUpdateInterface
{
    public static function getKey(): string;
    public function actualizeContent(ThesisEntity $thesisEntity, array $contentItems): void;
}