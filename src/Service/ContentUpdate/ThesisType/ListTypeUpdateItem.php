<?php
declare(strict_types=1);

namespace App\Service\ContentUpdate\ThesisType;

use App\Entity\ThesisEntity;
use App\Entity\ThesisListEntity;
use App\Repository\ThesisListRepository;
use App\Service\ContentUpdate\ContentType\ContentTypeUpdateItemInterface;
use App\Service\Thesis\ThesisContentVOBuilder;
use App\Service\ThesisContentType\ThesisContentTypeService;

class ListTypeUpdateItem implements ThesisTypeUpdateInterface
{
    private const KEY = 'ListTypeUpdateItem';

    /**
     * @param ContentTypeUpdateItemInterface[] $contentUpdateItems
     */
    public function __construct(
        private ThesisContentTypeService $thesisContentTypeService,
        private ThesisListRepository $thesisListRepository,
        private ThesisContentVOBuilder $thesisContentVOBuilder,
        private array $contentUpdateItems
    ) {
    }

    public function actualizeContent(ThesisEntity $thesisEntity, array $contentItems): void
    {
        $allThesisContentTypes = $this->thesisContentTypeService->getAll();
        $thesisContentTypeIdByName = [];
        foreach ($allThesisContentTypes as $entity) {
            $thesisContentTypeIdByName[$entity->getName()] = $entity->getId();
        }

        $thesisListEntities = $this->thesisListRepository->getAllByThesisId($thesisEntity->getId());
        $thesisContentVOs = $this->thesisContentVOBuilder
            ->buildFromJson($contentItems, $thesisEntity->getId());
        foreach ($this->contentUpdateItems as $contentUpdateItem) {
            $contentUpdateItem->update($thesisContentVOs, $thesisEntity->getId());
        }
        foreach ($contentItems as $index => $contentItemJson) {
            $contentItem = json_decode($contentItemJson, true);
            $listId = $contentItem['listId'] ?? null;
            if ($listId) {
                $thesisList = $thesisListEntities[$listId];
                unset($thesisListEntities[$listId]);
            } else {
                $thesisList = new ThesisListEntity();
                $thesisList->setThesisId($thesisEntity->getId());
            }
            if (isset($thesisContentVOs[$index])) {
                $thesisList->setContentTypeId($thesisContentTypeIdByName[$contentItem['type']]);
                $thesisList->setContentId($thesisContentVOs[$index]->getId());
            } else {
                $thesisList->setContentTypeId(null);
                $thesisList->setContentId(null);
            }

            $thesisList->setTitle($contentItem['title']);
            $thesisList->setOrder((int) $contentItem['order']);

            $this->thesisListRepository->add($thesisList);
        }

        foreach ($thesisListEntities as $entity) {
            $this->thesisListRepository->remove($entity);
        }
    }

    public static function getKey(): string
    {
        return self::KEY;
    }
}
