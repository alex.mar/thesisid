<?php
declare(strict_types=1);

namespace App\Service\ContentUpdate\ContentType;

use App\Entity\ThesisBashEntity;
use App\Repository\ThesisBashRepository;

class BashContentUpdateItem extends AbstractContentUpdateItem
{
    private const TYPE = 'bash';

    public function __construct(
        private ThesisBashRepository $thesisBashRepository
    ) {
    }

    public function getType(): string
    {
        return self::TYPE;
    }

    public function update(array $thesisContentVOs, int $thesisId): void
    {
        $thesisContentVOs = $this->filterContent($thesisContentVOs);
        if (empty($thesisContentVOs)) {
            return;
        }

        $allBashContentEntities = $this->thesisBashRepository->getAllByThesisId($thesisId);
        foreach ($thesisContentVOs as $contentVO) {
            if ($contentVO->getId()) {
                $entity = $allBashContentEntities[$contentVO->getId()];
                unset($allBashContentEntities[$contentVO->getId()]);
            } else {
                $entity = new ThesisBashEntity();
                $entity->setThesisId($thesisId);
            }
            $entity->setCommand($contentVO->getContent()[ThesisBashRepository::FIELD_COMMAND]);
            $entity->setOrder($contentVO->getOrder());
            $this->thesisBashRepository->add($entity);
            $contentVO->setId($entity->getId());
        }
        foreach ($allBashContentEntities as $entity) {
            $this->thesisBashRepository->remove($entity);
        }
    }
}
