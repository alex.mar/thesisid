<?php
declare(strict_types=1);

namespace App\Service\ContentUpdate\ContentType;

use App\Entity\ThesisTextEntity;
use App\Repository\ThesisTextRepository;
use App\Service\Thesis\VO\ThesisContentVO;

class TextContentUpdateItem extends AbstractContentUpdateItem
{
    private const TYPE = 'text';

    public function __construct(
        private ThesisTextRepository $thesisTextRepository
    ) {
    }

    public function update(array $thesisContentVOs, int $thesisId): void
    {
        $thesisContentVOs = $this->filterContent($thesisContentVOs);
        if (empty($thesisContentVOs)) {
            return;
        }

        $allTextContentEntities = $this->thesisTextRepository->getAllByThesisId($thesisId);
        foreach ($thesisContentVOs as $contentVO) {
            if ($contentVO->getId()) {
                $entity = $allTextContentEntities[$contentVO->getId()];
                unset($allTextContentEntities[$contentVO->getId()]);
            } else {
                $entity = new ThesisTextEntity();
                $entity->setThesisId($thesisId);
            }
            $entity->setText($contentVO->getContent()[ThesisTextRepository::FIELD_TEXT]);
            $entity->setOrder($contentVO->getOrder());
            $this->thesisTextRepository->add($entity);
            $contentVO->setId($entity->getId());
        }
        foreach ($allTextContentEntities as $entity) {
            $this->thesisTextRepository->remove($entity);
        }
    }

    public function getType(): string
    {
        return self::TYPE;
    }
}
