<?php
declare(strict_types=1);

namespace App\Service\ContentUpdate\ContentType;

use App\Entity\ThesisPhpEntity;
use App\Repository\ThesisPhpRepository;

class PhpContentUpdateItem extends AbstractContentUpdateItem
{
    private const TYPE = 'php';

    public function __construct(
        private ThesisPhpRepository $thesisPhpRepository
    ) {
    }

    public function getType(): string
    {
        return self::TYPE;
    }

    public function update(array $thesisContentVOs, int $thesisId): void
    {
        $thesisContentVOs = $this->filterContent($thesisContentVOs);
        if (empty($thesisContentVOs)) {
            return;
        }

        $allPhpContentEntities = $this->thesisPhpRepository->getAllByThesisId($thesisId);
        foreach ($thesisContentVOs as $contentVO) {
            if ($contentVO->getId()) {
                $entity = $allPhpContentEntities[$contentVO->getId()];
                unset($allPhpContentEntities[$contentVO->getId()]);
            } else {
                $entity = new ThesisPhpEntity();
                $entity->setThesisId($thesisId);
            }
            $entity->setCode($contentVO->getContent()[ThesisPhpRepository::FIELD_CODE]);
            $entity->setOrder($contentVO->getOrder());
            $this->thesisPhpRepository->add($entity);
            $contentVO->setId($entity->getId());
        }
        foreach ($allPhpContentEntities as $entity) {
            $this->thesisPhpRepository->remove($entity);
        }
    }
}
