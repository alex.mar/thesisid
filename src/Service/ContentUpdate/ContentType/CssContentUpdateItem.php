<?php
declare(strict_types=1);

namespace App\Service\ContentUpdate\ContentType;

use App\Entity\ThesisCssEntity;
use App\Repository\ThesisCssRepository;

class CssContentUpdateItem extends AbstractContentUpdateItem
{
    private const TYPE = 'css';

    public function __construct(
        private ThesisCssRepository $thesisCssRepository
    ) {
    }

    public function getType(): string
    {
        return self::TYPE;
    }

    public function update(array $thesisContentVOs, int $thesisId): void
    {
        $thesisContentVOs = $this->filterContent($thesisContentVOs);
        if (empty($thesisContentVOs)) {
            return;
        }

        $allCssContentEntities = $this->thesisCssRepository->getAllByThesisId($thesisId);
        foreach ($thesisContentVOs as $contentVO) {
            if ($contentVO->getId()) {
                $entity = $allCssContentEntities[$contentVO->getId()];
                unset($allCssContentEntities[$contentVO->getId()]);
            } else {
                $entity = new ThesisCssEntity();
                $entity->setThesisId($thesisId);
            }
            $entity->setStyle($contentVO->getContent()[ThesisCssRepository::FIELD_STYLE]);
            $entity->setOrder($contentVO->getOrder());
            $this->thesisCssRepository->add($entity);
            $contentVO->setId($entity->getId());
        }
        foreach ($allCssContentEntities as $entity) {
            $this->thesisCssRepository->remove($entity);
        }
    }
}
