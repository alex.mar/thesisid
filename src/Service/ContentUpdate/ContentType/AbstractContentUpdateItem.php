<?php
declare(strict_types=1);

namespace App\Service\ContentUpdate\ContentType;

use App\Service\Thesis\VO\ThesisContentVO;

abstract class AbstractContentUpdateItem implements ContentTypeUpdateItemInterface
{
    abstract public function getType(): string;

    /**
     * @param ThesisContentVO[] $contentVOs
     * @return ThesisContentVO[]
     */
    public function filterContent(array $contentVOs): array
    {
        $filtered = [];
        foreach ($contentVOs as $contentVO) {
            if ($contentVO->getType() === $this->getType()) {
                $filtered[] = $contentVO;
            }
        }

        return $filtered;
    }
}
