<?php
declare(strict_types=1);

namespace App\Service\ContentUpdate\ContentType;

use App\Entity\ThesisHtmlEntity;
use App\Repository\ThesisHtmlRepository;

class HtmlContentUpdateItem extends AbstractContentUpdateItem
{
    private const TYPE = 'html';

    public function __construct(
        private ThesisHtmlRepository $thesisHtmlRepository
    ) {
    }

    public function getType(): string
    {
        return self::TYPE;
    }

    public function update(array $thesisContentVOs, int $thesisId): void
    {
        $thesisContentVOs = $this->filterContent($thesisContentVOs);
        if (empty($thesisContentVOs)) {
            return;
        }

        $allHtmlContentEntities = $this->thesisHtmlRepository->getAllByThesisId($thesisId);
        foreach ($thesisContentVOs as $contentVO) {
            if ($contentVO->getId()) {
                $entity = $allHtmlContentEntities[$contentVO->getId()];
                unset($allHtmlContentEntities[$contentVO->getId()]);
            } else {
                $entity = new ThesisHtmlEntity();
                $entity->setThesisId($thesisId);
            }
            $entity->setCode($contentVO->getContent()[ThesisHtmlRepository::FIELD_CODE]);
            $entity->setOrder($contentVO->getOrder());
            $this->thesisHtmlRepository->add($entity);
            $contentVO->setId($entity->getId());
        }
        foreach ($allHtmlContentEntities as $entity) {
            $this->thesisHtmlRepository->remove($entity);
        }
    }
}
