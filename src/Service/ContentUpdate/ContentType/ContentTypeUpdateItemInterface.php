<?php
declare(strict_types=1);

namespace App\Service\ContentUpdate\ContentType;

interface ContentTypeUpdateItemInterface
{
    public function update(array $thesisContentVOs, int $thesisId): void;
}
