<?php
declare(strict_types=1);

namespace App\Service\ContentUpdate;

use App\Entity\ThesisEntity;
use App\Repository\ThesisTypeRepository;
use App\Service\ContentUpdate\ThesisType\DefaultTypeUpdateItem;
use App\Service\ContentUpdate\ThesisType\ListTypeUpdateItem;
use App\Service\ContentUpdate\ThesisType\ThesisTypeUpdateInterface;
use App\Service\ThesisType\ThesisTypeService;
use Exception;

class ContentUpdateService
{
    /**
     * @param ThesisTypeService $thesisTypeService
     * @param ThesisTypeUpdateInterface[] $contentActualizer
     */
    public function __construct(
        private ThesisTypeService $thesisTypeService,
        private array $contentActualizer
    ) {
    }

    /**
     * @throws Exception
     */
    public function update(ThesisEntity $thesisEntity, array $contentItems): void
    {
        $thesisType = $this->thesisTypeService->getThesisTypeNameById($thesisEntity->getTypeId());
        $contentActualizer = $this->defineContentActualizer($thesisType);
        $contentActualizer->actualizeContent($thesisEntity, $contentItems);
    }

    /**
     * @throws Exception
     */
    private function defineContentActualizer(string $thesisType): ThesisTypeUpdateInterface
    {
        $actualizer = null;
        $key = match ($thesisType) {
            ThesisTypeRepository::TYPE_LIST => ListTypeUpdateItem::getKey(),
            ThesisTypeRepository::TYPE_DEFINITION,
            ThesisTypeRepository::TYPE_DEFAULT => DefaultTypeUpdateItem::getKey(),
            default => null,
        };

        if ($key !== null) {
            $actualizer = $this->getActualizerByKey($key);
        }

        if ($actualizer === null) {
            throw new Exception('Content actualizer not defined');
        }

        return $actualizer;
    }

    private function getActualizerByKey(string $key): ?ThesisTypeUpdateInterface
    {
        $result = null;
        foreach ($this->contentActualizer as $contentActualizer) {
            if ($contentActualizer::getKey() === $key) {
                $result = $contentActualizer;
                break;
            }
        }

        return $result;
    }
}
