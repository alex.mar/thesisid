<?php
declare(strict_types=1);

namespace App\Controller\Admin;

use App\Repository\AdminUserRepository;
use App\Service\Authorization\AuthorizationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LoginController extends AbstractController
{
    public function __construct(
        private AdminUserRepository $adminUserRepository,
        private AuthorizationService $authorizationService
    ) {
    }

    #[Route('/admin/login/', name: 'app_admin_login')]
    public function index(Request $request): Response
    {
        $submit = (bool) $request->get('submit');
        $email = $request->get('login');
        $password = $request->get('password');
        if ($submit) {
            $response = new JsonResponse();
            $url = $this->generateUrl('app_admin_login');
            $user = $this->adminUserRepository->findOneBy(['email' => $email, 'password' => md5($password)]);
            $message = '';
            if ($user !== null) {
                $xid = $this->authorizationService->generateXid();
                $user->setXid($xid);
                $this->adminUserRepository->update($user);
                $url = $this->generateUrl('app_admin_home');
                $response->headers->setCookie(new Cookie('xid', $xid));
                $status = 'success';
            } else {
                $status = 'invalid';
                $message = 'Invalid email and password';
            }
            $response->setData([
                'status' => $status,
                'message' => $message,
                'url' => $url
            ]);
            return $response;
        }
        return $this->render('admin/login/login.html.twig');
    }

    #[Route('/admin/logout/', name: 'app_admin_logout')]
    public function logout(): Response
    {
        $response = $this->redirectToRoute('app_admin_login');
        $response->headers->clearCookie('xid');

        return $response;
    }
}
