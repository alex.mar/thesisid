<?php
declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\SeoEntity;
use App\Repository\SeoRepository;
use App\Repository\ThesisRepository;
use App\Service\Authorization\AuthorizationService;
use App\Service\Complete\CompleteService;
use App\Service\Menu\AdminMenuService;
use App\Service\RabbitMQ\RabbitMQClient;
use App\Service\RabbitMQ\Task\Task;
use App\Service\Search\ConditionBlock\Thesis\ThesisConditionBlock;
use App\Service\Search\Context\SearchContextBuilder;
use App\Service\Search\Level\SearchByCompleteLevel;
use App\Service\Search\Level\SearchLevelBuilder;
use App\Service\Solr\Client\SolrClientBuilder;
use App\Service\Solr\Query\Condition\Condition;
use App\Service\Solr\Query\QueryCollection;
use App\Service\Solr\Response\SolrResponseBuilder;
use App\Service\Solr\ThesisFields;
use App\Service\Thesis\ThesisService;
use App\Service\Thesis\ThesisVOBuilder;
use App\Service\ThesisType\ThesisTypeService;
use SolrQuery;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class AjaxController extends AbstractController
{
    public function __construct(
        protected RequestStack $requestStack,
        protected Environment $template,
        protected AuthorizationService $authorizationService,
        protected AdminMenuService $adminMenuService,
        private ThesisRepository $thesisRepository,
        private RabbitMQClient $rabbitMQClient,
    ) {
    }

    #[Route('/admin/thesis/seo/main/save/', name: 'app_admin_seo_main_save')]
    public function seoMainSave(
        Request $request,
        SeoRepository $seoRepository
    ): Response {
        $urlId = (int) $request->get('url');
        $title = $request->get('title');
        $keywords = $request->get('keywords');
        $description = $request->get('description');

        $seoEntity = $seoRepository->findOneBy(['urlId' => $urlId]);
        if ($seoEntity === null) {
            $seoEntity = new SeoEntity();
            $seoEntity->setUrlId($urlId);
        }
        $seoEntity->setTitle($title);
        $seoEntity->setKeywords($keywords);
        $seoEntity->setDescription($description);

        $seoRepository->add($seoEntity);

        return new JsonResponse([
            'message' => 'Seo successfully saved'
        ]);
    }

    #[Route('/admin/thesis/list/all/search-by-context/', name: 'app_admin_list_all_search_by_context')]
    public function thesisListAllSearchByContext(
        Request $request,
        ThesisRepository $thesisRepository,
        ThesisVOBuilder $thesisVOBuilder,
        CompleteService $completeService,
        SearchLevelBuilder $searchLevelBuilder,
        SearchContextBuilder $searchContextBuilder
    ): Response {
        $data = $request->getContent();
        $data = json_decode($data, true);
        $query = $data['query'] ?? '';
        $source = $data['source'] ?? '';

        $docs = [];
        if (!empty($source)) {
            $thesisIds = $thesisRepository->getThesisIdsBySource($source);
            $thesisVOCollection = $thesisVOBuilder->buildByIds($thesisIds, false);
            if (!empty($query)) {
                foreach ($thesisVOCollection->getArrayCopy() as $thesisVO) {
                    $thesisQueries = $thesisVO->getQueries();
                    foreach ($thesisQueries as $thesisQueryVO) {
                        if ($completeService->compare($query, $thesisQueryVO->getQuery())) {
                            $docs[] = $thesisVO->toArray();
                            continue 2;
                        }
                    }
                }
            } else {
                foreach ($thesisVOCollection->getArrayCopy() as $thesisVO) {
                    $docs[] = $thesisVO->toArray();
                }
            }
        } elseif (!empty($query)) {
            $searchByCompleteLevel = $searchLevelBuilder->build(SearchByCompleteLevel::getKey());
            $searchContext = $searchContextBuilder->buildEmpty($query);
            $solrResponseCollection = $searchByCompleteLevel->search($searchContext);
            foreach ($solrResponseCollection->getArrayCopy() as $solrResponse) {
                $thesisVOCollection = $solrResponse->getDocs();
                foreach ($thesisVOCollection->getArrayCopy() as $thesisVO) {
                    $docs[] = $thesisVO->toArray();
                }
            }
        }

        return $this->render('admin/thesis/list/all/list.html.twig', [
            'docs' => $docs
        ]);
    }

    #[Route('/admin/thesis/list/all/load-more/', name: 'app_admin_list_all_load_more')]
    public function thesisListAllLoadMore(
        Request $request,
        ThesisRepository $thesisRepository,
        ThesisVOBuilder $thesisVOBuilder,
        ThesisTypeService $thesisTypeService,
        SolrClientBuilder $solrClientBuilder,
        SolrResponseBuilder $solrResponseBuilder
    ): Response {
        $data = $request->getContent();
        $data = json_decode($data, true);
        $offset = (int) ($data['offset'] ?? 0);
        $query = $data['query'] ?? '';
        $lastId = $data['lastId'] ?? 0;
        $docs = [];
        $totalDocs = 0;
        if (empty($query)) {
            $thesisIds = $thesisRepository->getThesisIds($offset);
            $thesisVOCollection = $thesisVOBuilder->buildByIds($thesisIds, false);
            $docs = $thesisVOCollection->toArray();
        } else {
            $allThesisTypeNames = $thesisTypeService->getAllTypeNames();
            $allIndexedThesisVOs = [];
            foreach ($allThesisTypeNames as $typeName) {
                $coreName = $solrClientBuilder->getThesisCoreByThesisTypeName($typeName);
                $solrClient = $solrClientBuilder->build($coreName);
                $queryCollection = new QueryCollection();
                $queryCollection->append((new ThesisConditionBlock())->getConditionBlock($query));
                $queryCollection->append(new Condition(
                    ThesisFields::FIELD_ID,
                    '[* TO ' . $lastId-- . ']'
                ));
                $solrQuery = new SolrQuery($queryCollection->getQuery());
                $solrQuery->addSortField(ThesisFields::FIELD_ID, 1);
                $solrQuery->addField(ThesisFields::FIELD_ID);

                $queryResponse = $solrClient->query($solrQuery);
                $solrResponse = $solrResponseBuilder->buildFromSolr($queryResponse, $typeName, false);
                if ($solrResponse->getNumFound() === 0) {
                    continue;
                }
                $totalDocs += $solrResponse->getNumFound();
                $thesisVOCollection = $solrResponse->getDocs();
                foreach ($thesisVOCollection->getArrayCopy() as $thesisVO) {
                    $allIndexedThesisVOs[$thesisVO->getId()] = $thesisVO;
                }
            }
            rsort($allIndexedThesisVOs);
            $allIndexedThesisVOs = array_slice($allIndexedThesisVOs, 0, ThesisService::DEFAULT_LIMIT);
            foreach ($allIndexedThesisVOs as $thesisVO) {
                $docs[] = $thesisVO->toArray();
            }
        }

        return new JsonResponse([
            'docs' => $docs,
        ]);
    }

    #[Route('/admin/thesis/list/all/update-status/', name: 'app_admin_list_all_update_status')]
    public function thesisListAllUpdateStatus(
        Request $request,
        ThesisRepository $thesisRepository
    ): Response {
        $data = $request->getContent();
        $data = json_decode($data, true);
        $thesisId = (int) ($data['thesisId'] ?? 0);
        $show = (int) ($data['show'] ?? 0);

        $thesisEntity = $thesisRepository->find($thesisId);
        if ($thesisEntity !== null) {
            $thesisEntity->setStatus($show);
            $thesisRepository->add($thesisEntity);
            $thesisTask = new Task($this->getParameter('thesis_update_queue'), ['id' => $thesisId]);
            $this->rabbitMQClient->doTask($thesisTask);
            $status = 'success';
        } else {
            $status = 'error';
        }

        return new JsonResponse([
            'status' => $status
        ]);
    }
}
