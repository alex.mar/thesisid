<?php
declare(strict_types=1);

namespace App\Controller\Admin;

use App\Service\Authorization\AuthorizationService;
use App\Service\Menu\AdminMenuService;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class IndexController extends BaseController
{
    public function __construct(
        protected RequestStack $requestStack,
        protected Environment $template,
        protected AuthorizationService $authorizationService,
        protected AdminMenuService $adminMenuService
    ) {
        parent::__construct($requestStack, $template, $authorizationService, $adminMenuService);
    }

    #[Route('/admin/', name: 'app_admin_home')]
    public function index(): Response
    {
        return $this->render('admin/base.html.twig');
    }
}
