<?php
declare(strict_types=1);

namespace App\Controller\Admin;

use App\Service\Authorization\AuthorizationService;
use App\Service\Menu\AdminMenuService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;
use Twig\Environment;

class BaseController extends AbstractController
{
    public function __construct(
        private RequestStack $requestStack,
        private Environment $template,
        private AuthorizationService $authorizationService,
        private AdminMenuService $adminMenuService
    ) {
        $request = $this->requestStack->getCurrentRequest();

        $this->authorizationService->checkAuth($request);
        $menu = $this->adminMenuService->loadMenu($request);

        $this->template->addGlobal('menu', $menu);
    }
}
