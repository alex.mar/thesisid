<?php
declare(strict_types=1);

namespace App\Controller\Admin\Generation;

use App\Controller\Admin\BaseController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends BaseController
{
    #[Route('/admin/generation/', name: 'app_admin_generation')]
    public function index(): Response
    {
        return $this->redirectToRoute('app_admin_generation_by_query');
    }
}
