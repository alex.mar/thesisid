<?php
declare(strict_types=1);

namespace App\Controller\Admin\Generation;

use App\Controller\Admin\BaseController;
use App\Entity\CompleteEntity;
use App\Entity\GenerationEntity;
use App\Repository\CompleteRepository;
use App\Repository\GenerationRepository;
use App\Repository\ThesisCompleteRepository;
use App\Repository\ThesisRepository;
use App\Service\Complete\CompleteService;
use App\Service\RabbitMQ\RabbitMQClient;
use App\Service\RabbitMQ\Task\Task;
use App\Service\Search\VO\CompleteVOBuilder;
use App\Service\Url\UrlService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ByQueryController extends BaseController
{
    private const LIMIT_GENERATION_SOURCE = 10;

    #[Route('/admin/generation/by-query/', name: 'app_admin_generation_by_query')]
    public function index(
        Request $request,
        HttpClientInterface $httpClient,
        GenerationRepository $generationRepository,
        CompleteService $completeService,
        ThesisCompleteRepository $thesisCompleteRepository,
        ThesisRepository $thesisRepository,
        CompleteRepository $completeRepository,
        UrlService $urlService,
        CompleteVOBuilder $completeVOBuilder,
        RabbitMQClient $rabbitMQClient
    ): Response {
        $query = $request->get('query');
        $result = [];

        if (!empty($query)) {
            $completeVO = $completeService->isExistComplete($query);
            $existLinks = [];
            if ($completeVO !== null) {
                $thesisIds = $thesisCompleteRepository->getThesisIdsByCompleteId($completeVO->getCompleteId());
                $thesisSources = $thesisRepository->getThesisSourcesByIds($thesisIds);
                $existGenerationLinks = $generationRepository->getLinksByCompleteId($completeVO->getCompleteId());
                foreach ($thesisSources as $source) {
                    $key = array_search($source, $existLinks, true);
                    if ($key === false) {
                        $existLinks[] = $source;
                        $generationId = array_search($source, $existGenerationLinks, true);
                        if ($generationId === false) {
                            $newGeneration = new GenerationEntity();
                            $newGeneration->setCompleteId($completeVO->getCompleteId());
                            $newGeneration->setLink($source);
                            $generationRepository->add($newGeneration);
                            $generationId = $newGeneration->getId();
                        } else {
                            unset($existGenerationLinks[$generationId]);
                        }
                        $result[] = [
                            'id' => $generationId,
                            'title' => ucfirst($urlService->getDomainFromUrl($source)),
                            'link' => $source,
                            'count' => 1
                        ];
                    } else {
                        $result[$key]['count']++;
                    }
                }
                if (!empty($existGenerationLinks)) {
                    foreach ($existGenerationLinks as $generationId => $link) {
                        $result[] = [
                            'id' => $generationId,
                            'title' => ucfirst($urlService->getDomainFromUrl($link)),
                            'link' => $link,
                            'count' => 0
                        ];
                    }
                }
            } else {
                $complete = new CompleteEntity();
                $complete->setQuery($query);
                $completeRepository->add($complete);
                $completeTask = new Task(
                    $this->getParameter('complete_update_queue'),
                    ['id' => [$complete->getId()]]
                );
                $rabbitMQClient->doTask($completeTask);
                $completeVO = $completeVOBuilder->buildFromEntity($complete);
            }

            if (count($result) < self::LIMIT_GENERATION_SOURCE) {
                $url = 'https://www.googleapis.com/customsearch/v1?key='
                    . $this->getParameter('google_api_key')
                    . '&cx=' . $this->getParameter('google_search_key')
                    . '&q=' . $query;

                $response = $httpClient->request('GET', $url);

                $statusCode = $response->getStatusCode();
                if ($statusCode === 200) {
                    $content = $response->toArray();
                    $newLinks = [];
                    foreach ($content['items'] as $item) {
                        $newLink = $item['link'];
                        if (in_array($newLink, $existLinks, true)) {
                            continue;
                        }
                        $newLinks[] = $newLink;
                    }
                    $needLinks = self::LIMIT_GENERATION_SOURCE - count($existLinks);
                    $newLinks = array_slice($newLinks, 0, $needLinks);
                    foreach ($newLinks as $source) {
                        $newGeneration = new GenerationEntity();
                        $newGeneration->setCompleteId($completeVO->getCompleteId());
                        $newGeneration->setLink($source);
                        $generationRepository->add($newGeneration);
                        $generationId = $newGeneration->getId();

                        $result[] = [
                            'id' => $generationId,
                            'title' => ucfirst($urlService->getDomainFromUrl($source)),
                            'link' => $source,
                            'count' => 0
                        ];
                    }
                }
            }
        }

        return $this->render('admin/generation/by_query/index.html.twig', [
            'generation' => $result
        ]);
    }
}
