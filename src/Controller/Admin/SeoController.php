<?php
declare(strict_types=1);

namespace App\Controller\Admin;

use App\Repository\SeoRepository;
use App\Repository\UrlRepository;
use App\Service\Authorization\AuthorizationService;
use App\Service\Menu\AdminMenuService;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class SeoController extends BaseController
{
    public function __construct(
        protected RequestStack $requestStack,
        protected Environment $template,
        protected AuthorizationService $authorizationService,
        protected AdminMenuService $adminMenuService,
        private UrlRepository $urlRepository,
        private SeoRepository $seoRepository
    ) {
        parent::__construct($requestStack, $template, $authorizationService, $adminMenuService);
    }

    #[Route('/admin/seo/', name: 'app_admin_seo')]
    public function seo(): Response
    {
        return $this->redirectToRoute('app_admin_seo_main');
    }

    #[Route('/admin/seo/main/', name: 'app_admin_seo_main')]
    public function seoMain(): Response
    {
        $urls = $this->urlRepository->findAll();
        $firstUrlId = $urls[0]->getId();
        $seo = $this->seoRepository->findOneBy(['urlId' => $firstUrlId]);

        return $this->render('admin/seo/seo_main.html.twig', [
            'urls' => $urls,
            'seo' => $seo
        ]);
    }
}
