<?php
declare(strict_types=1);

namespace App\Controller\Admin;

use App\Repository\ThesisRepository;
use App\Service\Authorization\AuthorizationService;
use App\Service\Menu\AdminMenuService;
use App\Service\Thesis\ThesisVOBuilder;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class ThesisController extends BaseController
{
    public function __construct(
        protected RequestStack $requestStack,
        protected Environment $template,
        protected AuthorizationService $authorizationService,
        protected AdminMenuService $adminMenuService,
        private ThesisRepository $thesisRepository,
        private ThesisVOBuilder $thesisVOBuilder
    ) {
        parent::__construct($requestStack, $template, $authorizationService, $adminMenuService);
    }

    #[Route('/admin/thesis/list/', name: 'app_admin_thesis_list')]
    public function list(): Response
    {
        return $this->redirectToRoute('app_admin_thesis_list_all');
    }

    #[Route('/admin/thesis/list/all/', name: 'app_admin_thesis_list_all')]
    public function listAll(
        ThesisRepository $thesisRepository
    ): Response {
        $thesisIds = $thesisRepository->getThesisIds();
        $thesisVOCollection = $this->thesisVOBuilder->buildByIds($thesisIds, false);

        $count = $this->thesisRepository->getAllCountThesis();

        return $this->render('admin/thesis/list/all/index.html.twig', [
            'docs' => $thesisVOCollection->toArray(),
            'count' => $count
        ]);
    }
}
