<?php
declare(strict_types=1);

namespace App\Controller\Admin\Thesis;

use App\Controller\Admin\BaseController;
use App\Entity\ThesisCompleteEntity;
use App\Entity\ThesisEntity;
use App\Entity\ThesisMetaEntity;
use App\Entity\ThesisStateEntity;
use App\Repository\CompleteRepository;
use App\Repository\GenerationRepository;
use App\Repository\ThesisCompleteRepository;
use App\Repository\ThesisRepository;
use App\Repository\ThesisMetaRepository;
use App\Repository\ThesisStateRepository;
use App\Repository\ThesisTypeRepository;
use App\Service\ContentUpdate\ContentUpdateService;
use App\Service\Generation\Meta\MetaGenerator;
use App\Service\RabbitMQ\RabbitMQClient;
use App\Service\RabbitMQ\Task\Task;
use App\Service\Thesis\ThesisVOBuilder;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContentController extends BaseController
{
    #[Route('/admin/thesis/create/', name: 'app_admin_thesis_create')]
    public function index(
        Request $request,
        GenerationRepository $generationRepository,
        MetaGenerator $metaGenerator
    ): Response {
        $generationId = (int) $request->get('generationId');
        $generationDTO = $generationRepository->getGenerationDataById($generationId);
        $thesisMetaEntity = null;
        $source = null;
        $backLink = $this->generateUrl('app_admin_thesis_list');
        if ($generationDTO !== null) {
            $thesisMetaEntity = $metaGenerator->generate($generationDTO);
            $backLink = $this->generateUrl('app_admin_generation_by_query') . '?query=' . $generationDTO->getQuery();
            $source = $generationDTO->getLink();
        }

        return $this->render('admin/thesis/content/index.html.twig', [
            'thesis' => null,
            'thesisMeta' => $thesisMetaEntity,
            'source' => $source,
            'backLink' => $backLink
        ]);
    }

    #[Route('/admin/thesis/{id}/edit/', name: 'app_admin_thesis', defaults: ['id' => 0])]
    public function thesis(int $id): Response
    {
        return $this->redirectToRoute('app_admin_thesis_content');
    }

    #[Route('/admin/thesis/{id}/content/', name: 'app_admin_thesis_content', defaults: ['id' => 0])]
    public function thesisContent(
        int                  $id,
        Request              $request,
        ThesisMetaRepository $thesisMetaRepository,
        ThesisVOBuilder      $thesisVOBuilder,
        GenerationRepository $generationRepository
    ): Response {
        $thesisMeta = $thesisMetaRepository->findOneBy(['thesisId' => $id]);
        $thesisVOBuilder->setWithHighlight(false);
        $thesis = $thesisVOBuilder->buildById($id, false);
        $backLink = $this->generateUrl('app_admin_thesis_list');
        $generationId = $request->get('generationId') ?? null;
        if ($generationId !== null) {
            $generationDTO = $generationRepository->getGenerationDataById((int) $generationId);
            $backLink = $this->generateUrl('app_admin_generation_by_query') . '?query=' . $generationDTO->getQuery();
        }

        return $this->render('admin/thesis/content/index.html.twig', [
            'thesis' => $thesis?->toArray(),
            'thesisMeta' => $thesisMeta,
            'backLink' => $backLink
        ]);
    }

    #[Route('/admin/thesis/content/save/', name: 'app_admin_thesis_content_save')]
    public function thesisUpdate(
        Request              $request,
        ThesisMetaRepository $thesisMetaRepository,
        ThesisTypeRepository $thesisTypeRepository,
        ThesisRepository     $thesisRepository,
        ContentUpdateService $contentUpdateService,
        RabbitMQClient       $rabbitMQClient,
        GenerationRepository $generationRepository,
        ThesisCompleteRepository $thesisCompleteRepository
    ): Response {
        $allParams = $request->request->all();
        $content = $allParams['content'] ?? [];
        $thesisId = (int) $request->get('thesis-id');
        $generationId = (int) ($allParams['generationId'] ?? 0);

        $isNew = false;
        if (!empty($thesisId)) {
            $message = 'Thesis successfully updated';
            $thesisEntity = $thesisRepository->find($thesisId);
            $thesisMetaEntity = $thesisMetaRepository->findOneBy(['thesisId' => $thesisId]);
        } else {
            $fullContent = '';
            foreach ($content as $item) {
                $contentItem = json_decode($item, true);
                $fullContent .= ' ' . $contentItem['content'];
            }
//            $existThesisId = $thesisService->checkExistThesis($allParams['source'], $fullContent);
            $existThesisId = 0;
            if ($existThesisId) {
                return new JsonResponse([
                    'message' => 'Thesis already exist! Similar ThesisID #' . $existThesisId
                ]);
            } else {
                $isNew = true;
                $message = 'Thesis successfully created';
                $thesisEntity = new ThesisEntity();
                $thesisMetaEntity = new ThesisMetaEntity();
            }
        }
        $thesisTypeId = $thesisTypeRepository->getTypeIdByName($allParams['thesis-type']);
        if (empty($thesisTypeId)) {
            return new JsonResponse([
                'message' => 'Thesis Type not defined'
            ]);
        }

        try {
            $thesisEntity->setTitle($allParams['title'])
                ->setSource(urldecode($allParams['source']))
                ->setStatus((int) $allParams['status'])
                ->setTypeId($thesisTypeId);
            $thesisRepository->add($thesisEntity);

            $thesisMetaEntity->setThesisId($thesisEntity->getId());
            $thesisMetaEntity->setTitle($allParams['metaTitle']);
            $thesisMetaEntity->setKeywords($allParams['metaKeywords']);
            $thesisMetaEntity->setDescription($allParams['metaDescription']);
            $thesisMetaRepository->add($thesisMetaEntity);

            $contentUpdateService->update($thesisEntity, $content);

            if (!empty($generationId)) {
                $completeId = $generationRepository->getCompleteIdById($generationId);
                $thesisCompleteEntity = new ThesisCompleteEntity();
                $thesisCompleteEntity->setCompleteId($completeId);
                $thesisCompleteEntity->setThesisId($thesisEntity->getId());
                $thesisCompleteRepository->add($thesisCompleteEntity);
            }
        } catch (Exception $e) {
            if ($isNew) {
                $thesisRepository->remove($thesisEntity);
            }
            return new JsonResponse([
                'message' => $e->getMessage()
            ]);
        }

        $task = new Task($this->getParameter('thesis_update_queue'), ['id' => $thesisEntity->getId()]);
        $rabbitMQClient->doTask($task);

        $redirectUrl = $this->generateUrl('app_admin_thesis_content', ['id' => $thesisEntity->getId()]);
        if (!empty($generationId)) {
            $redirectUrl .= '?generationId=' . $generationId;
        }

        return new JsonResponse([
            'status' => 'success',
            'message' => $message,
            'data' => [
                'url' => $redirectUrl
            ]
        ]);
    }

    #[Route('/admin/thesis/content/delete/', name: 'app_admin_thesis_delete', methods: ['POST'])]
    public function thesisDelete(
        Request $request,
        ThesisRepository $thesisRepository,
        ThesisStateRepository $thesisStateRepository,
        ThesisCompleteRepository $thesisCompleteRepository,
        CompleteRepository $completeRepository,
        RabbitMQClient $rabbitMQClient
    ): JsonResponse {
        $data = $request->getContent();
        $data = json_decode($data, true);
        $thesisIds = $data['thesisIds'] ?? [];
        $thesisEntities = $thesisRepository->findBy(['id' => $thesisIds]);
        try {
            foreach ($thesisEntities as $thesisEntity) {
                $thesisId = $thesisEntity->getId();
                $thesisState = new ThesisStateEntity();
                $thesisState->setThesisId($thesisId);
                $thesisState->setTypeId($thesisEntity->getTypeId());
                $thesisStateRepository->add($thesisState);

                $completeIds = $thesisCompleteRepository->getCompleteIdsByThesisId($thesisId);
                foreach ($completeIds as $completeId) {
                    $completeThesisIds = $thesisCompleteRepository->getThesisIdsByCompleteId($completeId);
                    $completeThesisIds = array_combine($completeThesisIds, $completeThesisIds);
                    unset($completeThesisIds[$thesisId]);
                    if (empty($completeThesisIds)) {
                        $completeEntity = $completeRepository->find($completeId);
                        $completeTask = new Task($this->getParameter('complete_update_queue'), ['id' => $completeId]);
                        $rabbitMQClient->doTask($completeTask);
                        $completeRepository->remove($completeEntity);
                    }
                }

                $thesisRepository->remove($thesisEntity);

                $thesisTask = new Task($this->getParameter('thesis_update_queue'), ['id' => $thesisId]);
                $rabbitMQClient->doTask($thesisTask);
            }
            $status = 'success';
            $message = 'Thesis successfully deleted!';
            $url = $this->generateUrl('app_admin_thesis_list');
        } catch (Exception $e) {
            $status = 'error';
            $message = $e->getMessage();
            $url = '';
        }

        return new JsonResponse([
            'status' => $status,
            'message' => $message,
            'url' => $url
        ]);
    }
}
