<?php
declare(strict_types=1);

namespace App\Controller\Admin\Thesis;

use App\Controller\Admin\BaseController;
use App\Entity\CompleteEntity;
use App\Entity\ThesisCompleteEntity;
use App\Repository\CompleteRepository;
use App\Repository\GenerationRepository;
use App\Repository\ThesisCompleteRepository;
use App\Service\Complete\CompleteService;
use App\Service\RabbitMQ\RabbitMQClient;
use App\Service\RabbitMQ\Task\Task;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CompleteController extends BaseController
{
    #[Route('/admin/thesis/{id}/complete/', name: 'app_admin_thesis_complete', defaults: ['id' => 0], methods: ['GET'])]
    public function index(int $id, Request $request, GenerationRepository $generationRepository): Response
    {
        $generationId = (int) $request->get('generationId');
        $generationDTO = $generationRepository->getGenerationDataById($generationId);
        $backLink = $this->generateUrl('app_admin_thesis_list');
        if ($generationDTO !== null) {
            $backLink = $this->generateUrl('app_admin_generation_by_query') . '?query=' . $generationDTO->getQuery();
        }

        return $this->render('admin/thesis/complete/index.html.twig', [
            'thesisId' => $id,
            'backLink' => $backLink
        ]);
    }

    #[Route('/admin/thesis/complete/load/', name: 'app_admin_thesis_complete_load', methods: ['POST'])]
    public function loadComplete(
        Request $request,
        ThesisCompleteRepository $thesisCompleteRepository
    ): Response {
        $content = $request->getContent();
        $data = json_decode($content, true);
        $thesisId = (int) $data['thesisId'];
        $thesisQueryVOs = $thesisCompleteRepository->getByThesisId($thesisId);

        return $this->render('admin/thesis/complete/list.html.twig', [
            'thesisCompletes' => $thesisQueryVOs
        ]);
    }

    #[Route('/admin/thesis/complete/create/', name: 'app_admin_thesis_complete_create', methods: ['POST'])]
    public function createComplete(
        Request $request,
        CompleteService $completeService,
        CompleteRepository $completeRepository,
        ThesisCompleteRepository $thesisCompleteRepository,
        RabbitMQClient $rabbitMQClient
    ): Response {
        $content = $request->getContent();
        $data = json_decode($content, true);
        $thesisId = (int) $data['thesisId'];
        $complete = $data['complete'];
        $existComplete = $completeService->isExistComplete($complete);
        if ($existComplete !== null) {
            $thesisIds = $thesisCompleteRepository->getThesisIdsByCompleteId($existComplete->getCompleteId());
            if (in_array($thesisId, $thesisIds)) {
                return new JsonResponse([
                    'status' => 'error',
                    'message' => 'Complete already exist'
                ]);
            }

            $thesisCompleteEntity = new ThesisCompleteEntity();
            $thesisCompleteEntity->setThesisId($thesisId);
            $thesisCompleteEntity->setCompleteId($existComplete->getCompleteId());
            $thesisCompleteRepository->add($thesisCompleteEntity);
        } else {
            $completeEntity = new CompleteEntity();
            $completeEntity->setQuery($complete);
            $completeRepository->add($completeEntity);

            $thesisCompleteEntity = new ThesisCompleteEntity();
            $thesisCompleteEntity->setThesisId($thesisId);
            $thesisCompleteEntity->setCompleteId($completeEntity->getId());
            $thesisCompleteRepository->add($thesisCompleteEntity);

            $completeTask = new Task(
                $this->getParameter('complete_update_queue'),
                ['id' => [$completeEntity->getId()]]
            );
            $rabbitMQClient->doTask($completeTask);
        }

        return new JsonResponse(['status' => 'success']);
    }

    #[Route('/admin/thesis/complete/delete/', name: 'app_admin_thesis_complete_delete', methods: ['POST'])]
    public function deleteComplete(
        Request $request,
        CompleteRepository $completeRepository,
        ThesisCompleteRepository $thesisCompleteRepository,
        RabbitMQClient $rabbitMQClient
    ): Response {
        $content = $request->getContent();
        $data = json_decode($content, true);
        $relationId = (int) $data['relationId'];
        $thesisCompleteEntity = $thesisCompleteRepository->find($relationId);
        $completeId = $thesisCompleteEntity->getCompleteId();
        $thesisCompleteRepository->remove($thesisCompleteEntity);
        $thesisIds = $thesisCompleteRepository->getThesisIdsByCompleteId($completeId);
        if (empty($thesisIds)) {
            $completeEntity = $completeRepository->find($completeId);
            $completeRepository->remove($completeEntity);

            $completeTask = new Task(
                $this->getParameter('complete_update_queue'),
                ['id' => [$completeId]]
            );
            $rabbitMQClient->doTask($completeTask);
        }

        return new JsonResponse(['status' => 'success']);
    }
}
