<?php

namespace App\Controller;

use App\Entity\ThesisCreateEntity;
use App\Repository\CompleteRepository;
use App\Repository\ThesisCreateRepository;
use App\Repository\ThesisTypeRepository;
use App\Service\Complete\CompleteService;
use App\Service\Formatter\ThesisDocsFormatter;
use App\Service\Menu\SearchMenuService;
use App\Service\RabbitMQ\RabbitMQClient;
use App\Service\RabbitMQ\Task\Task;
use App\Service\Search\Context\SearchContextBuilder;
use App\Service\Search\ThesisSearchEngine;
use App\Service\Search\SearchService;
use App\Service\Seo\SeoBuilder;
use App\Service\Solr\Response\SolrResponse;
use SolrClientException;
use SolrServerException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    public function __construct(
        private RabbitMQClient     $rabbitMQClient,
        private ThesisSearchEngine $thesisSearchEngine,
        private SeoBuilder         $seoService,
        private CompleteService $completeService
    ) {
    }

    /**
     * @throws SolrClientException
     * @throws SolrServerException
     */
    #[Route('/', name: 'app_home')]
    public function index(
        Request $request,
        SearchContextBuilder $searchContextBuilder,
        ThesisDocsFormatter $thesisDocsFormatter,
        SearchMenuService $searchMenuService
    ): Response {
        $searchContextVO = $searchContextBuilder->build($request);

        $query = $searchContextVO->getQuery();

        $seoVO = $this->seoService->buildBySearch($query);

        if (empty($query)) {
            $seoVO = $this->seoService->buildByUrl($searchContextVO->getUrl());
            $response = $this->render('base.html.twig', [
                'seo' => $seoVO
            ]);
            $response->headers->clearCookie('query');

            return $response;
        }

        $completes = $this->completeService->getSearchCompletes($query);
        $solrResponseCollection = $this->thesisSearchEngine->search($searchContextVO);
//        $solrResponseCollection = $this->searchService->search($searchContextVO);
        $totalDocs = 0;
        if ($searchContextVO->getThesisType()->getName() === ThesisTypeRepository::TYPE_DEFAULT) {
            /** @var SolrResponse $solrResponse */
            foreach ($solrResponseCollection as $solrResponse) {
                $totalDocs += $solrResponse->getNumFound();
            }
        } else {
            /** @var SolrResponse $solrResponse */
            foreach ($solrResponseCollection as $solrResponse) {
                if ($solrResponse->getType() === $searchContextVO->getThesisType()->getName()) {
                    $totalDocs = $solrResponse->getNumFound();
                }
            }
        }
        $docs = $thesisDocsFormatter->format($solrResponseCollection);

        $searchMenu = $searchMenuService->loadMenu($searchContextVO, $solrResponseCollection);

        $response = $this->render('search.html.twig', [
            'seo' => $seoVO,
            'query' => $query,
            'completes' => $completes,
            'numFound' => $totalDocs,
            'docs' => $docs,
            'searchMenu' => $searchMenu
        ]);

        $response->headers->setCookie(new Cookie('query', $searchContextVO->getOriginalQuery()));

        return $response;
    }

    /**
     * @throws SolrClientException
     * @throws SolrServerException
     */
    #[Route('/complete/q={query}', name: 'app_complete')]
    public function complete(string $query): Response
    {
        $completeVOs = $this->completeService->getSearchCompletes($query);
        $completes = [];
        foreach ($completeVOs as $completeVO) {
            $completes[] = $completeVO->toArray();
        }
        return new JsonResponse([
            'complete' => $completes
        ]);
    }

    #[Route('/complete/update/', name: 'app_complete_update')]
    public function completeUpdate(
        Request $request,
        CompleteRepository $completeRepository
    ): Response {
        $data = $request->getContent();
        $data = json_decode($data, true);
        $completeId = (int) ($data['completeId'] ?? 0);

        $completeEntity = $completeRepository->find($completeId);
        $rank = $completeEntity->getRank();
        $rank++;
        $completeEntity->setRank($rank);
        $completeRepository->add($completeEntity);
        $completeTask = new Task(
            $this->getParameter('complete_update_queue'),
            ['id' => [$completeId]]
        );
        $this->rabbitMQClient->doTask($completeTask);

        return new JsonResponse(['url' => $this->generateUrl('app_home')]);
    }

    #[Route('/create/{query}', name: 'app_create')]
    public function create(
        string $query,
        ThesisCreateRepository $thesisCreateRepository
    ): Response {
        $thesisCreateEntity = new ThesisCreateEntity();
        $thesisCreateEntity->setQuery($query);
        $thesisCreateRepository->add($thesisCreateEntity);

        $task = new Task($this->getParameter('thesis_create_queue'), ['id' => $thesisCreateEntity->getId()]);
        $this->rabbitMQClient->doTask($task);

        return new JsonResponse();
    }
}
