<?php
declare(strict_types=1);

namespace App\Controller;

use App\Service\Menu\SearchMenuService;
use App\Service\Search\Context\SearchContextBuilder;
use App\Service\Search\SearchService;
use App\Service\Search\ThesisSearchEngine;
use App\Service\Seo\SeoBuilder;
use App\Service\Solr\Response\SolrResponseCollection;
use App\Service\Thesis\ThesisVOBuilder;
use App\Service\Thesis\ThesisVOCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class ThesisController extends AbstractController
{
    public function __construct(
        private SearchContextBuilder $searchContextBuilder,
        private SearchMenuService    $searchMenuService,
        private ThesisVOBuilder      $thesisVOBuilder,
        private ThesisSearchEngine   $thesisSearchEngine
    ) {
    }

    #[Route('/{id}/', name: 'app_thesis', requirements: ['id' => '\d+'])]
    public function thesis(Request $request): Response
    {
        $id = (int) $request->get('id');
        $thesisVO = $this->thesisVOBuilder->buildById($id);
        if ($thesisVO === null) {
            throw new NotFoundHttpException();
        }

        $searchContextVO = $this->searchContextBuilder->build($request);
        $solrResponseCollection = new SolrResponseCollection();
        if (!empty($searchContextVO->getQuery())) {
            $solrResponseCollection = $this->thesisSearchEngine->search($searchContextVO);
        }
        $searchMenu = $this->searchMenuService->loadMenu($searchContextVO, $solrResponseCollection);

        return $this->render('thesis.html.twig', [
            'thesis' => $thesisVO,
            'query' => $searchContextVO->getOriginalQuery(),
            'similar' => new ThesisVOCollection(),
            'seo' => $thesisVO->getSeo(),
            'searchMenu' => $searchMenu,
            'completes' => [],
        ]);
    }
}
