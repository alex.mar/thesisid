<?php
declare(strict_types=1);

namespace App\Command;

use App\Entity\ThesisEntity;
use App\Repository\ThesisRepository;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Twig\Environment;

#[AsCommand(
    name: 'app:sitemap-generation',
    description: 'Generation sitemap files',
    hidden: false
)]
class SitemapCommand extends Command
{

    public function __construct(
        private ThesisRepository $thesisRepository,
        private UrlGeneratorInterface $urlGenerator,
        private ParameterBagInterface $params,
        private Environment $twig,
        private MailerInterface $mailer,
        private HttpClientInterface $client
    ) {
        parent::__construct();
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Step: start generation sitemap');

        $urls[] = [
            'loc' => $this->urlGenerator->generate('app_home', [], UrlGeneratorInterface::ABSOLUTE_URL),
            'lastmod' => '2022-10-01'
        ];
        $thesisEntitiesGenerator = $this->thesisRepository->getAllPublishedThesis();
        $allCountThesis = $this->thesisRepository->getAllCountThesis(true);
        $generated = 0;
        foreach ($thesisEntitiesGenerator as $thesisEntities) {
            /** @var ThesisEntity $thesisEntity */
            foreach ($thesisEntities as $thesisEntity) {
                $urls[] = [
                    'loc' => $this->urlGenerator->generate(
                        'app_thesis',
                        ['id' => $thesisEntity->getId()],
                        UrlGeneratorInterface::ABSOLUTE_URL
                    ),
                    'lastmod' => $thesisEntity->getCreated()->format('Y-m-d')
                ];
                $generated++;
            }
            $output->writeln('Generated: ' . $generated . '/' . $allCountThesis);
        }
        $content = $this->twig->render('sitemap/sitemap.html.twig', [
            'urls' => $urls
        ]);

        $output->writeln('Step: dumping file sitemap.xml');
        $fileSystem = new Filesystem();
        $filePath = $this->params->get('kernel.project_dir') . '/public/sitemap.xml';
        $fileSystem->touch($filePath);
        $fileSystem->dumpFile($filePath, $content);

        $output->writeln('Step: sending file to google');
        $response = $this->client->request(
            'GET',
            'https://www.google.com/webmasters/tools/ping?sitemap=https://www.thesisid.com/sitemap.xml'
        );

        $sitemapEmails = $this->params->get('email_sitemap_to');
        $sitemapEmails = explode(',', $sitemapEmails);
        try {
            $output->writeln('Step: sending emails');
            foreach ($sitemapEmails as $emailTo) {
                $email = (new TemplatedEmail())
                    ->from($this->params->get('email_sitemap_from'))
                    ->to($emailTo)
                    ->subject('Sitemap')
                    ->htmlTemplate('emails/sitemap.html.twig')
                    ->context([
                        'count' => $allCountThesis,
                        'homeUrl' => $this->urlGenerator->generate('app_home'),
                        'googleStatus' => $response->getStatusCode()
                    ]);
                $this->mailer->send($email);
            }
        } catch (TransportExceptionInterface $e) {
        }

        $output->writeln('Step: finished generation sitemap');

        return Command::SUCCESS;
    }
}
