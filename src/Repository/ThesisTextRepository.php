<?php

namespace App\Repository;

use App\Entity\ThesisTextEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ThesisTextEntity|null find($id, $lockMode = null, $lockVersion = null)
 * @method ThesisTextEntity|null findOneBy(array $criteria, array $orderBy = null)
 * @method ThesisTextEntity[]    findAll()
 * @method ThesisTextEntity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ThesisTextRepository extends ServiceEntityRepository
{
    public const FIELD_TEXT = 'text';

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ThesisTextEntity::class);
    }

    public function add(ThesisTextEntity $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function remove(ThesisTextEntity $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @param int $thesisId
     * @return ThesisTextEntity[] - indexed by id
     */
    public function getAllByThesisId(int $thesisId): array
    {
        $allEntities = $this->findBy(['thesisId' => $thesisId]);
        $indexedAllEntities = [];
        foreach ($allEntities as $entity) {
            $indexedAllEntities[$entity->getId()] = $entity;
        }

        return $indexedAllEntities;
    }
}
