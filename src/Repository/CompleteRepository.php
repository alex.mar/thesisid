<?php

namespace App\Repository;

use App\Entity\CompleteEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<CompleteEntity>
 *
 * @method CompleteEntity|null find($id, $lockMode = null, $lockVersion = null)
 * @method CompleteEntity|null findOneBy(array $criteria, array $orderBy = null)
 * @method CompleteEntity[]    findAll()
 * @method CompleteEntity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompleteRepository extends ServiceEntityRepository
{
    public const FIELD_ID = 'id';
    public const FIELD_QUERY = 'query';

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CompleteEntity::class);
    }

    public function add(CompleteEntity $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function remove(CompleteEntity $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function flush(): void
    {
        $this->_em->flush();
    }
}
