<?php

namespace App\Repository;

use App\Entity\ThesisTypeEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ThesisTypeEntity>
 *
 * @method ThesisTypeEntity|null find($id, $lockMode = null, $lockVersion = null)
 * @method ThesisTypeEntity|null findOneBy(array $criteria, array $orderBy = null)
 * @method ThesisTypeEntity[]    findAll()
 * @method ThesisTypeEntity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ThesisTypeRepository extends ServiceEntityRepository
{
    public const TYPE_DEFAULT = 'default';
    public const TYPE_DEFINITION = 'definition';
    public const TYPE_LIST = 'list';

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ThesisTypeEntity::class);
    }

    public function add(ThesisTypeEntity $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function remove(ThesisTypeEntity $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function getTypeIdByName(string $name): int
    {
        try {
            $thesisTypeId = (int) $this->createQueryBuilder('tt')
                ->select('tt.id')
                ->where('tt.name = :name')
                ->setParameter('name', $name)
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NoResultException|NonUniqueResultException $e) {
            $thesisTypeId = 0;
        }

        return $thesisTypeId;
    }
}
