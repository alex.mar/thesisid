<?php

namespace App\Repository;

use App\Entity\SearchMenuEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<SearchMenuEntity>
 *
 * @method SearchMenuEntity|null find($id, $lockMode = null, $lockVersion = null)
 * @method SearchMenuEntity|null findOneBy(array $criteria, array $orderBy = null)
 * @method SearchMenuEntity[]    findAll()
 * @method SearchMenuEntity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SearchMenuRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SearchMenuEntity::class);
    }

    public function add(SearchMenuEntity $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function remove(SearchMenuEntity $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }
}
