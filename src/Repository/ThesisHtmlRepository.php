<?php

namespace App\Repository;

use App\Entity\ThesisHtmlEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ThesisHtmlEntity>
 *
 * @method ThesisHtmlEntity|null find($id, $lockMode = null, $lockVersion = null)
 * @method ThesisHtmlEntity|null findOneBy(array $criteria, array $orderBy = null)
 * @method ThesisHtmlEntity[]    findAll()
 * @method ThesisHtmlEntity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ThesisHtmlRepository extends ServiceEntityRepository
{
    public const FIELD_CODE = 'code';

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ThesisHtmlEntity::class);
    }

    public function add(ThesisHtmlEntity $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function remove(ThesisHtmlEntity $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @param int $thesisId
     * @return ThesisHtmlEntity[] - indexed by id
     */
    public function getAllByThesisId(int $thesisId): array
    {
        $allEntities = $this->findBy(['thesisId' => $thesisId]);
        $indexedAllEntities = [];
        foreach ($allEntities as $entity) {
            $indexedAllEntities[$entity->getId()] = $entity;
        }

        return $indexedAllEntities;
    }
}
