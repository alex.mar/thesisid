<?php

namespace App\Repository;

use App\Entity\SeoEntity;
use App\Entity\UrlEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<SeoEntity>
 *
 * @method SeoEntity|null find($id, $lockMode = null, $lockVersion = null)
 * @method SeoEntity|null findOneBy(array $criteria, array $orderBy = null)
 * @method SeoEntity[]    findAll()
 * @method SeoEntity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SeoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SeoEntity::class);
    }

    public function add(SeoEntity $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function remove(SeoEntity $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function getSeoByUrl(string $url): ?SeoEntity
    {
        try {
            return $this->createQueryBuilder('s')
                ->select('s')
                ->join(UrlEntity::class, 'u', Join::WITH, 'u.id = s.urlId')
                ->where('u.url = :url')
                ->setParameter('url', $url)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}
