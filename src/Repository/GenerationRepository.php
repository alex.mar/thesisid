<?php

namespace App\Repository;

use App\Entity\CompleteEntity;
use App\Entity\GenerationEntity;
use App\Service\Generation\DTO\GenerationDTO;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<GenerationEntity>
 *
 * @method GenerationEntity|null find($id, $lockMode = null, $lockVersion = null)
 * @method GenerationEntity|null findOneBy(array $criteria, array $orderBy = null)
 * @method GenerationEntity[]    findAll()
 * @method GenerationEntity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GenerationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GenerationEntity::class);
    }

    public function add(GenerationEntity $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function remove(GenerationEntity $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @param int $completeId
     * @return string[] - indexed by id
     */
    public function getLinksByCompleteId(int $completeId): array
    {
        $rows = $this->createQueryBuilder('g')
            ->select('g.id', 'g.link')
            ->where('g.completeId = :completeId')
            ->setParameter('completeId', $completeId)
            ->getQuery()
            ->getResult();

        $result = [];
        foreach ($rows as $row) {
            $result[$row['id']] = $row['link'];
        }

        return $result;
    }

    public function getGenerationDataById(int $generationId): ?GenerationDTO
    {
        if (empty($generationId)) {
            return null;
        }
        try {
            $row = $this->createQueryBuilder('g')
                ->select('g.link', 'c.query')
                ->join(CompleteEntity::class, 'c', Join::WITH, 'c.id = g.completeId')
                ->where('g.id = :id')
                ->setParameter('id', $generationId)
                ->getQuery()
                ->getSingleResult();
            $result = new GenerationDTO($row['link'], $row['query']);
        } catch (NoResultException|NonUniqueResultException $e) {
            $result = null;
        }

        return $result;
    }

    public function getCompleteIdById(int $generationId): int
    {
        if (empty($generationId)) {
            return 0;
        }
        try {
            $result = $this->createQueryBuilder('g')
                ->select('g.completeId')
                ->where('g.id = :id')
                ->setParameter('id', $generationId)
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NoResultException|NonUniqueResultException $e) {
            $result = 0;
        }

        return (int) $result;
    }
}
