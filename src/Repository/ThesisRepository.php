<?php

namespace App\Repository;

use App\Entity\ThesisEntity;
use App\Service\Thesis\ThesisService;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ThesisEntity|null find($id, $lockMode = null, $lockVersion = null)
 * @method ThesisEntity|null findOneBy(array $criteria, array $orderBy = null)
 * @method ThesisEntity[]    findAll()
 * @method ThesisEntity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ThesisRepository extends ServiceEntityRepository
{
    public const FIELD_ID = 'id';

    public const STATUS_PUBLISHED = 1;
    public const STATUS_PROGRESS = 0;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ThesisEntity::class);
    }

    public function add(ThesisEntity $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function remove(ThesisEntity $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function getAllCountThesis(?bool $isPublished = null): int
    {
        $count = 0;
        try {
            $qb = $this->createQueryBuilder('t')
                ->select('COUNT(t.id) as count');
            if ($isPublished !== null) {
                $status = $isPublished ? self::STATUS_PUBLISHED : self::STATUS_PROGRESS;
                $qb->where('t.status = :status')
                    ->setParameter('status', $status);
            }
            $count = $qb->getQuery()
                ->getSingleScalarResult();
        } catch (NoResultException|NonUniqueResultException $e) {
        }

        return (int) $count;
    }

    /**
     * @param string $source
     * @return int[]
     */
    public function getThesisIdsBySource(string $source): array
    {
        $rows = $this->createQueryBuilder('t')
            ->select('t.id')
            ->where('t.source = :source')
            ->setParameter('source', $source)
            ->getQuery()
            ->getResult();
        $result = [];
        foreach ($rows as $row) {
            $result[] = (int) $row[self::FIELD_ID];
        }

        return $result;
    }

    public function getThesisIds(int $offset = 0, int $limit = ThesisService::DEFAULT_LIMIT): array
    {
        $rows = $this->createQueryBuilder('t')
            ->select('t.id')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->orderBy('t.id', 'DESC')
            ->getQuery()
            ->getResult();

        $result = [];
        foreach ($rows as $row) {
            $result[] = (int) $row['id'];
        }

        return $result;
    }

    public function getAllPublishedThesis(): \Generator
    {
        $fromId = 0;
        $limit = 5;

        $qb = $this->createQueryBuilder('t')
            ->select()
            ->where('t.status = :status')
            ->andWhere('t.id > :fromId')
            ->setParameter('status', self::STATUS_PUBLISHED)
            ->orderBy('t.id')
            ->setMaxResults($limit);
        do {
            $qb->setParameter('fromId', $fromId);
            /** @var ThesisEntity[] $thesisEntities */
            $thesisEntities = $qb->getQuery()->getResult();
            yield $thesisEntities;
            $lastEntity = end($thesisEntities);
            $fromId = end($thesisEntities);
        } while (count($thesisEntities) === $limit);
    }

    /**
     * @param int[] $ids
     * @return string[]
     */
    public function getThesisSourcesByIds(array $ids): array
    {
        $rows = $this->createQueryBuilder('t')
            ->select('t.source')
            ->where('t.id IN (:ids)')
            ->setParameter('ids', $ids)
            ->getQuery()
            ->getResult();

        $result = [];
        foreach ($rows as $row) {
            $result[] = $row['source'];
        }

        return $result;
    }
}
