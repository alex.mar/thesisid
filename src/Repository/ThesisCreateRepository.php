<?php

namespace App\Repository;

use App\Entity\ThesisCreateEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ThesisCreateEntity>
 *
 * @method ThesisCreateEntity|null find($id, $lockMode = null, $lockVersion = null)
 * @method ThesisCreateEntity|null findOneBy(array $criteria, array $orderBy = null)
 * @method ThesisCreateEntity[]    findAll()
 * @method ThesisCreateEntity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ThesisCreateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ThesisCreateEntity::class);
    }

    /**
     * @param ThesisCreateEntity $entity
     * @param bool $flush
     */
    public function add(ThesisCreateEntity $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @param ThesisCreateEntity $entity
     * @param bool $flush
     */
    public function remove(ThesisCreateEntity $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }
}
