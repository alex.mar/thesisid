<?php

namespace App\Repository;

use App\Entity\ThesisCssEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ThesisCssEntity>
 *
 * @method ThesisCssEntity|null find($id, $lockMode = null, $lockVersion = null)
 * @method ThesisCssEntity|null findOneBy(array $criteria, array $orderBy = null)
 * @method ThesisCssEntity[]    findAll()
 * @method ThesisCssEntity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ThesisCssRepository extends ServiceEntityRepository
{
    public const FIELD_STYLE = 'style';

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ThesisCssEntity::class);
    }

    public function add(ThesisCssEntity $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function remove(ThesisCssEntity $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @param int $thesisId
     * @return ThesisCssEntity[] - indexed by id
     */
    public function getAllByThesisId(int $thesisId): array
    {
        $allEntities = $this->findBy(['thesisId' => $thesisId]);
        $indexedAllEntities = [];
        foreach ($allEntities as $entity) {
            $indexedAllEntities[$entity->getId()] = $entity;
        }

        return $indexedAllEntities;
    }
}
