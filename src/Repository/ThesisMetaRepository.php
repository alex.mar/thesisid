<?php

namespace App\Repository;

use App\Entity\ThesisMetaEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ThesisMetaEntity|null find($id, $lockMode = null, $lockVersion = null)
 * @method ThesisMetaEntity|null findOneBy(array $criteria, array $orderBy = null)
 * @method ThesisMetaEntity[]    findAll()
 * @method ThesisMetaEntity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ThesisMetaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ThesisMetaEntity::class);
    }

    public function add(ThesisMetaEntity $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function remove(ThesisMetaEntity $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }
}
