<?php

namespace App\Repository;

use App\Entity\ThesisPhpEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ThesisPhpEntity>
 *
 * @method ThesisPhpEntity|null find($id, $lockMode = null, $lockVersion = null)
 * @method ThesisPhpEntity|null findOneBy(array $criteria, array $orderBy = null)
 * @method ThesisPhpEntity[]    findAll()
 * @method ThesisPhpEntity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ThesisPhpRepository extends ServiceEntityRepository
{
    public const FIELD_CODE = 'code';

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ThesisPhpEntity::class);
    }

    public function add(ThesisPhpEntity $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function remove(ThesisPhpEntity $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @param int $thesisId
     * @return ThesisPhpEntity[] - indexed by id
     */
    public function getAllByThesisId(int $thesisId): array
    {
        $allEntities = $this->findBy(['thesisId' => $thesisId]);
        $indexedAllEntities = [];
        foreach ($allEntities as $entity) {
            $indexedAllEntities[$entity->getId()] = $entity;
        }

        return $indexedAllEntities;
    }
}
