<?php

namespace App\Repository;

use App\Entity\ThesisBashEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ThesisBashEntity|null find($id, $lockMode = null, $lockVersion = null)
 * @method ThesisBashEntity|null findOneBy(array $criteria, array $orderBy = null)
 * @method ThesisBashEntity[]    findAll()
 * @method ThesisBashEntity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ThesisBashRepository extends ServiceEntityRepository
{
    public const FIELD_COMMAND = 'command';

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ThesisBashEntity::class);
    }

    public function add(ThesisBashEntity $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function remove(ThesisBashEntity $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @param int $thesisId
     * @return ThesisBashEntity[] - indexed by id
     */
    public function getAllByThesisId(int $thesisId): array
    {
        $allEntities = $this->findBy(['thesisId' => $thesisId]);
        $indexedAllEntities = [];
        foreach ($allEntities as $entity) {
            $indexedAllEntities[$entity->getId()] = $entity;
        }

        return $indexedAllEntities;
    }
}
