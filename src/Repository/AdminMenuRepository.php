<?php

namespace App\Repository;

use App\Entity\AdminMenuEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<AdminMenuEntity>
 *
 * @method AdminMenuEntity|null find($id, $lockMode = null, $lockVersion = null)
 * @method AdminMenuEntity|null findOneBy(array $criteria, array $orderBy = null)
 * @method AdminMenuEntity[]    findAll()
 * @method AdminMenuEntity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdminMenuRepository extends ServiceEntityRepository
{
    public const VALUE_ACTIVE_YES = 'yes';
    public const VALUE_ACTIVE_NO = 'no';
    public const VALUE_BLOCK_SUBMENU = 'submenu';

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AdminMenuEntity::class);
    }

    public function add(AdminMenuEntity $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function remove(AdminMenuEntity $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @return AdminMenuEntity[]
     */
    public function getAllMainMenuItems(): array
    {
        return $this->createQueryBuilder('am')
            ->select()
            ->where('am.block != :submenu')
            ->setParameter('submenu', self::VALUE_BLOCK_SUBMENU)
            ->orderBy('am.order')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return AdminMenuEntity[]
     */
    public function getAllSubMenuItems(int $parentId): array
    {
        return $this->createQueryBuilder('am')
            ->select()
            ->where('am.parentId = :parentId')
            ->andWhere('am.block = :block')
            ->setParameters([
                'parentId' => $parentId,
                'block' => self::VALUE_BLOCK_SUBMENU
            ])
            ->orderBy('am.order')
            ->getQuery()
            ->getResult();
    }
}
