<?php

namespace App\Repository;

use App\Entity\UrlEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<UrlEntity>
 *
 * @method UrlEntity|null find($id, $lockMode = null, $lockVersion = null)
 * @method UrlEntity|null findOneBy(array $criteria, array $orderBy = null)
 * @method UrlEntity[]    findAll()
 * @method UrlEntity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UrlRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UrlEntity::class);
    }

    public function add(UrlEntity $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function remove(UrlEntity $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }
}
