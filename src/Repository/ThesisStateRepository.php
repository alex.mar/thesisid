<?php

namespace App\Repository;

use App\Entity\ThesisStateEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ThesisStateEntity>
 *
 * @method ThesisStateEntity|null find($id, $lockMode = null, $lockVersion = null)
 * @method ThesisStateEntity|null findOneBy(array $criteria, array $orderBy = null)
 * @method ThesisStateEntity[]    findAll()
 * @method ThesisStateEntity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ThesisStateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ThesisStateEntity::class);
    }

    public function add(ThesisStateEntity $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function remove(ThesisStateEntity $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }
}
