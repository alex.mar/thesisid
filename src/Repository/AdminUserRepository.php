<?php

namespace App\Repository;

use App\Entity\AdminUserEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<AdminUserEntity>
 *
 * @method AdminUserEntity|null find($id, $lockMode = null, $lockVersion = null)
 * @method AdminUserEntity|null findOneBy(array $criteria, array $orderBy = null)
 * @method AdminUserEntity[]    findAll()
 * @method AdminUserEntity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdminUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AdminUserEntity::class);
    }

    public function update(AdminUserEntity $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function remove(AdminUserEntity $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }
}
