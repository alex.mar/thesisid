<?php

namespace App\Repository;

use App\Entity\CompleteEntity;
use App\Entity\ThesisCompleteEntity;
use App\Service\Thesis\VO\ThesisQueryVO;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ThesisCompleteEntity>
 *
 * @method ThesisCompleteEntity|null find($id, $lockMode = null, $lockVersion = null)
 * @method ThesisCompleteEntity|null findOneBy(array $criteria, array $orderBy = null)
 * @method ThesisCompleteEntity[]    findAll()
 * @method ThesisCompleteEntity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ThesisCompleteRepository extends ServiceEntityRepository
{
    public const FIELD_ID = 'id';
    public const FIELD_COMPLETE_ID = 'completeId';
    public const FIELD_THESIS_ID = 'thesisId';

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ThesisCompleteEntity::class);
    }

    public function add(ThesisCompleteEntity $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function remove(ThesisCompleteEntity $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @param int $thesisId
     * @return ThesisQueryVO[]
     */
    public function getByThesisId(int $thesisId): array
    {
        $rows = $this->createQueryBuilder('tq')
            ->select('tq.id', 'c.query')
            ->join(CompleteEntity::class, 'c', Join::WITH, 'c.id = tq.completeId')
            ->where('tq.thesisId = :thesisId')
            ->orderBy('c.rank', 'DESC')
            ->setParameter('thesisId', $thesisId)
            ->getQuery()
            ->getResult();

        $thesisQueryVOs = [];
        foreach ($rows as $row) {
            $thesisQueryVOs[] = new ThesisQueryVO($row[self::FIELD_ID], $row[CompleteRepository::FIELD_QUERY]);
        }

        return $thesisQueryVOs;
    }

    /**
     * @param int[] $thesisIds
     * @return ThesisQueryVO[] - grouped by thesisId
     */
    public function getCompletesByThesisIds(array $thesisIds): array
    {
        $rows = $this->createQueryBuilder('tq')
            ->select('tq.id', 'c.query', 'tq.thesisId')
            ->join(CompleteEntity::class, 'c', Join::WITH, 'c.id = tq.completeId')
            ->where('tq.thesisId IN (:thesisIds)')
            ->orderBy('c.rank', 'DESC')
            ->setParameter('thesisIds', $thesisIds)
            ->getQuery()
            ->getResult();

        $thesisQueryVOs = [];
        foreach ($rows as $row) {
            $thesisQueryVOs[$row[self::FIELD_THESIS_ID]][] = new ThesisQueryVO(
                $row[self::FIELD_ID],
                $row[CompleteRepository::FIELD_QUERY]
            );
        }

        return $thesisQueryVOs;
    }

    /**
     * @param int $completeId
     * @return int[]
     */
    public function getThesisIdsByCompleteId(int $completeId): array
    {
        $rows = $this->createQueryBuilder('tc')
            ->select('tc.thesisId')
            ->where('tc.completeId = :completeId')
            ->setParameter('completeId', $completeId)
            ->getQuery()
            ->getResult();

        $result = [];
        foreach ($rows as $row) {
            $result[] = (int) $row[self::FIELD_THESIS_ID];
        }

        return $result;
    }

    /**
     * @param int $thesisId
     * @return int[]
     */
    public function getCompleteIdsByThesisId(int $thesisId): array
    {
        $rows = $this->createQueryBuilder('tc')
            ->select('tc.completeId')
            ->where('tc.thesisId = :thesisId')
            ->setParameter('thesisId', $thesisId)
            ->getQuery()
            ->getResult();

        $completeIds = [];
        foreach ($rows as $row) {
            $completeIds[] = (int) $row[self::FIELD_COMPLETE_ID];
        }

        return $completeIds;
    }
}
