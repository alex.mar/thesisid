<?php

namespace App\Repository;

use App\Entity\ThesisContentTypeEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ThesisContentTypeEntity>
 *
 * @method ThesisContentTypeEntity|null find($id, $lockMode = null, $lockVersion = null)
 * @method ThesisContentTypeEntity|null findOneBy(array $criteria, array $orderBy = null)
 * @method ThesisContentTypeEntity[]    findAll()
 * @method ThesisContentTypeEntity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ThesisContentTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ThesisContentTypeEntity::class);
    }

    public function add(ThesisContentTypeEntity $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function remove(ThesisContentTypeEntity $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }
}
