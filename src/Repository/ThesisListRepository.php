<?php

namespace App\Repository;

use App\Entity\ThesisListEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ThesisListEntity>
 *
 * @method ThesisListEntity|null find($id, $lockMode = null, $lockVersion = null)
 * @method ThesisListEntity|null findOneBy(array $criteria, array $orderBy = null)
 * @method ThesisListEntity[]    findAll()
 * @method ThesisListEntity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ThesisListRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ThesisListEntity::class);
    }

    public function add(ThesisListEntity $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function remove(ThesisListEntity $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @param int $thesisId
     * @return ThesisListEntity[] - indexed by id
     */
    public function getAllByThesisId(int $thesisId): array
    {
        $allEntities = $this->findBy(['thesisId' => $thesisId]);
        $indexedAllEntities = [];
        foreach ($allEntities as $entity) {
            $indexedAllEntities[$entity->getId()] = $entity;
        }

        return $indexedAllEntities;
    }
}
