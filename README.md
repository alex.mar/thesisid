ThesisID App
===

##### Documentation:
https://thesisid.atlassian.net/wiki/spaces/SEARCH/overview

##### Responsible:
1. Developers: Aleksandr Martsynenko, Maksym Tokar, Andrey Sudev

Dependencies:
---
1. Database

   PROD:
    ```
    thesisid.com:3306/thesisid
    ```
   DEV:
    ```
    thesisid.com:3306/thesisid_weekly
    ```

2. Rabbit MQ

    PROD: 
    ```
    app.thesisid.chost.com.ua:8983
    ```
    DEV:
    ```
    app.thesisid.dev.chost.com.ua:5672
    ```

3. Solr

    PROD: 
    ```
    app.thesisid.chost.com.ua:8983
    ```
    DEV:
    ```
    app.thesisid.dev.chost.com.ua:8983
    ```

Install project
------
1. Install symfony cli
```
https://symfony.com/download
```
2. Install yarn 
```
https://classic.yarnpkg.com/lang/en/docs/install/#debian-stable
```
3. Run composer in project directory
```
composer install
```
4. Run npm in project directory
```
yarn install
```
5. Run webpack in project directory
```
yarn encore dev --watch
```
5. Create env.local with settings for dev environment
6. Run symfony server
```
symfony server:start
```
7. Go to http://localhost:8000 in browser
